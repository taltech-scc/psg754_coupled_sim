import os,glob
import sys
from shutil import copy2
# import python_func_list
import subprocess
import Py_funcs as pyg
import fileinput
import io
from contextlib import closing
import numpy as np
import time
import sys


cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))
maxsims=5

fortran_name='vdisp_BILINEAR-R1.for'
fortran_name2='vdisp_BILINEAR-R1-1EL.for'
Critical_pathA=np.loadtxt("Critical_pathA.txt" ,skiprows=1)

def find_nearest(array, value):
    array_cot = np.asarray(array[:,0])
    idx = (np.abs(array_cot - value)).argmin()
    Ra_FLC=array[idx,1]
    return Ra_FLC
    # return array[idx],idx

#------------------------------------------------------------------
#This creates normal distributions between 0 to 1
# def get_truncated_normal(mean=1, sd=2, low=0, upp=2):
#     return truncnorm(
#         (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

# lowv=0.01
# uppv=0.98
# me=lowv/2+uppv/2
# X1 = get_truncated_normal(mean=me, sd=0.8, low=lowv, upp=uppv)
# rand_norm=X1.rvs(400)


# random.choice(theta)        # pick value from array with uniform distribution

#------------------------------------------------------------------


# To plot this distribution use
# fig, ax = plt.subplots(3, sharex=True)
# ax[0].hist(X1.rvs(400), density=True)




#Definition of stress state
endphi2=1.0
dphi2=0.05
phi=np.arange(-0.9,endphi2+dphi2,dphi2) #stress state at first path
phi2=np.arange(-0.9,endphi2+dphi2,dphi2) #stress state at second path



#Definition of direction of principal strain
# theta_crit1=[0,0,0,0,] #----> the length is the same as phi (muhammed will give this)
start_theta=0
end_theta=40
dtheta=5 #5


def cleanup_folder():
    for CleanUp in glob.glob('*.*'):
        if not (CleanUp[-4:]=='.rpt' or CleanUp[-4:]=='.for'): 
            # print (CleanUp)
            os.remove(CleanUp) 
def deleteFile():
   for f in glob.glob("*.inp"):
      os.remove(f)
   for f in glob.glob("*.py"):
      os.remove(f)
   for f in glob.glob("*.for"):
      os.remove(f)
def change_fortran(phi,phi2,theta,for_name):
    # cwd=os.getcwd()
    # folder=os.path.basename(os.path.normpath(cwd))
    # texttoreplace='File'
    i=0
    # phi=-0.8s
    with closing(fileinput.FileInput(for_name,inplace = 1)) as file:
        for line in file:
            i+=1
            if i==25:
                print('      phi1='+ str(phi)),
            elif i==26:            
                print('      phi2='+ str(phi2)),   
            elif i==21:
                print('      theta=' +str(theta) +'*pi/180'), # the direction to principal stress will remain the same for both paths
                # print('File=\'88'+folder+ '.inp\'')
            else:
                print(line,end='')
def change_radius(phi_A,phi_B,for_name,for_name2):
    Raflc=find_nearest(Critical_pathA, phi_A)
    Ra=Raflc*np.random.randint(10, 90)/100          #max value at 1st stage can be 0.9% of FLC
    Rb_final=1.1*find_nearest(Critical_pathA, phi_B)
    i=0
    with closing(fileinput.FileInput(for_name,inplace = 1)) as file:
        for line in file:
            i+=1
            if i==22:
                print('      r1='+ str(Ra)),
            elif i==23:            
                print('      r2='+ str(Rb_final)),   
            else:
                print(line,end='')
    i=0
    with closing(fileinput.FileInput(for_name2,inplace = 1)) as file:
        for line in file:
            i+=1
            if i==22:
                print('      r1='+ str(Ra)),
            elif i==23:            
                print('      r2='+ str(Rb_final)),   
            else:
                print(line,end='')
    return (Ra,Rb_final)

jt=1
for ind in np.arange(4000): 
    cotfi1=np.random.choice(phi) #random cotfii
    cotfi2=np.random.choice(phi2) 
    RA_RB=change_radius(cotfi1,cotfi2,fortran_name,fortran_name2)  
    fol1=str('V_{:05d}-A_{:04.2f}-B_{:04.2f}-RA_{:4.2f}-RB_{:4.2f}'.format(ind+872,cotfi1,cotfi2,RA_RB[0],RA_RB[1])) 
    # convert_phi_triax=1/np.sqrt(3)*(cotfi1+1)/np.sqrt(1+cotfi1+cotfi1**2)   
    os.makedirs(fol1)
    copy2(fortran_name, fol1); copy2(fortran_name2, fol1); 
    copy2('MASTER.inp', fol1); copy2('Extract_TRIAX_EP1.py', fol1); copy2('Equation.inp', fol1);
    copy2('Master-single.inp', fol1); copy2('Extract_TRIAX_EP1-1EL.py', fol1);
    os.chdir(fol1)
    
    for idx1,theta in enumerate(np.arange(start_theta,end_theta+dtheta,dtheta)):
        fol='theta_'+str(theta)
        os.makedirs(fol)
        copy2(fortran_name, fol)
        copy2(fortran_name2, fol) 
        copy2('MASTER.inp', fol); copy2('Extract_TRIAX_EP1.py', fol); copy2('Equation.inp', fol);
        copy2('Master-single.inp', fol); copy2('Extract_TRIAX_EP1-1EL.py', fol);  
        os.chdir(fol)
        change_fortran(cotfi1,cotfi2,theta,fortran_name)
        change_fortran(cotfi1,cotfi2,theta,fortran_name2)
        
        #------------version 6.14
        # pyg.change_inp_write_solve(1,version=14,pc='mk',analysis='static',fname='MASTER.inp',fortran=fortran_name,theta=angle)
        # n=open('solveModel.bat',"a")       
        # if angle==0:
        #     n.write('call abq6141 cae noGUI=Extract_TRIAX_EP1-1EL\n')
        # n.write('call abq6141 cae noGUI=Extract_TRIAX_EP1\n')
        # n.write('for %%i in (*.*) do if not "%%~xi"==".rpt" if not "%%~xi"==".for" if not "%%~xi"==".bat" del /q "%%i"')
#------------------------------end of version change
        pyg.change_inp_write_solve(1,version=21,pc='ws',analysis='static',fname='MASTER.inp',fortran=fortran_name,theta=theta)
        n=open('solveModel.bat',"a")
        if theta==0:
            n.write('call abq2021hf6 cae noGUI=Extract_TRIAX_EP1-1EL\n')
        n.write('call abq2021hf6 cae noGUI=Extract_TRIAX_EP1\n')
        # n.write('for %%i in (*.*) do if not "%%~xi"==".rpt" if not "%%~xi"==".for" if not "%%~xi"==".inp" if not "%%~xi"==".bat" del /q "%%i"')
        n.write('for %%i in (*.*) do if not "%%~xi"==".rpt" if not "%%~xi"==".for" del /q "%%i"')
#------------------------------end of version change 
        n.close()
        
        
        
                
#Run the simulation
        subprocess.Popen(['solveModel.bat'],shell=True)   
        if idx1==jt*maxsims:
            t1=0
            while not os.path.exists('Principal_strains.rpt'):
                print('----------------------------------------------------')
                print('     Waiting for sim: '+fol1+'    '+fol)
                print('     nrsims='+str(idx1)+'   jt='+str(jt))
                print('     Duration of last='+str(t1*4))
                print('----------------------------------------------------')
                time.sleep(4)
                t1+=1
                if t1==400: #==800sek
                    break              
            jt+=1                 
        if theta+0.0001>end_theta:
            tt=0
            while not os.path.exists('Principal_strains.rpt'):
                print('----------------------------------------------------')
                print('     Last simulation with triax  last sim: '+fol1+'    '+fol)
                print('     Duration of last='+str(tt*4))
                print('----------------------------------------------------')
                time.sleep(4)
                tt+=1
                if tt==400: #==800sek
                    break
            jt=1
        os.chdir("..")
    deleteFile()
    os.chdir("..")
deleteFile()
os.chdir("..")

