# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *


# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='Master-single.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   




# eps_out= session.XYDataFromHistory(name='EPS-out', odb=odb, 
    # outputVariableName='Equivalent plastic strain: PEEQ at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# T_out = session.XYDataFromHistory(name='T-out', odb=odb, 
    # outputVariableName='Stress triaxiality: TRIAX at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# eps_in= session.XYDataFromHistory(name='EPS-in', odb=odb, 
    # outputVariableName='Equivalent plastic strain: PEEQ at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# T_in = session.XYDataFromHistory(name='T-in', odb=odb, 
    # outputVariableName='Stress triaxiality: TRIAX at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )


# aBAQUS UMAT OUTPUT
ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
    outputVariableName='ep3: SDV_ep3 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )
ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
    outputVariableName='ep2: SDV_ep2 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )
ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
    outputVariableName='ep1: SDV_ep1 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )   

ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
    outputVariableName='ep3: SDV_ep3 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )
ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
    outputVariableName='ep2: SDV_ep2 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )
ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
    outputVariableName='ep1: SDV_ep1 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )   

# aBAQUS STANDARD OUTPUT
# ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
    # outputVariableName='Principal plastic strains: PEP1 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
    # outputVariableName='Principal plastic strains: PEP2 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
    # outputVariableName='Principal plastic strains: PEP3 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )   

# ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
    # outputVariableName='Principal plastic strains: PEP1 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
    # outputVariableName='Principal plastic strains: PEP2 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )
# ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
    # outputVariableName='Principal plastic strains: PEP3 at Element 1 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), )    

SEout= session.XYDataFromHistory(name='SENER-out', odb=odb, 
    outputVariableName='Strain energy density: SENER at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )

SEin= session.XYDataFromHistory(name='SENER-in', odb=odb, 
    outputVariableName='Strain energy density: SENER at Element 1 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )




session.writeXYReport(fileName='energy-1EL.rpt', appendMode=OFF, xyData=(SEout,SEin))
session.writeXYReport(fileName='Principal_strains-1EL.rpt', appendMode=OFF, xyData=(ep1,ep2,ep3,ep1in,ep2in,ep3in))
# session.writeXYReport(fileName='Triax-eps-hist-1EL.rpt', appendMode=OFF, xyData=(eps_out, T_out,eps_in, T_in))


