# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *


# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='MASTER.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   

ELin=[146,396,646,896]
ELout=[155,405,655,905]

# for indx, (setA, setB) in enumerate(zip(nrange_list_A[1:-1], nrange_list_B[1:-1])):

for indx,(ELi,ELo) in enumerate(zip(ELin,ELout)):
# for EL in enumerate(ELin:
    ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
        outputVariableName='ep3: SDV_ep3 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
        outputVariableName='ep2: SDV_ep2 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
        outputVariableName='ep1: SDV_ep1 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )   

    ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
        outputVariableName='ep3: SDV_ep3 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
        outputVariableName='ep2: SDV_ep2 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
        outputVariableName='ep1: SDV_ep1 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )   
       

    SEout= session.XYDataFromHistory(name='SENER-out', odb=odb, 
        outputVariableName='Strain energy density: SENER at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )

    SEin= session.XYDataFromHistory(name='SENER-in', odb=odb, 
        outputVariableName='Strain energy density: SENER at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    # x0 = session.xyDataObjects['E_int']
    # x1 = session.xyDataObjects['E_kin']
    # x2 = session.xyDataObjects['F']
    # x3 = session.xyDataObjects['U']
    Din = session.XYDataFromHistory(name='Dinn',odb=odb, 
        outputVariableName='D_mmc: SDV_D_mmc at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ),)
    Dout = session.XYDataFromHistory(name='Dout',odb=odb, 
        outputVariableName='D_mmc: SDV_D_mmc at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ),)

    session.writeXYReport(fileName='Damage'+str(indx)+'.rpt', appendMode=OFF, xyData=(Din,Dout))
    session.writeXYReport(fileName='energy'+str(indx)+'.rpt', appendMode=OFF, xyData=(SEout,SEin))
    session.writeXYReport(fileName='Principal_strains'+str(indx)+'.rpt', appendMode=OFF, xyData=(ep1,ep2,ep3,ep1in,ep2in,ep3in))
    # session.writeXYReport(fileName='Triax-eps-hist.rpt', appendMode=OFF, xyData=(eps_out, T_out,eps_in, T_in))



