# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *


# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='MASTER.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   

# ELin=[146,396,646,896]
# ELout=[155,405,655,905]

# for 
# EL=odb.rootAssembly.instances['PART-1-1'].elementSets['INSIDE_0']
# odb.rootAssembly.instances['PART-1-1'].elementSets.keys()

# EL.elements[0].label

for indx in range(4):
    ELi=odb.rootAssembly.instances['PART-1-1'].elementSets['INSIDE_'+str(indx*10)].elements[0].label
    ELo=odb.rootAssembly.instances['PART-1-1'].elementSets['OUTSIDE_'+str(indx*10)].elements[0].label
    ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
        outputVariableName='ep3: SDV_ep3 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
        outputVariableName='ep2: SDV_ep2 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
        outputVariableName='ep1: SDV_ep1 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )   

    ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
        outputVariableName='ep3: SDV_ep3 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
        outputVariableName='ep2: SDV_ep2 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
        outputVariableName='ep1: SDV_ep1 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )   
       

    SEout= session.XYDataFromHistory(name='SENER-out', odb=odb, 
        outputVariableName='Strain energy density: SENER at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ), )

    SEin= session.XYDataFromHistory(name='SENER-in', odb=odb, 
        outputVariableName='Strain energy density: SENER at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ), )
    Din = session.XYDataFromHistory(name='Dinn',odb=odb, 
        outputVariableName='D_mmc: SDV_D_mmc at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
        steps=('Step-1', ),)
    Dout = session.XYDataFromHistory(name='Dout',odb=odb, 
        outputVariableName='D_mmc: SDV_D_mmc at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
        steps=('Step-1', ),)

    session.writeXYReport(fileName='Damage'+str(indx)+'.rpt', appendMode=OFF, xyData=(Din,Dout))
    session.writeXYReport(fileName='energy'+str(indx)+'.rpt', appendMode=OFF, xyData=(SEout,SEin))
    session.writeXYReport(fileName='Principal_strains'+str(indx)+'.rpt', appendMode=OFF, xyData=(ep1,ep2,ep3,ep1in,ep2in,ep3in))




# for indx,(ELi,ELo) in enumerate(zip(ELin,ELout)):
#     ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
#         outputVariableName='ep3: SDV_ep3 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )
#     ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
#         outputVariableName='ep2: SDV_ep2 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )
#     ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
#         outputVariableName='ep1: SDV_ep1 at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )   

#     ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
#         outputVariableName='ep3: SDV_ep3 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )
#     ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
#         outputVariableName='ep2: SDV_ep2 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )
#     ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
#         outputVariableName='ep1: SDV_ep1 at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )   
       

#     SEout= session.XYDataFromHistory(name='SENER-out', odb=odb, 
#         outputVariableName='Strain energy density: SENER at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )

#     SEin= session.XYDataFromHistory(name='SENER-in', odb=odb, 
#         outputVariableName='Strain energy density: SENER at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
#         steps=('Step-1', ), )
#     Din = session.XYDataFromHistory(name='Dinn',odb=odb, 
#         outputVariableName='D_mmc: SDV_D_mmc at Element '+str(ELi)+' Int Point 1 in ELSET INSIDE_'+str(indx*10), 
#         steps=('Step-1', ),)
#     Dout = session.XYDataFromHistory(name='Dout',odb=odb, 
#         outputVariableName='D_mmc: SDV_D_mmc at Element '+str(ELo)+' Int Point 1 in ELSET OUTSIDE_'+str(indx*10), 
#         steps=('Step-1', ),)

#     session.writeXYReport(fileName='Damage'+str(indx)+'.rpt', appendMode=OFF, xyData=(Din,Dout))
#     session.writeXYReport(fileName='energy'+str(indx)+'.rpt', appendMode=OFF, xyData=(SEout,SEin))
#     session.writeXYReport(fileName='Principal_strains'+str(indx)+'.rpt', appendMode=OFF, xyData=(ep1,ep2,ep3,ep1in,ep2in,ep3in))
#     # session.writeXYReport(fileName='Triax-eps-hist.rpt', appendMode=OFF, xyData=(eps_out, T_out,eps_in, T_in))



