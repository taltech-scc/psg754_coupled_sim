# -*- coding: mbcs -*-
#
# Abaqus/CAE Release 2021.HF6 replay file
# Internal Version: 2021_04_09-15.38.16 168095
# Run by Mihkel on Mon Aug 22 09:07:14 2022
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=258.622253417969, 
    height=272.766662597656)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from caeModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=ON)
import os
os.chdir(r"E:\teguh\UC1-CS2")
#: Warning: File "SMAPyrModules\SMAPyrAbqPy.m\src\allAbaqusMacros.py", line 140, in <module>
#: File "E:\teguh\UC1-CS2\abaqusMacros.py", line 26, in <module>
#:     name='C:/Users/Teguh/Work/P2/For journal/Shear+Compression/UC1/CS200/Master_final.odb')
#: OdbError: Cannot open file C:/Users/Teguh/Work/P2/For journal/Shear+Compression/UC1/CS200/Master_final.odb. *** ERROR: No such file: C:/Users/Teguh/Work/P2/For journal/Shear+Compression/UC1/CS200/Master_final.odb.
o1 = session.openOdb(name='E:/teguh/UC1-CS2/ESL A13 nonzero/Master_final.odb', 
    readOnly=False)
session.viewports['Viewport: 1'].setValues(displayedObject=o1)
#: Model: E:/teguh/UC1-CS2/ESL A13 nonzero/Master_final.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       2
#: Number of Node Sets:          6
#: Number of Steps:              1
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=250)
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=250)
session.viewports['Viewport: 1'].restore()
session.Viewport(name='Viewport: 2', origin=(5.59687471389771, 
    -2.09999990463257), width=475.501159667969, height=269.266662597656)
session.viewports['Viewport: 2'].makeCurrent()
o1 = session.openOdb(name='E:/teguh/UC1-CS2/Master_final.odb', readOnly=False)
session.viewports['Viewport: 2'].setValues(displayedObject=o1)
#* OdbError: The database is from a previous release of Abaqus. 
#* Run abaqus -upgrade -job <newFileName> -odb <oldOdbFileName> to upgrade it.
from  abaqus import session
session.upgradeOdb("E:/teguh/UC1-CS2/Master_final-old.odb", 
    "E:/teguh/UC1-CS2/Master_final.odb", )
from  abaqus import session
o7 = session.openOdb('E:/teguh/UC1-CS2/Master_final.odb', readOnly=False)
#: Model: E:/teguh/UC1-CS2/Master_final.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       5
#: Number of Node Sets:          10
#: Number of Steps:              1
session.viewports['Viewport: 2'].setValues(displayedObject=o7)
session.viewports['Viewport: 2'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports['Viewport: 2'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
session.viewports['Viewport: 2'].view.setValues(nearPlane=7.42358, 
    farPlane=9.91185, width=6.0001, height=3.32276, cameraPosition=(4.62123, 
    0.437068, 8.1084), cameraUpVector=(-0.19926, 0.967336, -0.156705), 
    cameraTarget=(1.58252, 1.50701, -0.0182804))
session.viewports['Viewport: 2'].view.setValues(nearPlane=6.51979, 
    farPlane=10.8763, width=5.26962, height=2.91823, cameraPosition=(6.03256, 
    -3.46963, 5.60759), cameraUpVector=(-0.278415, 0.844803, 0.456938), 
    cameraTarget=(1.57043, 1.54047, 0.00313634))
session.viewports['Viewport: 2'].view.setValues(nearPlane=6.92529, 
    farPlane=10.4828, width=5.59736, height=3.09973, cameraPosition=(6.80548, 
    -0.642397, 6.6516), cameraUpVector=(-0.526576, 0.814882, 0.242251), 
    cameraTarget=(1.56653, 1.5262, -0.00213289))
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=23)
session.viewports['Viewport: 2'].setValues(width=275.1796875)
session.viewports['Viewport: 2'].setValues(origin=(262.353515625, 
    1.39999389648438))
session.viewports['Viewport: 2'].view.setValues(nearPlane=6.86578, 
    farPlane=10.5828, width=3.59857, height=3.45594, viewOffsetX=0.181065, 
    viewOffsetY=-0.0432574)
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=95)
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=59)
session.viewports['Viewport: 1'].makeCurrent()
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=163)
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=95)
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=155)
session.viewports['Viewport: 2'].makeCurrent()
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=37)
session.viewports['Viewport: 1'].makeCurrent()
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=311)
session.viewports['Viewport: 2'].makeCurrent()
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=100)
session.viewports['Viewport: 1'].makeCurrent()
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=500)
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
#: Warning: The selected Primary Variable is not available in the current frame for any elements in the current display group.
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=0)
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=100)
session.viewports['Viewport: 2'].makeCurrent()
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=20)
session.viewports['Viewport: 2'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(COMPONENT, 'U3'), )
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(COMPONENT, 'U3'), )
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=200)
session.viewports['Viewport: 2'].makeCurrent()
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=40)
session.viewports['Viewport: 1'].makeCurrent()
odb = session.odbs['E:/teguh/UC1-CS2/ESL A13 nonzero/Master_final.odb']
session.xyDataListFromField(odb=odb, outputPosition=NODAL, variable=(('RF', 
    NODAL, ((COMPONENT, 'RF1'), )), ('U', NODAL, ((COMPONENT, 'U1'), )), ), 
    nodeSets=("PART-1-1.LEFT", ))
session.viewports['Viewport: 2'].makeCurrent()
odb = session.odbs['E:/teguh/UC1-CS2/Master_final.odb']
session.xyDataListFromField(odb=odb, outputPosition=NODAL, variable=(('RF', 
    NODAL, ((COMPONENT, 'RF1'), )), ('U', NODAL, ((COMPONENT, 'U1'), )), ), 
    nodeSets=("PART-1-1.LEFT_MID", ))
xy1 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 4466']
xy2 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 4484']
xy3 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 4493']
xy4 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 4511']
xy5 = sum(xy1, xy2, xy3, xy4)
xy5.setValues(
    sourceDescription='sum("RF:RF1 PI: PART-1-1 N: 4466", "RF:RF1 PI: PART-1-1 N: 4484", "RF:RF1 PI: PART-1-1 N: 4493", "RF:RF1 PI: PART-1-1 N: 4511")')
tmpName = xy5.name
session.xyDataObjects.changeKey(tmpName, 'Force-fem')
xy1 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 1']
xy2 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 2']
xy3 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 3']
xy4 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 4']
xy5 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 5']
xy6 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 6']
xy7 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 7']
xy8 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 8']
xy9 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 9']
xy10 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 10']
xy11 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 11']
xy12 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 12']
xy13 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 13']
xy14 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 14']
xy15 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 15']
xy16 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 16']
xy17 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 17']
xy18 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 18']
xy19 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 19']
xy20 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 20']
xy21 = session.xyDataObjects['RF:RF1 PI: PART-1-1 N: 21']
xy22 = sum(xy1, xy2, xy3, xy4, xy5, xy6, xy7, xy8, xy9, xy10, xy11, xy12, xy13, 
    xy14, xy15, xy16, xy17, xy18, xy19, xy20, xy21)
xy22.setValues(
    sourceDescription='sum("RF:RF1 PI: PART-1-1 N: 1", "RF:RF1 PI: PART-1-1 N: 2", "RF:RF1 PI: PART-1-1 N: 3", "RF:RF1 PI: PART-1-1 N: 4", "RF:RF1 PI: PART-1-1 N: 5", "RF:RF1 PI: PART-1-1 N: 6", "RF:RF1 PI: PART-1-1 N: 7", "RF:RF1 PI: PART-1-1 N: 8", "RF:RF1 PI: PART-1-1 N: 9", "RF:RF1 PI: PART-1-1 N: 10", "RF:RF1 PI: PART-1-1 N: 11", "RF:RF1 PI: PART-1-1 N: 12", "RF:RF1 PI: PART-1-1 N: 13", "RF:RF1 PI: PART-1-1 N: 14", "RF:RF1 PI: PART-1-1 N: 15", "RF:RF1 PI: PART-1-1 N: 16", "RF:RF1 PI: PART-1-1 N: 17", "RF:RF1 PI: PART-1-1 N: 18", "RF:RF1 PI: PART-1-1 N: 19", "RF:RF1 PI: PART-1-1 N: 20", "RF:RF1 PI: PART-1-1 N: 21")')
tmpName = xy22.name
session.xyDataObjects.changeKey(tmpName, 'FIRCE esl')
xyp = session.XYPlot('XYPlot-2')
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
xy1 = session.xyDataObjects['FIRCE esl']
c1 = session.Curve(xyData=xy1)
xy2 = session.xyDataObjects['Force-fem']
c2 = session.Curve(xyData=xy2)
chart.setValues(curvesToPlot=(c1, c2, ), )
session.charts[chartName].autoColor(lines=True, symbols=True)
session.viewports['Viewport: 2'].setValues(displayedObject=xyp)
xy1 = session.xyDataObjects['U:U1 PI: PART-1-1 N: 1']
xy2 = session.xyDataObjects['FIRCE esl']
xy3 = combine(xy1, xy2)
xy3.setValues(
    sourceDescription='combine ("U:U1 PI: PART-1-1 N: 1","FIRCE esl"  )')
tmpName = xy3.name
session.xyDataObjects.changeKey(tmpName, 'f-d-esl')
xyp = session.xyPlots['XYPlot-2']
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
xy1 = session.xyDataObjects['f-d-esl']
c1 = session.Curve(xyData=xy1)
chart.setValues(curvesToPlot=(c1, ), )
session.charts[chartName].autoColor(lines=True, symbols=True)
xy1 = session.xyDataObjects['U:U1 PI: PART-1-1 N: 4466']
xy2 = session.xyDataObjects['Force-fem']
xy3 = combine(xy1, xy2)
xyp = session.xyPlots['XYPlot-2']
chartName = xyp.charts.keys()[0]
chart = xyp.charts[chartName]
c1 = session.Curve(xyData=xy3)
chart.setValues(curvesToPlot=(c1, ), )
session.charts[chartName].autoColor(lines=True, symbols=True)
del session.viewports['Viewport: 1']
session.viewports['Viewport: 2'].setValues(origin=(64.8304672241211, 
    -2.566650390625))
session.viewports['Viewport: 2'].maximize()
iges = mdb.openIges(
    'C:/Users/Mihkel/Downloads/OneDrive_1_23-08-2022/Hull&Structure.igs', 
    msbo=False, trimCurve=DEFAULT, scaleFromFile=OFF)
mdb.models['Model-1'].PartFromGeometryFile(name='Hull&Structure', 
    geometryFile=iges, combine=False, stitchTolerance=1.0, 
    dimensionality=THREE_D, type=DEFORMABLE_BODY, convertToAnalytical=1, 
    stitchEdges=1)
p = mdb.models['Model-1'].parts['Hull&Structure']
session.viewports['Viewport: 2'].setValues(displayedObject=p)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11470.1, 
    farPlane=15416.4, width=8539.93, height=4211.92, cameraPosition=(5947.22, 
    -1042.7, 13607.2), cameraUpVector=(-0.419222, 0.890416, -0.177236), 
    cameraTarget=(2618.78, -164.34, 615.242))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10949, 
    farPlane=16091.4, width=8151.99, height=4020.59, cameraPosition=(-1061.23, 
    -12809.2, 3311.68), cameraUpVector=(-0.00602934, 0.536296, 0.844008), 
    cameraTarget=(2617.25, -166.902, 613))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11057.3, 
    farPlane=15987.1, width=8232.67, height=4060.38, cameraPosition=(-2428.82, 
    -2891.97, -11620.7), cameraUpVector=(-0.253799, -0.724971, 0.640314), 
    cameraTarget=(2609.16, -108.243, 524.677))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11209.8, 
    farPlane=15834.6, width=5125.64, height=2527.98, viewOffsetX=-180.15, 
    viewOffsetY=472.38)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11795, 
    farPlane=15817.1, width=5393.18, height=2659.93, cameraPosition=(855.741, 
    -10879, -7680.56), cameraUpVector=(-0.0068196, -0.297143, 0.954809), 
    cameraTarget=(2463.3, -215.825, 341.509), viewOffsetX=-189.553, 
    viewOffsetY=497.037)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10311.6, 
    farPlane=17107.2, width=4714.9, height=2325.4, cameraPosition=(15667.9, 
    -3165.21, -592.443), cameraUpVector=(-0.409601, -0.530295, 0.742303), 
    cameraTarget=(2720.97, 201.747, 704.186), viewOffsetX=-165.713, 
    viewOffsetY=434.526)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11866.5, 
    farPlane=14754.1, width=5425.88, height=2676.06, cameraPosition=(2883.19, 
    3969.83, -12020.9), cameraUpVector=(0.585632, -0.79506, 0.157843), 
    cameraTarget=(2165.57, 373.361, 909.349), viewOffsetX=-190.702, 
    viewOffsetY=500.05)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10109.8, 
    farPlane=16564.7, width=4622.65, height=2279.9, cameraPosition=(-10755.3, 
    356.075, -1608.97), cameraUpVector=(0.473606, -0.560938, -0.679003), 
    cameraTarget=(2432.34, 364.27, 984.622), viewOffsetX=-162.471, 
    viewOffsetY=426.024)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10046.9, 
    farPlane=16520, width=4593.88, height=2265.72, cameraPosition=(-7423.5, 
    6500.96, -5495.05), cameraUpVector=(0.432186, -0.669153, -0.604524), 
    cameraTarget=(2497.46, 265.964, 1088.27), viewOffsetX=-161.46, 
    viewOffsetY=423.373)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10324.1, 
    farPlane=16242.8, width=682.689, height=336.704, viewOffsetX=-1883.84, 
    viewOffsetY=821.147)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10324.5, 
    farPlane=16242.3, width=682.72, height=336.72, cameraPosition=(-7396.18, 
    6486.33, -5550.07), cameraUpVector=(0.41761, -0.684717, -0.597298), 
    cameraTarget=(2524.78, 251.337, 1033.25), viewOffsetX=-1883.93, 
    viewOffsetY=821.185)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10324.5, 
    width=682.724, height=336.721, cameraPosition=(-7359.87, 6464.09, 
    -5625.85), cameraUpVector=(0.397385, -0.705647, -0.586641), cameraTarget=(
    2561.09, 229.099, 957.468), viewOffsetX=-1883.94, viewOffsetY=821.188)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10051.4, 
    farPlane=16515.5, width=5367.37, height=2647.2, viewOffsetX=-794.441, 
    viewOffsetY=345.918)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10157.8, 
    farPlane=16805.9, width=5424.18, height=2675.22, cameraPosition=(-8711.9, 
    5867.09, 5786.1), cameraUpVector=(-0.028495, -0.970207, 0.240597), 
    cameraTarget=(1477.1, -534.519, -200.901), viewOffsetX=-802.85, 
    viewOffsetY=349.58)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11073.4, 
    farPlane=15138.6, width=5913.11, height=2916.36, cameraPosition=(3453.17, 
    7459.41, 11466.5), cameraUpVector=(-0.427747, -0.864686, 0.263345), 
    cameraTarget=(1215.53, 40.6429, 484.865), viewOffsetX=-875.218, 
    viewOffsetY=381.091)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9726.12, 
    farPlane=15724.2, width=5193.67, height=2561.53, cameraPosition=(8113.51, 
    9746.81, 6625.17), cameraUpVector=(-0.367046, -0.62053, 0.692979), 
    cameraTarget=(1089.26, -16.5649, 627.002), viewOffsetX=-768.732, 
    viewOffsetY=334.724)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10254.5, 
    farPlane=14890, width=5475.82, height=2700.69, cameraPosition=(4565.27, 
    12117.3, 3514.66), cameraUpVector=(0.0215193, -0.534166, 0.845106), 
    cameraTarget=(1002.58, -572.729, 885.61), viewOffsetX=-810.494, 
    viewOffsetY=352.908)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10291.4, 
    farPlane=14853.1, width=5495.51, height=2710.4, viewOffsetX=-1060.19, 
    viewOffsetY=-294.457)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10121.1, 
    farPlane=15000.6, width=5404.59, height=2665.56, cameraPosition=(-2700.95, 
    11368.7, 2708.43), cameraUpVector=(0.37624, -0.338454, 0.862492), 
    cameraTarget=(1644.73, -1242.19, 1058.41), viewOffsetX=-1042.65, 
    viewOffsetY=-289.586)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9154.74, 
    farPlane=15929.2, width=4888.56, height=2411.05, cameraPosition=(-9889.64, 
    2850.91, 437.233), cameraUpVector=(0.274907, -0.147438, 0.950099), 
    cameraTarget=(2827.37, -1483.57, 800.391), viewOffsetX=-943.098, 
    viewOffsetY=-261.936)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9042.38, 
    farPlane=16041.6, width=8115.23, height=4002.45, viewOffsetX=-745.748, 
    viewOffsetY=55.0885)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9436.76, 
    farPlane=15745.2, width=8469.17, height=4177.02, cameraPosition=(-5372.51, 
    6697.98, 8089.74), cameraUpVector=(0.350068, -0.774005, 0.527606), 
    cameraTarget=(1932.18, -1246.72, 79.4232), viewOffsetX=-778.273, 
    viewOffsetY=57.4912)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9457.23, 
    farPlane=15245.8, width=8487.55, height=4186.08, cameraPosition=(-3762.31, 
    10443.2, -1978.11), cameraUpVector=(-0.0064197, -0.138245, 0.990377), 
    cameraTarget=(1763.45, -1461.02, 919.637), viewOffsetX=-779.961, 
    viewOffsetY=57.6159)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9979.28, 
    farPlane=14505.3, width=8956.07, height=4417.16, cameraPosition=(-616.738, 
    7865.15, -8285.9), cameraUpVector=(0.0222219, 0.48541, 0.874004), 
    cameraTarget=(1376.61, -933.933, 1676.24), viewOffsetX=-823.015, 
    viewOffsetY=60.7963)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10438.4, 
    farPlane=14046.2, width=2264.41, height=1116.81, viewOffsetX=-1299.66, 
    viewOffsetY=395.749)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9753.98, 
    farPlane=14699.8, width=2115.94, height=1043.59, cameraPosition=(-3571.38, 
    8205.26, -6249.93), cameraUpVector=(0.0981014, 0.356735, 0.929041), 
    cameraTarget=(1790.88, -1261.44, 1641.16), viewOffsetX=-1214.44, 
    viewOffsetY=369.801)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9618.9, 
    farPlane=14834.9, width=4447.7, height=2193.62, viewOffsetX=-859.136, 
    viewOffsetY=210.7)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9603.7, 
    farPlane=15050.6, width=4440.67, height=2190.15, cameraPosition=(-4296.44, 
    10083.1, -1953.3), cameraUpVector=(0.188514, -0.0293082, 0.981633), 
    cameraTarget=(1875.3, -1464.51, 1080.61), viewOffsetX=-857.778, 
    viewOffsetY=210.367)
session.viewports['Viewport: 2'].view.setValues(nearPlane=9647.98, 
    farPlane=15006.4, width=4461.14, height=2200.25, viewOffsetX=-876.951, 
    viewOffsetY=193.857)
Mdb()
#: A new model database has been created.
#: The model "Model-1" has been created.
session.viewports['Viewport: 2'].setValues(displayedObject=None)
iges = mdb.openIges(
    'C:/Users/Mihkel/Downloads/OneDrive_1_23-08-2022/Hull&Structure.igs', 
    msbo=True, trimCurve=DEFAULT, scaleFromFile=OFF)
mdb.models['Model-1'].PartFromGeometryFile(name='Hull&Structure', 
    geometryFile=iges, combine=False, stitchTolerance=1.0, 
    dimensionality=THREE_D, type=DEFORMABLE_BODY, convertToAnalytical=1, 
    stitchEdges=1)
p = mdb.models['Model-1'].parts['Hull&Structure']
session.viewports['Viewport: 2'].setValues(displayedObject=p)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10440.5, 
    farPlane=16611.4, width=7773.37, height=3833.85, cameraPosition=(9838.15, 
    6715.42, -8395.42), cameraUpVector=(-0.910505, 0.406117, 0.0777773), 
    cameraTarget=(2618.79, -164.338, 615.238))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10591.7, 
    farPlane=16115.1, width=7885.98, height=3889.39, cameraPosition=(-2872.31, 
    6639.48, -9642.97), cameraUpVector=(-0.343653, 0.369136, 0.863505), 
    cameraTarget=(2538.32, -164.819, 607.34))
session.viewports['Viewport: 2'].view.setValues(nearPlane=9748.2, 
    farPlane=16656.4, width=7257.96, height=3579.65, cameraPosition=(-7542.7, 
    8562.01, -873.003), cameraUpVector=(0.175071, -0.151649, 0.972807), 
    cameraTarget=(2568.71, -177.328, 550.276))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10284.6, 
    farPlane=16120.1, width=188.128, height=92.785, viewOffsetX=-796.778, 
    viewOffsetY=-334.477)
p = mdb.models['Model-1'].parts['Hull&Structure']
f = p.faces
p.RemoveFaces(faceList = f[145:146], deleteCells=False)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10295.2, 
    farPlane=16109.4, width=64.7436, height=31.9317, viewOffsetX=-1409.29, 
    viewOffsetY=-331.614)
p = mdb.models['Model-1'].parts['Hull&Structure']
e = p.edges
p.RepairInvalidEdges(edgeList = e[541:542])
mdb.models['Model-1'].parts['Hull&Structure'].checkGeometry()
#: Part 'Hull&Structure' is not valid.
#: Part 'Hull&Structure' is a shell part(273 shell faces, 1246 edges, 981 vertices).
session.viewports['Viewport: 2'].view.setValues(nearPlane=9996.42, 
    farPlane=16408.2, width=4604.66, height=2271.03, viewOffsetX=-439.231, 
    viewOffsetY=315.808)
a = mdb.models['Model-1'].rootAssembly
session.viewports['Viewport: 2'].setValues(displayedObject=a)
mdb.ModelFromInputFile(name='test', 
    inputFileName='C:/Users/Mihkel/Downloads/OneDrive_1_23-08-2022/test.inp')
#: The model "test" has been created.
#: The part "PART-1" has been imported from the input file.
#: 
#: WARNING: The following keywords/parameters are not yet supported by the input file reader:
#: ---------------------------------------------------------------------------------
#: *ELFILE
#: *ELPRINT
#: *FILEFORMAT
#: *NODEFILE
#: *NODEPRINT
#: The model "test" has been imported from an input file. 
#: Please scroll up to check for error and warning messages.
session.viewports['Viewport: 2'].assemblyDisplay.setValues(
    optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
a = mdb.models['test'].rootAssembly
session.viewports['Viewport: 2'].setValues(displayedObject=a)
session.viewports['Viewport: 2'].view.setValues(nearPlane=6105.32, 
    farPlane=8462.53, width=4585.52, height=2261.59, cameraPosition=(614.227, 
    -4355.87, 5842.31), cameraUpVector=(0.274212, 0.884308, 0.377898), 
    cameraTarget=(1235.05, 474.191, 479.088))
session.viewports['Viewport: 2'].view.setValues(nearPlane=5961.31, 
    farPlane=8727.21, width=4477.36, height=2208.25, cameraPosition=(-359.608, 
    -5328.59, -3614.99), cameraUpVector=(0.0932473, -0.276079, 0.956601), 
    cameraTarget=(1229.74, 468.891, 427.562))
session.viewports['Viewport: 2'].view.setValues(nearPlane=5285.45, 
    farPlane=9188.47, width=3969.74, height=1957.89, cameraPosition=(-5140.38, 
    -1240.79, -2659.33), cameraUpVector=(0.567461, -0.818374, 0.0908471), 
    cameraTarget=(1164.63, 524.562, 440.577))
session.viewports['Viewport: 2'].view.setValues(nearPlane=6163.34, 
    farPlane=8465.78, width=4629.11, height=2283.09, cameraPosition=(2762.41, 
    -209.411, -6581.39), cameraUpVector=(-0.0463212, -0.902924, 0.427297), 
    cameraTarget=(1156.68, 523.524, 444.523))
session.viewports['Viewport: 2'].view.setValues(nearPlane=5712.89, 
    farPlane=8947.49, width=4290.79, height=2116.23, cameraPosition=(4825.51, 
    -1204.56, -5563.21), cameraUpVector=(-0.314189, -0.83617, 0.449561), 
    cameraTarget=(1176.51, 513.957, 454.311))
o1 = session.openOdb(name='C:/Users/Mihkel/Downloads/Job-Mizuguomo_FEM.odb', 
    readOnly=False)
session.viewports['Viewport: 2'].setValues(displayedObject=o1)
#: Model: C:/Users/Mihkel/Downloads/Job-Mizuguomo_FEM.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       12
#: Number of Node Sets:          3
#: Number of Steps:              1
session.viewports['Viewport: 2'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
session.viewports['Viewport: 2'].odbDisplay.commonOptions.setValues(
    deformationScaling=UNIFORM)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10671.1, 
    farPlane=16454.2, width=7756.19, height=3918.52, cameraPosition=(9511.27, 
    -4790.52, 11185.7), cameraUpVector=(0.233562, 0.95973, -0.156098), 
    cameraTarget=(2618.78, -164.329, 615.252))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10667.5, 
    farPlane=16457.9, width=7753.58, height=3917.19, cameraPosition=(9511.27, 
    -4790.52, 11185.7), cameraUpVector=(-0.679282, 0.665104, 0.31018), 
    cameraTarget=(2618.78, -164.33, 615.252))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10776.5, 
    farPlane=16359.2, width=7832.8, height=3957.22, cameraPosition=(7626.42, 
    11778.5, -3556.6), cameraUpVector=(-0.505988, 0.127827, 0.853016), 
    cameraTarget=(2601.77, -14.845, 482.248))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10978.1, 
    farPlane=16157.6, width=4165.27, height=2104.34, viewOffsetX=350.672, 
    viewOffsetY=315.7)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10640.7, 
    farPlane=17134, width=4037.27, height=2039.67, cameraPosition=(-7920.79, 
    8779.94, 3829.56), cameraUpVector=(0.382475, -0.429702, 0.817966), 
    cameraTarget=(2164.05, 623.628, 306.339), viewOffsetX=339.896, 
    viewOffsetY=305.998)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10622, 
    farPlane=17253.7, width=4030.17, height=2036.09, cameraPosition=(-9688.02, 
    6944.5, 1480.16), cameraUpVector=(0.35706, -0.227347, 0.905992), 
    cameraTarget=(2063.4, 550.698, 189.359), viewOffsetX=339.298, 
    viewOffsetY=305.46)
session.viewports['Viewport: 2'].odbDisplay.setPrimaryVariable(
    variableLabel='U', outputPosition=NODAL, refinement=(INVARIANT, 
    'Magnitude'), )
session.viewports['Viewport: 2'].view.setValues(nearPlane=11295.3, 
    farPlane=16301.5, width=4285.66, height=2165.16, cameraPosition=(8390.46, 
    12423.1, 1372.89), cameraUpVector=(-0.273781, -0.329209, 0.903695), 
    cameraTarget=(2974.34, 175.722, 227.98), viewOffsetX=360.807, 
    viewOffsetY=324.824)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11098.9, 
    farPlane=16539.1, width=4211.16, height=2127.53, cameraPosition=(9670.12, 
    11675.5, -749.394), cameraUpVector=(-0.25922, -0.159449, 0.952566), 
    cameraTarget=(2970.54, 61.6196, 186.641), viewOffsetX=354.535, 
    viewOffsetY=319.177)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10513.4, 
    farPlane=17207, width=3989, height=2015.29, cameraPosition=(-9675.9, 
    6449.28, -1662.15), cameraUpVector=(0.269892, 0.0591958, 0.961069), 
    cameraTarget=(2241.19, 466.678, 21.3283), viewOffsetX=335.831, 
    viewOffsetY=302.338)
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=0)
session.viewports[session.currentViewportName].odbDisplay.setFrame(
    step='Step-1', frame=1)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10683.6, 
    farPlane=17148.1, width=4053.56, height=2047.91, cameraPosition=(-9607.37, 
    -2390.67, -5990.1), cameraUpVector=(-0.117386, -0.0335716, 0.992519), 
    cameraTarget=(2102.06, 370.776, 2.08533), viewOffsetX=341.266, 
    viewOffsetY=307.231)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11741.3, 
    farPlane=16230.9, width=4454.9, height=2250.67, cameraPosition=(-4188.67, 
    1012.82, -11634.4), cameraUpVector=(-0.491746, -0.603233, 0.62793), 
    cameraTarget=(2254.59, 604.269, 153.769), viewOffsetX=375.054, 
    viewOffsetY=337.649)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11512.5, 
    farPlane=16610.5, width=4368.09, height=2206.81, cameraPosition=(7582.24, 
    -9103.1, -8711.88), cameraUpVector=(-0.685517, -0.432301, 0.585817), 
    cameraTarget=(2603.64, 2.25671, -170.832), viewOffsetX=367.745, 
    viewOffsetY=331.069)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11517.7, 
    farPlane=16605.3, width=4370.06, height=2207.8, cameraPosition=(7158.04, 
    -9452.4, -8586.77), cameraUpVector=(0.112859, -0.326524, 0.938427), 
    cameraTarget=(2179.43, -347.047, -45.7209), viewOffsetX=367.91, 
    viewOffsetY=331.218)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11384.3, 
    farPlane=16738.8, width=6660.88, height=3365.15, viewOffsetX=530.673, 
    viewOffsetY=181.307)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10608.8, 
    farPlane=17573.8, width=6207.18, height=3135.94, cameraPosition=(13999.7, 
    -7826.13, 2312.67), cameraUpVector=(-0.279085, 0.403051, 0.871586), 
    cameraTarget=(2752.67, -682.257, 548.005), viewOffsetX=494.527, 
    viewOffsetY=168.957)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11123.3, 
    farPlane=17571.9, width=6508.19, height=3288.01, cameraPosition=(12132.7, 
    9875.05, 4413.7), cameraUpVector=(-0.441512, -0.394815, 0.805722), 
    cameraTarget=(3517.57, 304.25, 563.471), viewOffsetX=518.508, 
    viewOffsetY=177.15)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11431.8, 
    farPlane=17263.4, width=1549.22, height=782.682, viewOffsetX=-840.694, 
    viewOffsetY=-379.577)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11956.6, 
    farPlane=18118.4, width=1620.34, height=818.611, cameraPosition=(15467.5, 
    7443.36, 655.796), cameraUpVector=(-0.304162, -0.17596, 0.936228), 
    cameraTarget=(4028.2, 391.936, 399.45), viewOffsetX=-879.287, 
    viewOffsetY=-397.002)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11829.2, 
    farPlane=18245.9, width=3955.45, height=1998.34, viewOffsetX=-323.378, 
    viewOffsetY=463.61)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11789.3, 
    farPlane=18285.8, width=3942.11, height=1991.6, cameraPosition=(15366.9, 
    7599.79, 840.442), cameraUpVector=(-0.125465, -0.463704, 0.877062), 
    cameraTarget=(3927.64, 548.362, 584.096), viewOffsetX=-322.287, 
    viewOffsetY=462.046)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12342.3, 
    farPlane=16136.3, width=4127.03, height=2085.02, cameraPosition=(468.66, 
    13946.9, 2832.22), cameraUpVector=(-0.100503, -0.502919, 0.85847), 
    cameraTarget=(2339.29, 828.451, 584.773), viewOffsetX=-337.404, 
    viewOffsetY=483.719)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12375.5, 
    farPlane=16103, width=3683.8, height=1861.1, viewOffsetX=-681.677, 
    viewOffsetY=-117.156)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10317.8, 
    farPlane=16857.2, width=3071.26, height=1551.63, cameraPosition=(-9050.26, 
    7157.68, 2304.7), cameraUpVector=(0.302287, -0.3617, 0.881927), 
    cameraTarget=(2143.04, -79.6501, 579.867), viewOffsetX=-568.329, 
    viewOffsetY=-97.6752)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10538.5, 
    farPlane=16705.6, width=3136.96, height=1584.83, cameraPosition=(-7894.98, 
    4783.54, -6866.64), cameraUpVector=(-0.201677, 0.14781, 0.968235), 
    cameraTarget=(2160.31, -94.0623, 599.561), viewOffsetX=-580.487, 
    viewOffsetY=-99.7647)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11623.4, 
    farPlane=16307.7, width=3459.9, height=1747.98, cameraPosition=(-2666.17, 
    12886.3, -1202.92), cameraUpVector=(0.272771, -0.101842, 0.956673), 
    cameraTarget=(1968.82, 435.812, 833.178), viewOffsetX=-640.245, 
    viewOffsetY=-110.035)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11362.9, 
    farPlane=17427.2, width=3382.37, height=1708.81, cameraPosition=(11949.6, 
    10728.4, -312.555), cameraUpVector=(-0.232438, -0.144447, 0.961825), 
    cameraTarget=(2646.34, 1075.92, 647.548), viewOffsetX=-625.898, 
    viewOffsetY=-107.569)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11356, 
    farPlane=17718.7, width=3380.33, height=1707.78, cameraPosition=(16650.8, 
    2528.74, 2043.1), cameraUpVector=(-0.386893, -0.181336, 0.904119), 
    cameraTarget=(3377.65, 716.835, 955.083), viewOffsetX=-625.52, 
    viewOffsetY=-107.504)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11327.2, 
    farPlane=17781.6, width=3371.76, height=1703.45, cameraPosition=(16185.2, 
    2656.05, -3164.72), cameraUpVector=(0.00179991, -0.593262, 0.805007), 
    cameraTarget=(3540.5, 541.281, 870.402), viewOffsetX=-623.933, 
    viewOffsetY=-107.231)
session.viewports['Viewport: 2'].view.setValues(nearPlane=13138, 
    farPlane=15887.4, width=3910.78, height=1975.77, cameraPosition=(2559.52, 
    7163.32, -11953), cameraUpVector=(0.941671, -0.203866, 0.267757), 
    cameraTarget=(2587.72, 1014.28, -1.82373), viewOffsetX=-723.676, 
    viewOffsetY=-124.373)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12932.9, 
    farPlane=16092.4, width=7374.88, height=3725.87, viewOffsetX=-713.135, 
    viewOffsetY=-525.054)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11664.4, 
    farPlane=18160.7, width=6651.54, height=3360.43, cameraPosition=(-7581.42, 
    -6171.5, -8588.8), cameraUpVector=(0.657548, 0.512946, -0.55183), 
    cameraTarget=(1055.9, 196.39, -496.29), viewOffsetX=-643.19, 
    viewOffsetY=-473.557)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12888.6, 
    farPlane=16632, width=7349.62, height=3713.11, cameraPosition=(2870.9, 
    -8583.38, -11354.2), cameraUpVector=(0.640457, 0.755656, -0.137107), 
    cameraTarget=(2132.2, -136.391, -926.087), viewOffsetX=-710.693, 
    viewOffsetY=-523.257)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11521.8, 
    farPlane=17688.6, width=6570.23, height=3319.35, cameraPosition=(12998.6, 
    293.411, -9396.48), cameraUpVector=(-0.00160615, 0.88758, 0.460651), 
    cameraTarget=(2882.61, 735.7, -558.275), viewOffsetX=-635.327, 
    viewOffsetY=-467.768)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11108.4, 
    farPlane=18063.9, width=6334.48, height=3200.25, cameraPosition=(16206.9, 
    -4746.68, 457.971), cameraUpVector=(-0.148388, 0.449822, 0.880705), 
    cameraTarget=(3787.53, 377.87, 835.091), viewOffsetX=-612.531, 
    viewOffsetY=-450.984)
session.viewports['Viewport: 2'].view.setValues(nearPlane=11504.1, 
    farPlane=17712.9, width=6560.1, height=3314.24, cameraPosition=(11278.4, 
    -10504, 5666.05), cameraUpVector=(-0.19842, 0.61274, 0.76497), 
    cameraTarget=(3658.96, -298.734, 1371.9), viewOffsetX=-634.348, 
    viewOffsetY=-467.047)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12518, 
    farPlane=16766.9, width=7138.3, height=3606.35, cameraPosition=(1125.86, 
    -12816.9, 7708.37), cameraUpVector=(-0.220602, 0.722915, 0.654774), 
    cameraTarget=(2796.43, -824.978, 1873.52), viewOffsetX=-690.258, 
    viewOffsetY=-508.211)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12755.8, 
    farPlane=16612.7, width=7273.9, height=3674.86, cameraPosition=(1666.39, 
    -14693.6, 472.611), cameraUpVector=(-0.21308, 0.296291, 0.931026), 
    cameraTarget=(2859.21, -1332.26, 1305.47), viewOffsetX=-703.37, 
    viewOffsetY=-517.865)
session.viewports['Viewport: 2'].view.setValues(nearPlane=12359.6, 
    farPlane=17022.6, width=7047.96, height=3560.71, cameraPosition=(-511.775, 
    -14428.5, 391.049), cameraUpVector=(-0.0669163, 0.299403, 0.951777), 
    cameraTarget=(2726.71, -1411.29, 1230.94), viewOffsetX=-681.521, 
    viewOffsetY=-501.778)
session.viewports['Viewport: 2'].view.setProjection(projection=PARALLEL)
session.viewports['Viewport: 2'].view.setValues(session.views['Front'])
leaf = dgo.LeafFromOdbElementPick(elementPick=(('PART-1-1', 6356, (
    '[#7fc00f #7fe #0:2 #c0000020 #ff #0 #20', 
    ' #e0000000 #1fffff #0 #ffc0002 #fffffc00 #f #0', 
    ' #30 #0:7 #fffe0000 #7ff #0:5 #ffc00000 #ffff', 
    ' #0:155 #200000 #7c000000 #80400008 #100 #420006 #10080402', 
    ' #804020 #4700201 #f0000000 #ffffffff:8 #7ffff #fc #0:8', 
    ' #1fffff #4000000 #0:8 #4000000 #fffffff8 #ffffffff:11 #3ffffff', 
    ' #0:8 #fe000000 #ffffffff:18 #40201ff #0:2 #40201ff8 #80010080', 
    ' #c003 #8080808:2 #18 #80402c0 #ffffe010 #ffffffff:12 #ffc0003f', 
    ' #ffffffff:14 #ffffff #0:3 #1ffbfff8 #0:2 #fffffffe #ffffffff', 
    ' #3fffff #1c #0 #ffffff80 #7fffffff #5f0001fe #2010be3f', 
    ' #fffffc00 #ffffffff:12 #e003ffff #3ff003f #0:2 #100000 #7fe000', 
    ' #0 #100000 #0 #fffff000 #f #10000 #fe0007fe', 
    ' #7ffff #0 #180000 #0:8 #3ffffff #0:6 #7fffffe0', 
    ' #0:156 #10 #43e00 #804020 #30000 #2010021 #20100804', 
    ' #1008040 #238 #fffff800 #ffffffff:8 #7e0003 #0:8 #ffff8000', 
    ' #f #200 #0:8 #fffc0200 #ffffffff:12 #1ff #0:8', 
    ' #ffffff00 #ffffffff:17 #ffffff #201 #0 #ffc0000 #80402010', 
    ' #6001c000 #4040000 #4040404 #c0404 #1600000 #f0080402 #ffffffff:12', 
    ' #1fffff #ffffffe0 #ffffffff:14 #7f #0:2 #fffc0000 #ffd', 
    ' #0 #ffff0000 #ffffffff:2 #e001f #0 #ffc00000 #ffffffff', 
    ' #ff3fff #5f1faf80 #fe001008 #ffffffff:13 #604e4d #18 #0', 
    ' #9fffffe #ffffe054 #fffff9ff #ffffdfff #fbc347ff #2727807 #c003', 
    ' #0 #fffff000 #ff02a04f #ffcfffff #feffffff #1a3fffff #c03fde ]', )), ), )
session.viewports['Viewport: 2'].odbDisplay.displayGroup.remove(leaf=leaf)
session.viewports['Viewport: 2'].view.setValues(nearPlane=10388.9, 
    farPlane=15813.2, cameraPosition=(13634.8, 4359.43, 3248.24), 
    cameraUpVector=(-0.329239, 0.316841, 0.889502), cameraTarget=(2427.9, 
    15.855, 647.333))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10546.1, 
    farPlane=15656.2, width=3035.01, height=1533.32, cameraPosition=(13557.7, 
    4674.42, 3054.26), cameraTarget=(2350.84, 330.842, 453.35))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10658.3, 
    farPlane=15495.8, cameraPosition=(14177.4, -2372.5, 3121.91), 
    cameraUpVector=(-0.153232, 0.306713, 0.939387), cameraTarget=(2388.85, 
    -101.48, 457.501))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10760, 
    farPlane=15394.3, width=1346.65, height=680.344, cameraPosition=(14339.3, 
    -1900.89, 2807.38), cameraTarget=(2550.79, 370.132, 142.968))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10939.9, 
    farPlane=15391.8, cameraPosition=(14652.2, -1074.78, 1816.35), 
    cameraUpVector=(-0.108444, 0.241262, 0.964382), cameraTarget=(2569.45, 
    419.386, 83.8802))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10853.6, 
    farPlane=15478.1, width=3035.01, height=1533.32, cameraPosition=(14704.1, 
    -645.488, 1824.41), cameraTarget=(2621.38, 848.678, 91.9446))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10359.7, 
    farPlane=15323.7, cameraPosition=(12745.3, -6300.49, -489.648), 
    cameraUpVector=(0.046561, 0.0072227, 0.998889), cameraTarget=(2492.17, 
    475.668, -60.6935))
session.viewports['Viewport: 2'].view.setValues(nearPlane=10461.3, 
    farPlane=15222.1, width=1346.65, height=680.344, cameraPosition=(13092.6, 
    -5796.38, -151.383), cameraTarget=(2839.48, 979.78, 277.571))
session.viewports['Viewport: 2'].view.setValues(cameraPosition=(13092.6, 
    -5796.38, -151.383), cameraUpVector=(0.0391855, -0.00395849, 0.999224), 
    cameraTarget=(2839.48, 979.78, 277.571))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11422.1, 
    farPlane=15803, cameraPosition=(14697, 1972.93, 3882.75), cameraUpVector=(
    -0.282088, 0.0545921, 0.957834), cameraTarget=(2907.47, 1309.04, 448.536))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11303.9, 
    farPlane=15921.2, width=3570.6, height=1803.91, cameraPosition=(14756.8, 
    1780.08, 3714.65), cameraTarget=(2967.29, 1116.19, 280.439))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11719.3, 
    farPlane=16739.4, cameraPosition=(13690.4, 7497.52, 150.36), 
    cameraUpVector=(-0.0960277, 0.142132, 0.985179), cameraTarget=(2864.27, 
    1668.54, -63.902))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11838.8, 
    farPlane=16619.9, width=1584.3, height=800.404, cameraPosition=(13873.9, 
    7149.5, 345.83), cameraTarget=(3047.78, 1320.52, 131.568))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11706.1, 
    farPlane=16125.7, cameraPosition=(15082.7, 1245.23, 3717.15), 
    cameraUpVector=(-0.255766, 0.0176281, 0.966578), cameraTarget=(3211.9, 
    518.909, 589.286))
session.viewports['Viewport: 2'].view.setValues(width=1346.65, height=680.344, 
    cameraPosition=(15120.1, 1583.46, 3496.8), cameraTarget=(3249.26, 857.139, 
    368.941))
session.viewports['Viewport: 2'].view.setValues(nearPlane=11764.5, 
    farPlane=16273.8, cameraPosition=(15215.5, 2660.47, 2607.62), 
    cameraUpVector=(-0.202281, 0.0786748, 0.976162), cameraTarget=(3260.37, 
    982.394, 265.53))
