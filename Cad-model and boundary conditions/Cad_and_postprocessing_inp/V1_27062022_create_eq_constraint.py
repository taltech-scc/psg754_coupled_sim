import sys
import numpy as np
import pandas as pd


models=10    #The number of beam models in file
input_filename='multimodel.inp'

lookup = '*Node'
lookup2 = '*Element, type=C3D8R'
keyword_node=[]
#This loop finds the locations where node lists start
with open(input_filename) as f:
    for num, line in enumerate(f, 1):
        if lookup in line.rstrip():
            keyword_node.append(num)      # This is the line nr for *Nset, nset=BOTTOM
        if len(keyword_node) is models:
            break

#This loop extracts the nodes and puts them in pandas NODEidxyz file
model_nidxyz_list=[]  #one item in the list is df with one beam nodes, so list has the length of 10
Node=[]
inc_model=0
with open(input_filename) as f:
    for num, line in enumerate(f, 1):
        start1=keyword_node[inc_model]
        if num > start1:    # now inside the bottom set
            # print(line)
            if lookup2 in line:
                NODEidxyz = pd.DataFrame(Node)
                NODEidxyz.columns=['ID','x','y','z']
                model_nidxyz_list.append(NODEidxyz)
                Node.clear()
                inc_model+=1
                if inc_model == models:
                    break
                continue
            else:
                n_table= [float(x) for x in line.split(',') if x.strip(',')]
                Node.append(n_table) 
del NODEidxyz


Model_info = np.loadtxt('inp_model.txt')  #Teguh INP
#Model has dimension
length = Model_info [0]
breadth = Model_info [1]
thickness =Model_info [2]
# # face = INP[3]

x_Length=0.0149999997


nl = "\n"
n=open('Equation.inp','a')
n.write('*Equation \n')
#Functions for Equation constraints
def wr2(file,F1,F2,eq1,eq2,instance):
    for i in range(1,len(F1)-1,1):
        # print(F2.ID.iloc[i])
        Nodeid1=F1.ID.iloc[i].astype(int); Nodeid2=F2.ID.iloc[i].astype(int);
        file.write(f'3{nl}')
        file.write(f"{Nodeid1:5d},{eq1:2d}, 1.0, {Nodeid2:5d},{eq1:2d}, -1.0, DNORM_{instance}, {eq1:2d}, -1.0 {nl}")
        file.write(f'3{nl}')
        file.write(f"{Nodeid1:5d},{eq2:2d}, 1.0, {Nodeid2:5d},{eq2:2d}, -1.0, DSHEAR_{instance}, {eq2:2d}, -1.0 {nl}")

#Equation constrains not interacting with DSHEAR or DNORM
def eq_interact(file,F1,F2,eq1):
    for i in range(1,len(F1)-1,1):
        nl = "\n"
        # print(F2.ID.iloc[i])
        Nodeid1=F1.ID.iloc[i].astype(int); Nodeid2=F2.ID.iloc[i].astype(int);
        file.write(f'2{nl}')
        file.write(f"{Nodeid1:5d},{eq1:2d}, 1.0, {Nodeid2:5d},{eq1:2d}, -1.0{nl}")

def eq_corner(file,F1,eq1,eq2,instance):
    # For nodes B and D
    for node in F1.ID.astype(int):
        nl = "\n"
        # print(int(node))
        # Nodeid1=F1.ID.iloc[i].astype(int); Nodeid2=F2.ID.iloc[i].astype(int);
        file.write(f'2{nl}')
        file.write(f"{node:5d},{eq1:2d}, 1.0, DNORM_{instance}, {eq1:2d}, -1.0{nl}")
        file.write(f'2{nl}')
        file.write(f"{node:5d},{eq2:2d}, 1.0, DSHEAR_{instance}, {eq2:2d}, -1.0{nl}")

def eq_corner_c(file,F1,instance):
    # For nodes B and D
    for node in F1.ID.astype(int):
        nl = "\n"
        # print(int(node))
        # Nodeid1=F1.ID.iloc[i].astype(int); Nodeid2=F2.ID.iloc[i].astype(int);
        file.write(f'3{nl}')
        file.write(f"{node:5d},1, 1., DNORM_{instance}, 1, -1.0, DSHEAR_{instance}, 1, -1. {nl}")
        file.write(f'3{nl}')
        file.write(f"{node:5d},2, 1., DSHEAR_{instance}, 2, -1., DNORM_{instance}, 2, -1.{nl}")
        
        
for modelinstance,NODEidxyz in enumerate(model_nidxyz_list):
# def eq_constraints_per_instance():
    # y=0
    n.write(f'** Start instance nr {modelinstance}{nl}')
    All_z_coords=np.sort(NODEidxyz.z.unique())
    All_y_coords=np.sort(NODEidxyz.y.unique())
    
    
    #This is for the nodes on the left-A (Original model: node set A)
    NodesetA=NODEidxyz.loc[(NODEidxyz.x==0) & (NODEidxyz.y==All_y_coords[0])].sort_values('z')
    #This is for the nodes on the right-A (Original model: node set B)
    NodesetB=NODEidxyz.loc[(NODEidxyz.x==x_Length) & (NODEidxyz.y==All_y_coords[0])].sort_values('z')
    # y=element width
    #This is for the nodes on the left-B  (Original model: node set D)
    NodesetD=NODEidxyz.loc[(NODEidxyz.x==0) & (NODEidxyz.y==All_y_coords[1])].sort_values('z')
    #This is for the nodes on the right-B (Original model: node set C)
    NodesetC=NODEidxyz.loc[(NODEidxyz.x==x_Length) & (NODEidxyz.y==All_y_coords[1])].sort_values('z')
    # np.savetxt(r'Right-B.inp', Node_xL_yW.ID, fmt='%d')
    
    n.write(f'** Replace sets B,C,D nr {modelinstance}{nl}')
    # wr2(n,nrange_list_B[0],nrange_list_A[0],2,1)
    eq_corner(n,NodesetB,1,2,modelinstance)
    eq_corner(n,NodesetD,2,1,modelinstance)
    eq_corner_c(n,NodesetC,modelinstance)
    
    n.write(f'** Rest {modelinstance}{nl}')
    nrange_list_A=[]
    nrange_list_B=[]
    for ind,zcoord in enumerate(All_z_coords):
        nrange=NODEidxyz.loc[(NODEidxyz.y==All_y_coords[0]) & (NODEidxyz.z==zcoord)].sort_values('x')
        nrange_list_A.append(nrange)
        # np.savetxt(f"Bottom-A-{ind}.inp", nrange.ID, fmt='%d')
        nrangeB=NODEidxyz.loc[(NODEidxyz.y==All_y_coords[1]) & (NODEidxyz.z==zcoord)].sort_values('x')
        nrange_list_B.append(nrangeB)
        # np.savetxt(f"Bottom-B-{ind}.inp", nrangeB.ID, fmt='%d')
    
    

    
    ## Generate equations constraints

    
    wr2(n,nrange_list_B[0],nrange_list_A[0],2,1,modelinstance)            #Bottom row of nodes
    wr2(n,nrange_list_B[-1],nrange_list_A[-1],2,1,modelinstance)          #Top row of nodes 
    
    eq_interact(n,nrange_list_B[0],nrange_list_A[0],3)      #Bottom row of nodes
    eq_interact(n,nrange_list_B[-1],nrange_list_A[-1],3)    #Top row of nodes
    
    
    # These are for face elements
    for indx, (setA, setB) in enumerate(zip(nrange_list_A[1:-1], nrange_list_B[1:-1])):
        wr2(n,setB,setA,2,1,modelinstance)
        eq_interact(n,setB,setA,3) 
    n.write(f'** End instance nr {modelinstance}{nl}')
n.close() 
