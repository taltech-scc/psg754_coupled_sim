# -*- coding: utf-8 -*-
"""
Created on 13.09.2021 15:15:44

@author: Mihkel

Plot one simulation result based on the sequence number V_**** and the principal
stress direction wrt imperfection band (theta angle). All are bilinear simulations.
The sequence number interpretation: V_****-A_xx-B_yy
    **** - sequence number
    xx - cotfii at the first proportional loading (step A)
    yy - cotfii at the second proportional loading step (step B)

"""
# fff


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.signal import savgol_filter
# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division
pi=4*np.arctan(1)


fname=glob.glob('energy*')

for case in fname:
    E=pd.read_csv(case,delimiter= '\s+',index_col=False,
            names=['Time','Eout','Ein'],header=0,na_values='-nan(ind)')   
    fig,ax=pyp.newfigdef(3)
    ax.plot(E.Time,E.Ein,':x',label=f'{case}')         
    
ax.legend(loc='best')    