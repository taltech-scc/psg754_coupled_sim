#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  4 08:39:03 2021
Plots the critical principal stress angle with respect to imperfection band between
our analysis and those of BM. 
@author: mihkelkorgesaar
"""


import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt
import glob
import py_general as pyg
fig=plt.gcf(); ax=fig.axes
if ax==[]:
    plt.close(fig)
    fig, ax = plt.subplots(figsize=pyg.cm2inch(8, 8))
    print('create from scratch')
else:
    ax=ax[0]
    print('plot on exisiting')  
    




# data=glob.glob('datafile_critical_energy__BB*')
# data_mk=glob.glob('datafile_critical_energy_MK_*')
# d=np.loadtxt(data[0],skiprows=3)
# dmk=np.loadtxt(data_mk[0],skiprows=3)
# cot_fii=d[:,11]
# cot_fii_mk=dmk[:,11]

BMres=np.loadtxt('BMcritical.txt',skiprows=3)       #extracted from BM paper with plot digitizer






# convert alpha to polar angle
pi=4*np.arctan(1)
# polar=pi/2-np.arctan(cot_fii)
# polarMK=pi/2-np.arctan(cot_fii_mk)

# ax.plot(polar,d[:,1],'-ok',label='Muhammed')
ax.plot(BMres[:,0],BMres[:,1],'-or',label='Bonatti')
# ax.plot(polarMK,dmk[:,1],'-ob',label='Mihkel')


ax.set(ylabel='critical theta',xlabel='cotfii A')

a=np.linspace(pi/4,3*pi/4,41)

plt.show()
# da=a[1:]-a[:-1]
ax.legend(loc='best')