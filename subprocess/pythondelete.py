
import os
import sys
import glob
# import python_plotting as pyp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
import shutil


# Fnames=glob.glob('*/*/finalized_*')

# def deleteFile():
#     for f in glob.glob("*/*.for", recursive=True):    #deletes files in subdir only
#         os.remove(f)
def deleteFile():
    # time.sleep(2)
    for f in glob.glob("*/*/*.txt", recursive=True):    #deletes files only 2 sublevels deep
        os.remove(f)
    for f in glob.glob("*/**/*.bat", recursive=True):    #deletes files only 2 sublevels deep
        os.remove(f)
    for f in glob.glob("*/**/*.inp", recursive=True):    #deletes files in subdir and all lower level dirs
        os.remove(f)
    for f in glob.glob("*/**/*.py", recursive=True):    #deletes files in subdir and all lower level dirs
        os.remove(f)
    for f in glob.glob("*/*.for", recursive=True):    #deletes files in subdir only
        os.remove(f)
      
# deleteFile()


def deletemaster():
    # time.sleep(2)
    for f in glob.glob("*/*/MASTER*", recursive=True):    #deletes files only 2 sublevels deep
        os.remove(f)
deletemaster()