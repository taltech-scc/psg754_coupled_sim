"""
just a comment dfsfdsf
"""
import numpy as np
import os
import matplotlib.pyplot as plt
import sys
print (__name__)
import numpy as np
import fileinput
import io
import re
import glob
from contextlib import closing
# import cv2 as cv
# from scipy.stats import norm

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return (array[idx],idx)
    
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

def splitpath(pathin): #This splits the FDcurves2 path to file name and directory name
    (drive, path) = os.path.splitdrive(pathin)
    (path, folder)  = os.path.split(path)
    return (path,drive,folder)

def findsomething():
    ind=np.min(np.where((abs(T_e_ratio)>1)*(eps[:-1]>0.05)==True))
# fig=plt.gcf(); 
# ax=fig.gca()
# if ax==[]:
#     plt.close(fig)
#     fig, ax = plt.subplots(figsize=(5, 3))  

def read_external_data():
    exec(open("khea_info.txt").read())


def check_if_file_exists():
    if not os.path.exists('Results_combined.xlsx'):
    # Create an empty excel file
        writer = pd.ExcelWriter('Results_combined.xlsx')
        writer.close()   

def export_numpy_to_pandas_excel():
    #Copied from c:\Work\1_Research\Mikko\FE_modeling\
        ''' this section is used to export data to excel file
        1. Convert from np.array to pandas dataframe
        2. Concatenate to form columns in excel
        3. Write to excel using writer (mode append and specific engine)
        '''
        df_F=pd.DataFrame(F); df_Frame=pd.DataFrame(Fframe); df_d=pd.DataFrame(d)
        df_ph=pd.DataFrame([p_h,p_w,frac_step,d[frac_step],Fframe[frac_step]])
        df=pd.concat([df_F,df_Frame,df_d,df_ph], axis=1)
        with pd.ExcelWriter('Results_combined.xlsx',mode='a',engine='openpyxl') as writer: 
            df.to_excel(writer,sheet_name=pathna[10:],header=('F','Fframe','d','p_h,p_w,frac_step,d_frac,F_frac'))

def change_fortran(ksi,angle):
    # cwd=os.getcwd()
    # folder=os.path.basename(os.path.normpath(cwd))
    # texttoreplace='File'
    i=0
    # ksi=-0.8s
    with closing(fileinput.FileInput('vdisp.for',inplace = 1)) as file:
        for line in file:
            i+=1
            if i==9:
                print('      gamma='+ str(ksi)),
            elif i==14:
                print('      theta=' +str(angle) +'*pi/180'),
                # print('File=\'88'+folder+ '.inp\'')
            else:
                print(line,end='')
                
                
'''
# replace input file name inside the code and also
# the file name in the folder to reflect on the folder name

# cwd=os.getcwd()
# folder=os.path.basename(os.path.normpath(cwd))
# texttoreplace='File'

# Change the filename according to pattern
'''

def change_inp_write_solve(cpus,**input):
    '''
    
    '''

    # input['version']=20       - this way you can access different values
    for key in input: # see prindib **kwargs dictionary objekti
        print("%s = %s" % (key, input[key])) 
    if 'fname' in input:
        print('Input filename defined by user - NOT A FOLDER NAME')
        folder=input['fname']
    else:
        cwd=os.getcwd()
        folder=os.path.basename(os.path.normpath(cwd))
        # texttoreplace='File'
        
        # Change the filename according to pattern
        a=glob.glob('*.inp')        # get the current filename
        dst=folder+ ".inp"          # newfile name
        os.rename(a[0], dst)        # change the name
    if 'fortran' in input:
        fort_name=input['fortran']
    else:
        fort_name=glob.glob('*.for')  
    print('---------------------------------------------')
    # umat_name=glob.glob('*.for')
    print(folder)    
    # get fortran filname
     
    n=open('solveModel.bat',"w")
    
# call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2016.4.246\windows\bin\ifortvars.bat" intel64 vs2013
# call abq6141 job=Geers_so_mid_layer_fixed-03-04-2020 cpus=8 domains=8 memory="1000" parallel=domain double=explicit INTERACTIVE>out.txt
# REM call abq6141 cae noGUI=Extract.py
# del *.pac; *.prt; *.res; *.sel; *.stt; *.mdl; *.com;*.par;*.pes;*.pmg; *.abq;
# REM exit 
    if input['version']==14 and input['pc']=='mk':
        abaqus='abq6141'
        n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2016.4.246\windows\bin\ifortvars.bat" intel64 vs2013'); n.write('\n')
    elif input['version']==20 and input['pc']=='mk':
        abaqus='abq2020hf4'
        n.write(r'call "c:\Program Files (x86)\Intel\oneAPI\setvars.bat" intel64 vs2017'); n.write('\n')
        # n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2017.7.272\windows\bin\ifortvars.bat" intel64 vs2017'); n.write('\n')
    elif input['version']==21 and input['pc']=='ws':
        abaqus='abq2021hf6'
        n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2020.2.254\windows\bin\ifortvars.bat" intel64 vs2017'); n.write('\n')  
    elif input['version']==21 and input['pc']=='laptop':
        abaqus='abq2021hf6'
        n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2020.4.311\windows\bin\ifortvars.bat" intel64 vs2017'); n.write('\n')  
    elif input['version']==21 and input['pc']=='wshp':
        abaqus='abq2021hf6'
        n.write(r'call "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries_2020.4.311\windows\bin\ifortvars.bat" intel64 vs2017'); n.write('\n')  


    if not fort_name:
        try:
            if input['analysis']=='static':
                n.write('call ' +abaqus+ ' job=' +folder+ ' cpus='+str(cpus)+ ' memory="1000" double=explicit INTERACTIVE>out.txt\n')        
        except:
            n.write('call ' +abaqus+ ' job=' +folder+ ' cpus='+str(cpus)+ ' domains='+str(cpus)+' memory="1000" parallel=domain double=explicit INTERACTIVE>out.txt\n')   
    else:
        try:
            if input['analysis']=='static' and  input['theta']==0 and  input['singleEl']=='yes':    
                n.write('call ' +abaqus+ ' job=' +folder+ ' user=' +fort_name+ ' cpus='+str(cpus)+ ' memory="1000" double=explicit INTERACTIVE>out.txt\n')   
                n.write('call ' +abaqus+ ' job=Master-single.inp user=vdisp_BILINEAR-R1-1EL.for cpus=1 memory="1000" double=explicit INTERACTIVE>out-single.txt\n')   
            elif input['analysis']=='static' and  input['theta']==0 and  input['singleEl']=='no':   
                n.write('call ' +abaqus+ ' job=' +folder+ ' user=' +fort_name+ ' cpus='+str(cpus)+ ' memory="1000" double=explicit INTERACTIVE>out.txt\n')   
            else:
                n.write('call ' +abaqus+ ' job=' +folder+ ' user=' +fort_name+ ' cpus='+str(cpus)+ ' memory="1000" double=explicit INTERACTIVE>out.txt\n')   

        except:
            n.write('call ' +abaqus+ ' job=' +folder+ ' user=' +fort_name[0]+ ' cpus='+str(cpus)+ ' domains='+str(cpus)+' memory="1000" parallel=domain double=explicit INTERACTIVE>out.txt\n')          
        
  
    n.write('del *.pac; *.prt; *.res; *.sel; *.stt; *.mdl; *.com;*.par;*.pes;*.pmg; *.abq;\n')
    n.write('REM exit\n')
    n.close()   

# change_files(10,20,'mk')
if __name__=="__main__":
    fortran_name='vdisp_BILINEAR-R1.for'
    change_inp_write_solve(1,version=21,pc='ws',analysis='static',fname='MASTER.inp',fortran=fortran_name)