# -*- coding: utf-8 -*-
"""
Purpose:
Postprocess linear FLC analysis results and find the theta under which
localization happens first



   
Variable Name	            Variable description
--------------------------------------------------------------------------------------------------------------------------
theta	                    Direction of 1st principal strain wrt to imperfection band
df	                        Dataframe of damage containing damage.txt for each of the theta (0…90) in single stress state
Ind_plot	                Postprocess damage in form of index location, critical damage and localization label 
Dlt0	                    sub series of Ind_plot containing cases where there were damage did not reach 1
nr_of_data	                Length of the simulation in steps
r1	                        length of radial path until localization or max damage

v1-25.05.2022 MK
               
@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
sys.path.append('C:\work\ML_FLC\Scripts')
import Global_definitions as gd


pi=4*np.arctan(1)
Model='Model2_multimodel'
PathAfixed='no'  
linearflc='yes'   #linear FLC scenario  
criterion="damage" #strain 7772,19,9936
start_angle=0
end_angle=90
dalfa=10
angle_list = np.arange(start_angle,end_angle,dalfa)    #for linearflc
#--------------------- Start OF USER INPUT -------------------------------------

#Define the dataset to be analyzed

os.chdir('C:\work\ML_FLC')
# fol=glob.glob(Model+'/Bilinear/Dataset/')
if PathAfixed=='yes': #bi-linear path scenario with first path fixed (folder structure a bit differernt)

    fol=glob.glob(Model+'/LinearBeam_MM/')
    # fol=glob.glob(Model+'/NonLinear_paths/NL_V01-Theta/')
else:
    fol=glob.glob(Model+'/LinearBeam_MM-2')




os.chdir(fol[0])

if PathAfixed=='yes':

    os.chdir('theta_90')
    Fnames=glob.glob('V_060*')
    os.chdir("..")
else:  
# plt.ioff()
    Fnames=glob.glob('V_*') #strain 7772,19,9936

#--------------------- END OF USER INPUT -------------------------------------
tswitch_add=5 #oli 10

plotres='no'
if criterion=="energy":
    Fail='energy.rpt'
    Fail_1el='energy-1EL.rpt'
    namehead=["Time", "out", "inn"]
    resultfail='Localization-all-energy-13-05-2022.txt'
    resultfail_min='Localization-min-energy-13-05-2022.txt'
    # resultfail_min_use='Localization-min-energy-work.txt'
elif criterion=="strain":
    Fail='Principal_strains.rpt'
    Fail_1el='Principal_strains-1EL.rpt'   
    namehead=['Time','e1o','e2o','e3o','e1i','e2i','e3i']
    resultfail='TEST-Localization-all-strain.txt'
    resultfail_min='TEST-Localization-min-strain.txt'
elif criterion=="damage":
    Fail='Damage'
    Fail_1el='Damage-1EL.rpt'   
    namehead=['Time','Din','Dout']
    resultfail='Localization-all-Damage-29072022.txt'
    resultfail_min='Localization-min-Damage-29072022.txt'

if len(Fnames)==1:
    resultfail='TEST.txt'
    resultfail_min='TESTmin.txt'

naverage=3          #This is the value over which running mean is calculated
dt=0.0025

viridis = cm.get_cmap('rainbow', 21)
map=viridis(range(21))



n=open(resultfail,"a")
n.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}\n"
        .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Dcrit','Fail'))
n_min=open(resultfail_min,"a")
n_min.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}\n"
        .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Dcrit','Fail'))




def plot_energy_curve(E,indx,angle,ax):
    ax.plot(E.Time,E.inn,'--',linewidth=0.5,label=angle)
    ax.plot(E.Time,E.out,'--',linewidth=0.5)
    ax.plot(E.Time[indx],E.out[indx],'o',linewidth=0.5)
    ax.legend(loc='best')  
    # if angle==0:
    #     ax.plot(E.t,E.out,'-k',linewidth=0.6)    
    
# sys.exit()
for i,fol in enumerate(Fnames):
    # if i<18395:
    #     continue
    # if PathAfixed=='yes' and linearflc=='yes':
    #     angle_list = np.arange(0,42,2)
    # else:
    
    angle_list_working=[]
    ind_length=[]; 
    missing=""
    indxls_min=[]
    e1_list=[]; e2_list=[];# e1B_list=[]; e2B_list=[]
    r1_list=[]
    if PathAfixed=='no':
        os.chdir(fol)
    if plotres=='yes':
        fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
        ax=fig.axes[0]; ax1=fig.axes[1]
    
    # if postprocessed=='yes':
    #     fols=glob.glob('theta*')
    #     for ith,angle in enumerate(fols):    
    # else:   
        
        
    for ith,angle in enumerate(angle_list):
        # fig2, ax2 = plt.subplots(figsize=pyp.cm2inch(8, 8))
        if PathAfixed=='no':
            f=open(f'alfa_{angle}_{angle+dalfa}/D_BI-MM.for')
        lines=f.readlines()
        r1=float(lines[21].split('=')[1].replace("\n",""))
        r2=0
        # cotfii_a=float(lines[21].split('/')[1].replace("\n","")); cotfii_b=float(lines[25].split('=')[1].replace("\n",""))
        # txt=lines[20].split('/')[1].replace(",","")
        theta=[int(s) for s in lines[20].split('/')[1].replace(",","").split() if s.isdigit()]
        cotfii_a=float(lines[24].split('=')[1].replace("\n","")); cotfii_b=float(lines[25].split('=')[1].replace("\n",""))
        tswitch_step=int(r1/(r1+r2)/0.0025)+tswitch_add
        if linearflc=='yes':
            tswitch_step=6
        # try:
        path=f'alfa_{angle}_{angle+dalfa}/{Fail}*'
        flist=glob.glob(path)
        # R=pd.read_csv(flist[0],delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
        dfs = []
        for indk,file in enumerate(flist):
            dfs.append(pd.read_csv(file, delimiter= '\s+',names=[f'Time_{angle+indk}',f'Din_{angle+indk}',f'Dout_{angle+indk}'],header=0,na_values='-nan(ind)'))
        df = pd.concat(dfs, axis=1)  #concatenate the list into single dataframe
        filter_col = [col for col in df if col.startswith('Di')] #list of names starting with Di
        
        Ind_plot=pd.DataFrame(columns=['MaxIndx','CritD','Local'])
        
        Ind_plot.MaxIndx=(df[filter_col] >= 1).idxmax()     #gives the index where damage >1
        Ind_plot.Local=('True')                              #Specify that in all cases localization happened (default)
        # ind_plot=(df[filter_col] >= 1).idxmax()         #Find the index from Din where value larger than 1
        
        # Create a series where maxindex =0, meaning damage has not reached 1.
        Dlt0=Ind_plot.loc[(Ind_plot.MaxIndx<1)]
# No localization cases, max index is the length of the analysis   
        Ind_plot.MaxIndx[Dlt0.index]=(df[Dlt0.index]).idxmax()
# No localization cases,set localization variable to zero          
        Ind_plot.Local[Dlt0.index]=('False')  
# not perfect, but creates matrix where it looks from the original dataset values based on MaxIndx. Result is a matrix w
# which diagonal is the damage value we are after (maxd=)
        dd=df[Ind_plot.index].iloc[Ind_plot.MaxIndx]      
        Ind_plot.CritD=np.diag(dd)
        

        #Next lines are needed since sometimes damage does not reach the level of 1, then use the max value instead
        #----------------------------------------------
        #np.max(df[filter_col])             # gives the max damage value in each Din
        # (np.max(df[filter_col])).idxmax() # gives the name of the max from the Din range
        # ind_plot[ind_plot0.index]=np.max(df[ind_plot0.index]) #the initial list obtained, but replace only D values that were less than 1
        # ind_plot[ind_plot0.index]=(np.max(df[ind_plot0.index]))     #gives the value
        # ind_plot[ind_plot0.index]=(df[ind_plot0.index]).idxmax()    #gives the index of max value
        # zero_max_damge=(np.max(df[ind_plot0.index])).idxmax()
        
        nr_of_data=(df[filter_col]).idxmax()  # gives the length of the datapoints
        # indx=ind_plot
        indxls_min.append(Ind_plot)
        ind_length.append(nr_of_data)
        
        # ind_plot=[]
        # for column in df[filter_col]:
        #     ind_plot.append(np.max(np.where((df[column]<1)))-1)
        # e1A,e2A,e1,e2,e1B,e2B=gd.strain_state(r1,r2,cotfii_a,cotfii_b,ind_plot,dt)
        if linearflc=='yes':
            r1=r1*Ind_plot.MaxIndx/399  #r1 length normalized with respect to index of localization and ??? shouldnt do like this, because expected length is still 400???
            r1_list.append(r1)
        for i,thetai in enumerate(theta):
            # print(i,thetai)
            n.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:>8}\n".
                  format(fol,thetai,int(Ind_plot.MaxIndx[i]),nr_of_data[i],cotfii_a,0,r1[i],Ind_plot.CritD[i],Ind_plot.Local[i]))  
        # except:
        #     print('Done')
    dfind = pd.concat(indxls_min, axis=0)
    theta_lengths= pd.concat(ind_length, axis=0)
    r1df=pd.concat(r1_list, axis=0)
    dfind['rlen']=r1df
    if len(dfind.index[dfind.Local=='True']):
        # print('some lcoalization cases')
        dfind_min=dfind[(dfind.Local=='True')].rlen.idxmin()
    else: #no localization
        # print('no localization')
        dfind_min=dfind.CritD.idxmax()
    theta_min=int(dfind_min.split('_')[1])
    n_min.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:>8}\n".
        format(fol,theta_min,int(dfind.MaxIndx[dfind_min]),theta_lengths[dfind_min],cotfii_a,0,dfind.rlen[dfind_min],dfind.CritD[dfind_min],dfind.Local[dfind_min])) 
    os.chdir("..") 
n.close()  
n_min.close()    
# n_min_all.close()    



# dfs = []
# for indx,file in enumerate(flist):
#     dfs.append(pd.read_csv(file, delimiter= '\s+',names=[f'Time_{angle+indx}',f'Din_{angle+indx}',f'Dout_{angle+indx}'],header=0,na_values='-nan(ind)'))

