# -*- coding: utf-8 -*-
"""
@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import bisect  







pi=4*np.arctan(1)


#-------------------------
# This is how the simulation has been defined (same parameters need to be used here)
start_angle=0
end_angle=80
dalfa=20
angle_list = np.arange(start_angle,end_angle,dalfa)    #for linearflc
forname='vdisp_NL_MM.for'
#--------------------- DATASET INFORMATIONS -------------------------------------
#Define the dataset to be analyzed
Model='Model2_multimodel'
criterion="damage"
os.chdir('C:\work\ML_FLC')
fol=glob.glob(Model+'/Complex_mm_2')



os.chdir(fol[0])
Fnames=glob.glob('V_*') #strain 7772,19,9936

#--------------------- END OF USER INPUT -------------------------------------
tswitch_add=5 #oli 10

plotres='no'
if criterion=="damage":
    Fail='Damage'
    Fail_1el='Damage-1EL.rpt'   
    namehead=['Time','Din','Dout']
    resultfail='rerun.txt'


if len(Fnames)==1:
    resultfail='TEST.txt'
    resultfail_min='TESTmin.txt'

naverage=3          #This is the value over which running mean is calculated
dt=0.0025

viridis = cm.get_cmap('rainbow', 21)
map=viridis(range(21))



n=open(resultfail,"a")
n.write(f"{'Folder':34}\n")




def con(f):
    res=pi/2.-np.arctan(f+1e-10)
    return res
def plot_energy_curve(E,indx,angle,ax):
    ax.plot(E.Time,E.inn,'--',linewidth=0.5,label=angle)
    ax.plot(E.Time,E.out,'--',linewidth=0.5)
    ax.plot(E.Time[indx],E.out[indx],'o',linewidth=0.5)
    ax.legend(loc='best')  
    # if angle==0:
    #     ax.plot(E.t,E.out,'-k',linewidth=0.6)    
    
# sys.exit()
for i,fol in enumerate(Fnames):
    if i<884:
        continue
    
    angle_list_working=[]
    ind_length=[]; 
    missing=""
    indxls_min=[]
    e1_list=[]; e2_list=[];# e1B_list=[]; e2B_list=[]
    rclist=[]
    failstage_ls=[]

    os.chdir(fol)
    if plotres=='yes':
        fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
        ax=fig.axes[0]; ax1=fig.axes[1]
    
    # if postprocessed=='yes':
    #     fols=glob.glob('theta*')
    #     for ith,angle in enumerate(fols):    
    # else:   
        
        
    for ith,angle in enumerate(angle_list):
        # fig2, ax2 = plt.subplots(figsize=pyp.cm2inch(8, 8))
        f=open(f'alfa_{angle}_{angle+dalfa}/{forname}')
        lines=f.readlines()
        rarr=[float(s) for s in lines[82].split('/')[1].split(',')]
        fiarr=[float(s) for s in lines[83].split('/')[1].split(',')]
        theta=[int(s) for s in lines[20].split('/')[1].replace(",","").split() if s.isdigit()]
        path=f'alfa_{angle}_{angle+dalfa}/{Fail}*'
        flist=glob.glob(path)
        dfs = []
        for indk,file in enumerate(flist):
            ang=int(angle+dalfa/10*indk)
            dfs.append(pd.read_csv(file, delimiter= '\s+',names=[f'Time_{ang:d}',f'Din_{ang:d}',f'Dout_{ang:d}'],header=0,na_values='-nan(ind)'))
        df = pd.concat(dfs, axis=1)  #concatenate the list into single dataframe
        filter_col = [col for col in df if col.startswith('Di')] #list of names starting with Di (the imperfection zone)
        #Create a dataframe to hold the results       
        Ind_plot=pd.DataFrame(columns=['MaxIndx','CritD','Local','Fstage'])        
        Ind_plot.MaxIndx=(df[filter_col] >= 1).idxmax()     #gives the index where damage >1
        Ind_plot.Local=('True')                             #Specify that in all cases localization happened (default)
#----------------------------------------------------
# This portion of the code determines weather localization happened or not (damage > 1)
#----------------------------------------------------
        # Create a series where maxindex =0, meaning damage has not reached 1.
        Dlt0=Ind_plot.loc[(Ind_plot.MaxIndx<1)]
        # No localization cases, max index is the length of the analysis   
        Ind_plot.MaxIndx[Dlt0.index]=(df[Dlt0.index]).idxmax()
        # No localization cases,set localization variable to False
        Ind_plot.Local[Dlt0.index]=('False')  
#----------------------------------------------------
#Determine critical damage
#----------------------------------------------------
# not perfect, but creates matrix where it looks from the original dataset values based on MaxIndx. Result is a matrix w
# which diagonal is the damage value we are after (maxd=)
        dd=df[Ind_plot.index].iloc[Ind_plot.MaxIndx]      
        Ind_plot.CritD=np.diag(dd)

        
        nr_of_data=(df[filter_col]).idxmax()  # gives the length of the datapoints (should be 400)
        # Put ind_plot dataframe to list. The list contains 10 simulations (for each theta value )
        indxls_min.append(Ind_plot)
        ind_length.append(nr_of_data)
#----------------------------------------------------
#plot the path for specific case (only in debug mode, uncomment the section)
#----------------------------------------------------
# find correspoding strain value  (for plot)   
        ep1,ep2,tsw=gd.applied_strain_hist_long(dt,rarr,fiarr)
        failstage=3
        try:
            if ep1[int(Ind_plot.MaxIndx[0])]<ep1[int(sum(tsw[:-1])*400)]:
                # print('failure in 2nd stage')
                failstage=2
            Ind_plot.Fstage=failstage
        except:
            n.write(f"{fol:34}\n")  
            print(fol)
            break
    os.chdir("..") 
n.close()  

