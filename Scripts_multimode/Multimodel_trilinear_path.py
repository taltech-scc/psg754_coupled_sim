# -*- coding: utf-8 -*-
"""
Created on 13.09.2021 15:15:44

@author: Mihkel
Script applies to random data folders. 

Plots the strain state at the end of first (figure1) and second (figure2) loading step. 

Plot is made based on the critical index determined with script Critical_index_PathB.py, outputs minindex.txt

"""
# fff


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle
pi=4*np.arctan(1)



def con(f):
    res=pi/2.-np.arctan(f+1e-10)
    return res
def energy_plot(R,case,fig,ax,**singleenergy):
    defaultKwargs = { 'singleenergy': 'no'}
    kwargs = { **defaultKwargs, **singleenergy}  
    
    '''
    Energy plot
    '''    
    Fname_seg_A=glob.glob(case)
    R_critical=R.loc[(R.Fol==Fname_seg_A[0].split('\\')[-1])].iloc[0]
    dt=0.0025
    failind=R_critical.Ind
    Ehist=R_critical.Dfile.replace('Damage', 'energy')
    Files1=glob.glob(f'{Fname_seg_A[0]}/{Ehist}')[0]
    ener_in=np.loadtxt(Files1,skiprows=3,usecols = (2))
    ener_out=np.loadtxt(Files1,skiprows=3,usecols = (1))
    time=np.loadtxt(Files1,skiprows=3,usecols = (0))

       
    if len(Files1)>10:
        ax.plot(time,ener_in,'-k')#,label='1 element')            
    else:   
        ax.plot(time[failind],ener_in[failind],'or',markersize=4)
        ax.plot(time,ener_in,'-')#'In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        ax.plot(time,ener_out,'--')#'Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        ax.grid(which='major') 
    ax.set(xlabel='Time',ylabel='Energy')
    ax.legend(loc='best')      

    
    if  kwargs['singleenergy']=='yes':
        En1file=glob.glob(f'{Fname_seg_A[0]}/single/energy-1EL.rpt')[0]
        E1=np.loadtxt(En1file,skiprows=3,usecols = (1))
        time1=np.loadtxt(En1file,skiprows=3,usecols = (0))
        ax.plot(time1,E1,'-r')#'In '+' A='+str(Phi_A)+' B='+str(Phi_B))      
    os.chdir("../..")
    
def specific_case_path(R,case,fig,ax):
    # Exclud cases where failure was in second stage
    # R=R.loc[(Ra.fiA==0.7) & (Ra.Ra<0.2) & (Ra.Ra>0.1)] #Cluster 2
    Fname_seg_A=glob.glob(case)
    R_critical=R.loc[(R.Fol==Fname_seg_A[0].split('\\')[-1])].iloc[0]
    rarr=[R_critical.Ra,R_critical.Rb, R_critical.Rc]
    fiarr=[R_critical.fiA,R_critical.fiB, R_critical.fiC]
    failind=R_critical.Ind
   # add circles
    # theta=np.arange(0,np.pi,0.01)
    # e1Ra=R_critical.Ra*np.sin(theta); e2Ra=R_critical.Ra*np.cos(theta); 
    # e1tot=R_critical.Ra*np.sin(con(R_critical.fiA))+R_critical.Rb*np.sin(con(R_critical.fiB))
    
    # Rctot=Rbtot+R_critical.Rcneck*np.sin(con(R_critical.fiC))

    # e1Rb=Rbtot*np.sin(theta); e2Rb=Rbtot*np.cos(theta)
    # # rc=sum(rarr)*Ind_plot.MaxIndx/400-sum(rarr[:-1])   
    # e1Rc=Rctot*np.sin(theta); e2Rc=Rctot*np.cos(theta)
    # ax.plot(e2Ra,e1Ra,'--')
    # ax.plot(e2Rb,e1Rb,'--')
    # ax.plot(e2Rc,e1Rc,'--')
    
    dt=0.0025
    ep1,ep2,tsw=gd.applied_strain_hist_long(dt,rarr,fiarr)    
    # start of third path coordinates
    e2start_seg_3=ep2[int(sum(tsw[:-1])*400)]
    e1start_seg_3=ep1[int(sum(tsw[:-1])*400)]
    
   

    ax.plot(ep2,ep1,'--k')
    ax.plot(ep2[:int(sum(tsw[:-1])*400)],ep1[:int(sum(tsw[:-1])*400)],'-r',linewidth=4,alpha=0.3)
    ax.plot(e2start_seg_3,e1start_seg_3,'or')
    ax.plot(ep2[failind],ep1[failind],'x',markersize=3)
    if ep1[failind]<ep1[int(sum(tsw[:-1])*400)]:
       print('early failure')
       
    # compcase='V_0201*'
    # plot_path_single_case_dataset(data,compcase,fig,ax)
    # only_criticalA(fig,ax,fig_e,ax_e)
    
    strainhist=R_critical.Dfile.replace('Damage', 'Principal_strains')
    Files1=glob.glob(f'{Fname_seg_A[0]}/{strainhist}')
    e1i=np.loadtxt(Files1[0],skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
    e2i=np.loadtxt(Files1[0],skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
    e3i=np.loadtxt(Files1[0],skiprows=3,usecols = (6))
    ax.plot(e3i,e1i,':r')  
    ax.plot(e2i,e1i,':b')   
    ax.set(title=case)
    return (e2start_seg_3,e1start_seg_3)


def create_figs_all(R):
    plt.ioff()
    for indx,case in enumerate(R.itertuples(index=False)):  
        if indx<232:
            continue
        fig= pickle.load(open('FLC-base.fig.pickle', 'rb'))
        ax=plt.gca()
        e2start_seg_3,e1start_seg_3=specific_case_path(R,case.Fol,fig,ax);   #energy_plot(R,case.Fol,fig_e,ax_e,singleenergy='no')
        e2_bi_end,e1_bi_end=add_bilinear(R,case.Fol,fig,ax)
        # Calc eucledian distance
        dis=np.sqrt((e2start_seg_3-e2_bi_end)**2+(e1start_seg_3-e1_bi_end)**2)
        # if dis<0.1 or dis>1000:
        #     continue
        # print(case.Fol)
        # os.chdir("..")
        plt.savefig(f'{case.Fol}.png',dpi=500,transparent=False) 
        plt.close(fig)

def add_bilinear(R,case,fig,ax):
    Bilinearres="Localization-min-Damage-02-05-2022-bilinear.txt"  
    BIDS=pd.read_csv(glob.glob(Bilinearres)[0],delimiter= '\s+',index_col=False,
         names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
    
    #Trilinear results
    Fname_seg_A=glob.glob(case)
    R_critical=R.loc[(R.Fol==Fname_seg_A[0].split('\\')[-1])].iloc[0]
    rarr=[R_critical.Ra,R_critical.Rb, R_critical.Rc]
    fia=R_critical.fiA;  fib=R_critical.fiB
    Ra=R_critical.Ra
    #-------------------------
    
    Rbi=BIDS.loc[(BIDS.fiA==fia) & (BIDS.fiB==fib) & (BIDS.Ra<1.1*Ra) & (BIDS.Ra>0.9*Ra)]  
    dt=0.0025
    for indx,case in enumerate(Rbi.itertuples(index=False)):
        ep1,ep2,tsw=gd.applied_strain_hist_long(dt,[case.Ra,case.Rbtot],[case.fiA,case.fiB])
        ax.plot(ep2,ep1,'--b',linewidth=0.3)
        ax.plot(ep2[case.Ind],ep1[case.Ind],'og',markersize=3)
    try:
        return (ep2[case.Ind],ep1[case.Ind])
    except:
        return (10000,10000)


def copy_postprocessed_data(R):        
    R=R.set_index('Fol') #Use folder names as index
    piltok=pd.read_excel('Image_postprocessed.xlsx',sheet_name="Sheet2",header=0,index_col=0)
    R['image']=piltok.res           #Append this image info to dataframe
    R=R.loc[(R.image=='ok')]
    R.to_excel('Postprocessed-trilinear2testnew.xlsx',  index=True)


if __name__ == "__main__":
    # function for results in the paper from bilinear dataset
    # plt.ioff()
    plt.ion()
    fig= pickle.load(open('FLC-base.fig.pickle', 'rb'))
    ax=plt.gca()
    fig_e,ax_e=pyp.newfigdef(2)
    Model='Model2_multimodel/Complex_MM'
    fol=glob.glob(f'C:/work/ML_FLC/{Model}')
    os.chdir(fol[0])
    res_file=glob.glob('Localization-min-Damage-11-08-2022-trilinear.txt')
    Ra=pd.read_csv(res_file[0],delimiter= '\s+',index_col=False,
       names=['Fol','theta','Ind','Elength','fiA','fiB','fiC','Ra','Rb','Rc','Rcneck','D','Necking','Stage','Dfile'],header=0) 
    R=Ra.loc[(Ra.Stage==3) & (Ra.Necking==True) & (Ra.fiA!=Ra.fiB) & (Ra.Rcneck>0.06)] #Cluster 2
    copy_postprocessed_data(R)
    


    #1 test case---------------------
    # case='V_0564*'
    # case='V_0663*'
    # case='V_1491*'
    # specific_case_path(R,case,fig,ax);  energy_plot(R,case,fig_e,ax_e,singleenergy='no')     
    # add_bilinear(R,case,fig,ax)
    # end one test case-------------------------
    
#Plot all cases    
    # plt.close(fig)
    # create_figs_all(R)


