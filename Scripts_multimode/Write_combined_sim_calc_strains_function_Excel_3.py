

'''
This file reads the strain data from bilinear dataset and puts it to desired format:
such as time, corfii, and strain data. 
10.03.2022
First this was made on a data which included also equivalent plastic strain (c:\work\ML_FLC\Model2\Bilinear\Dataset\Postprocessed\).
Note that this data was generated based on c:\work\ML_FLC\Model2\Bilinear\Dataset critical theta values. HOwever, the 
re-simulations sometimes ended earlier, so the critical index values in POSTPROCESSED_ENERGY_DATA.txt are now always correct. 

18.03.2022
The second run of this script was made on the original dataset folder. This did not include the output of plastic strain,
but this was calculated manually. 



'''


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts_multimode/')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division
pi=4*np.arctan(1)


def postprocess_sim_to_strain_and_cotfii(R,analysistype,dirname):
    
    
    for ind,e in enumerate(R.itertuples(index=True)):
        # if e.Index=='V_4890-f1_0.95_f2_0.50_f3_0.80':
        # if e.Index=='V_0004-f1_-1.00_f2_-0.95_f3_-0.55':
            if analysistype=='random':      
                strainhist=e.Dfile.replace('Damage', 'Principal_strains')
                fi1=glob.glob(f'{e.Index}/{strainhist}')
                damagef=glob.glob(f'{e.Index}/{e.Dfile}')
            elif analysistype=='single':
                fi1=glob.glob(f'{e.Fol}/Principal_strains.r*')
                damagef=glob.glob(f'{e.Fol}/Damage.r*')
            else:
                # fi1=glob.glob('theta_'+str(e.theta)+'/'+e.Fol+'/'+'Principal_strains.r*')
                fi1=glob.glob(f'{e.Fol}/{e.file[:-11]}Principal_strains{e.file[-5:]}')
                damagef=glob.glob(f'{e.Fol}/{e.file}')
                
            # try:
            #     F=pd.read_csv(fi1[0],delimiter= '\s+',index_col=False,
            #             names=['Time','e1out','e2out','e3out','e1','e2','e3','epsout','eps','Tout','Tin'],header=0)   
            # except:
            F=pd.read_csv(fi1[0],delimiter= '\s+',index_col=False,
                        names=['Time','e1out','e2out','e3out','e1','e2','e3'],header=0)  
            Ds=pd.read_csv(damagef[0],delimiter= '\s+',index_col=False, names=['Time','Din','Dout'],header=0)  
            rarr=[e.Ra,e.Rb]
            fiarr=[e.fiA,e.fiB]
            failind=e.Ind
            ep1,ep2,tsw,Ri_list,Phi=gd.applied_strain_hist_long(0.0025,rarr,fiarr,'extra')   

            Triax_calc=1/np.sqrt(3)*(1+Phi)/np.sqrt(1+Phi+Phi**2)
            ep3=-(ep2+ep1) #from compressibility condition
            eps_calc=np.sqrt(2)/3*np.sqrt((ep1-ep2)**2+(ep1-ep3)**2+(ep2-ep3)**2)
            results=pd.DataFrame()   
            results['Time']=F.Time
            results['cotfii']=Phi[:len(F.Time)]
            results['e1sim']=F.e1
            results['e2sim']=F.e2
            results['e3sim']=F.e3
            # results['epssim']=F.eps
            # results['Tsim']=F.Tin
            results['e1_applied']=ep1[:len(F.Time)]
            results['e2_applied']=ep2[:len(F.Time)]
            results['e3_incomp']=ep3[:len(F.Time)]
            results['eps_applied']=eps_calc[:len(F.Time)]
            results['T_applied']=Triax_calc[:len(F.Time)]
            results['D_sim']=Ds.Din
            results['D_calc']=eps_calc[:len(F.Time)]/eps_calc[int(failind)]
            
            fig,ax=pyp.newfigdef(1)
            ax.plot(F.Time,F.e1); ax.plot(F.Time,ep1[:len(F.Time)])
            ax.plot(F.Time[failind],F.e1[failind],'o')
            fig2,ax2=pyp.newfigdef(2)
            ax2.plot(F.Time,F.e2); ax2.plot(F.Time,ep2[:len(F.Time)])
            ax2.plot(F.Time,ep3[:len(F.Time)],'--k')
            # res_file=e.Fol+'/theta_'+str(e.theta)+'/'+e.Fol+'_Res_postpocessed.xlsx'
            # res_file2='postprocessed_res/'+e.Fol+'_Res_postpocessed.xlsx'
            if ind==0:
                os.mkdir(dirname)
            # res_file2=dirname+'/'+e.Index+'_Res_postpocessed.xlsx'
            # res_file2=e.Index+'_Res_postpocessed.xlsx'    #This version was used in c:\work\ML_FLC\Model2_multimodel\Complex_MM\Rerun-400-data-points\
            # os.mkdir('Excelres')
            res_file2=f'{dirname}/{e.Fol}_Res_postpocessed.xlsx'
            # results.to_excel(res_file,  index=None)
            results.to_excel(res_file2,  index=True)
            
if __name__ == "__main__":
    
    postprocess_random=0
    postprocess_fixed1=1
    
    # input 1------------------------------------------
    if postprocess_random==1:
        Model='Model2_multimodel'
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear/Dataset')
        fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Complex_MM/Rerun-400-data-points/')
        os.chdir(fol[0])
        
        anal2='Localization-index.txt'
        R=pd.read_csv(glob.glob(anal2)[0],header=0,index_col=0)
        
        dt=0.0025 
        time=np.arange(0,1+dt,dt)
        postprocess_sim_to_strain_and_cotfii(R,'random','Excel_combined2')
    
    # input 2------------------------------------------  
    if postprocess_fixed1==1:
        Model='Model2'
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15')
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/BI-A-fix-V_000-a_0.70-T_0.663_R1-0.15')
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/BiLinear_A_Fixed/BI-A-fix-V_000-a_0.80-R1_0.4')
        fol=glob.glob('c:\work\ML_FLC\Model2_multimodel\BiLinear_A_Fixed_MM\BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15-NEW')
        os.chdir(fol[0])
        
        # anal2='Localization-BI-*.txt'
        # anal2='Localization-min-Damage*.txt'
        anal2='Bilinear-results-strain.txt'


        # R=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',index_col=False,
        #         names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
        R=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',index_col=False,
                names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','Dcrit','e1A','e2A','e1f','e2f','stage','file'],header=0)
                
        dt=0.0025 
        time=np.arange(0,1+dt,dt)
        postprocess_sim_to_strain_and_cotfii(R,'fixed','Excel_combined_damage')