# -*- coding: utf-8 -*-
"""
testing 23:38
Purpose:
Postprocess multisegmented (3) linear paths


Variable Name	            Variable description
--------------------------------------------------------------------------------------------------------------------------
rarr	                    simulated radial paths (their lengths) from fortran file
fiarr	                    simulated radial directions from fortran file
theta                       simulated principal strain directions from fortran file
dfind	                    Dataframe including index, damage, localization info, and rc length per main folder (all thetas)


v1-30.07.2022 MK
               
@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import bisect  



plt.ioff()
# plt.ion()



pi=4*np.arctan(1)


#-------------------------
# This is how the simulation has been defined (same parameters need to be used here)
start_angle=0
end_angle=40
dalfa=10
angle_list = np.arange(start_angle,end_angle,dalfa)    #for linearflc
forname='vdisp_NL_MM.for'
#--------------------- DATASET INFORMATIONS -------------------------------------
#Define the dataset to be analyzed
Model='Model2_multimodel'
criterion="damage"
os.chdir('..')
fol=glob.glob(Model+'/Complex_MM_3FLC/FLC3-1/')



os.chdir(fol[0])
Fnames=glob.glob('V_*') #strain 7772,19,9936

#--------------------- END OF USER INPUT -------------------------------------
tswitch_add=5 #oli 10

plotres='no'
if criterion=="energy":
    Fail='energy.rpt'
    Fail_1el='energy-1EL.rpt'
    namehead=["Time", "out", "inn"]
    resultfail='Localization-all-energy-13-05-2022.txt'
    resultfail_min='Localization-min-energy-13-05-2022.txt'
    # resultfail_min_use='Localization-min-energy-work.txt'
elif criterion=="strain":
    Fail='Principal_strains.rpt'
    Fail_1el='Principal_strains-1EL.rpt'   
    namehead=['Time','e1o','e2o','e3o','e1i','e2i','e3i']
    resultfail='TEST-Localization-all-strain.txt'
    resultfail_min='TEST-Localization-min-strain.txt'
elif criterion=="damage":
    Fail='Damage'
    Fail_1el='Damage-1EL.rpt'   
    namehead=['Time','Din','Dout']
    resultfail='Localization-all-Damage-08-08-2022-trilinear.txt'
    resultfail_min='Localization-min-Damage-08-08-2022-trilinear.txt'

if len(Fnames)==1:
    resultfail='TEST.txt'
    resultfail_min='TESTmin.txt'

naverage=3          #This is the value over which running mean is calculated
dt=0.0025

viridis = cm.get_cmap('rainbow', 21)
map=viridis(range(21))



n=open(resultfail,"a")
n.write(f"{'Folder':41}{'angle':>8}{'Index':>8}{'Elength':>8}{'cotfiiA':>8}{'cotfiiB':>8}{'cotfiiC':>8}{'Ra':>8}{'Rb':>8}{'Rctot':>8}{'RcNECK':>8}{'Dcrit':>8}{'Localization':>14}{'Fail_stage':>14}\n")

n_min=open(resultfail_min,"a")
n_min.write(f"{'Folder':41}{'angle':>8}{'Index':>8}{'Elength':>8}{'cotfiiA':>8}{'cotfiiB':>8}{'cotfiiC':>8}{'Ra':>8}{'Rb':>8}{'Rctot':>8}{'RcNECK':>8}{'Dcrit':>8}{'Localization':>14}{'Fail_stage':>14}{'Damage_File':>25}\n")



def con(f):
    res=pi/2.-np.arctan(f+1e-10)
    return res
def addEnergyPlot(path,case):
    fig,ax=gd.sim_energy_curve_plot(path,case)
    ax.legend(loc='best') 
    plt.show() 
    return fig
    # if angle==0:
    #     ax.plot(E.t,E.out,'-k',linewidth=0.6)    
def addstrainplot(ep2,ep1,tsw):  
    fig,ax=pyp.FLC_plot()
    ax.plot(ep2,ep1,'--k')
    #Tws is the switch time normalized with respect to 1
    #The following plots the first two segments in red
    ax.plot(ep2[:int(sum(tsw[:-1])*400)],ep1[:int(sum(tsw[:-1])*400)],'-r')
    ax.plot(ep2[int(sum(tsw[:-1])*400)],ep1[int(sum(tsw[:-1])*400)],'or')
    #Assumed strain maxindex
    ax.plot(ep2[int(dfind.MaxIndx[dfind_min])],ep1[int(dfind.MaxIndx[dfind_min])],'x',label=dfind_min)
    plt.show()
    pdir=f'alfa_{angle}_{angle+dalfa}'
    gd.sim_strain_hist_plot_folder(strainp,fig,ax,anglerange.index(theta_min),dfind.MaxIndx[dfind_min])
    ax.legend(loc='best')
    plt.show() 
    return fig


 
# sys.exit()
for i,fol in enumerate(Fnames):
    # if i<13:
    #     continue
    
    angle_list_working=[]
    ind_length=[]; 
    missing=""
    indxls_min=[]
    e1_list=[]; e2_list=[];# e1B_list=[]; e2B_list=[]
    rclist=[]
    failstage_ls=[]

    os.chdir(fol)
    if plotres=='yes':
        fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
        ax=fig.axes[0]; ax1=fig.axes[1]
    
    # if postprocessed=='yes':
    #     fols=glob.glob('theta*')
    #     for ith,angle in enumerate(fols):    
    # else:   
        
        
    for ith,angle in enumerate(angle_list): #goes through each folder, 
        # fig2, ax2 = plt.subplots(figsize=pyp.cm2inch(8, 8))
        f=open(f'alfa_{angle}_{angle+dalfa}/{forname}')
        lines=f.readlines()
        f.close()  
        rarr=[float(s) for s in lines[82].split('/')[1].split(',')]
        fiarr=[float(s) for s in lines[83].split('/')[1].split(',')]
        theta=[int(s) for s in lines[20].split('/')[1].replace(",","").split() if s.isdigit()]
        path=f'alfa_{angle}_{angle+dalfa}/{Fail}*'
        flist=glob.glob(path)
        dfs = []
        for indk,file in enumerate(flist):
            ang=int(angle+dalfa/10*indk)
            dfs.append(pd.read_csv(file, delimiter= '\s+',names=[f'Time_{ang:d}',f'Din_{ang:d}',f'Dout_{ang:d}'],header=0,na_values='-nan(ind)'))
        try:
            df = pd.concat(dfs, axis=1)  #concatenate the list into single dataframe
            filter_col = [col for col in df if col.startswith('Di')] #list of names starting with Di (the imperfection zone)
            #Create a dataframe to hold the results       
            Ind_plot=pd.DataFrame(columns=['MaxIndx','CritD','Local','Fstage'])        
            Ind_plot.MaxIndx=(df[filter_col] >= 1).idxmax()     #gives the index where damage >1
            Ind_plot.Local=('True')                             #Specify that in all cases localization happened (default)
    #----------------------------------------------------
    # This portion of the code determines weather localization happened or not (damage > 1)
    #----------------------------------------------------
            # Create a series where maxindex =0, meaning damage has not reached 1.
            Dlt0=Ind_plot.loc[(Ind_plot.MaxIndx<1)]
            # No localization cases, max index is the length of the analysis   
            Ind_plot.MaxIndx[Dlt0.index]=(df[Dlt0.index]).idxmax()
            # No localization cases,set localization variable to False          
            Ind_plot.Local[Dlt0.index]=('False')  
    #----------------------------------------------------
    #Determine critical damage
    #----------------------------------------------------
    # not perfect, but creates matrix where it looks from the original dataset values based on MaxIndx. Result is a matrix w
    # which diagonal is the damage value we are after (maxd=)
            dd=df[Ind_plot.index].iloc[Ind_plot.MaxIndx]      
            Ind_plot.CritD=np.diag(dd)
    
            
            nr_of_data=(df[filter_col]).idxmax()  # gives the length of the datapoints (should be 400)
            # Put ind_plot dataframe to list. The list contains 10 simulations (for each theta value )
            indxls_min.append(Ind_plot)
            ind_length.append(nr_of_data)
    #----------------------------------------------------
    #plot the path for specific case (only in debug mode, uncomment the section)
    #----------------------------------------------------
    # find correspoding strain value  (for plot)   
            ep1,ep2,tsw=gd.applied_strain_hist_long(dt,rarr,fiarr)
            failstage=3
    #---------------------THIS SECTION ONLY IN DEBUG MODE------------------------
    #PLOTS THE STRIAN PATH        
            # fig,ax=pyp.FLC_plot()
            # ax.plot(ep2,ep1,'--k')
            # #Tws is the switch time normalized with respect to 1
            # #The following plots the first two segments in red
            # ax.plot(ep2[:int(sum(tsw[:-1])*400)],ep1[:int(sum(tsw[:-1])*400)],'-r')
            # ax.plot(ep2[int(sum(tsw[:-1])*400)],ep1[int(sum(tsw[:-1])*400)],'or')
            # ax.plot(ep2[int(Ind_plot.MaxIndx[0])],ep1[int(Ind_plot.MaxIndx[0])],'x')
            # plt.show()
            # pdir=f'alfa_{angle}_{angle+dalfa}'
            # gd.sim_strain_hist_plot_folder(pdir,fig,ax,0)
            # plt.show()
    #---------------------END OF THE DEBUG MODE------------------------
            if ep1[int(Ind_plot.MaxIndx[0])]<ep1[int(sum(tsw[:-1])*400)]:
                # print('failure in 2nd stage')
                #This works only partially, because the strain calculation does not account localization
                failstage=2
            Ind_plot.Fstage=failstage
    #-------------------------------------------
    # Calculate the length of the third loading segment based on the determined critical index
    #-------------------------------------------
            # This calculates the length of the Rc until neck. Both options should give the same value
            # rCmax=(ep1[276]-rarr[0]*np.sin(con(fiarr[0]))-rarr[1]*np.sin(con(fiarr[1])))/np.sin(con(fiarr[2]))  #Eq from the paper
            rc=sum(rarr)*Ind_plot.MaxIndx/400-sum(rarr[:-1])        #Based on the normalization # TODO This 400 might be problematic
            rc.name='Rcneck'
            rclist.append(rc)
            missingdata=False
    #-------------------------------------------
    # Write all cases to text file
    #-------------------------------------------
    
            # for i,thetai in enumerate(theta):
            #     n.write(f"{fol:34}{thetai:8d}{int(Ind_plot.MaxIndx[i]):8d}{nr_of_data[i]:8d}"
            #             f"{fiarr[0]:8.2f}{fiarr[1]:8.2f}{fiarr[2]:8.2f}{rarr[0]:8.4f}"
            #             f"{rarr[1]:8.4f}{rarr[2]:8.4f}{rc[i]:8.4f}{Ind_plot.CritD[i]:8.3f}{Ind_plot.Local[i]:^14s}{int(Ind_plot.Fstage[i]):14d}\n")  
            # n.write(f"{'Folder':40}{'angle':>8}{'Index':>8}{'Elength':>8}{'cotfiiA':>8}{'cotfiiB':>8}{'cotfiiC':>8}{'Ra':>8}{'Rb':>8}{'Rc':>8}{'RcNECK':>8}{'Dcrit':>8}{'Localization':>8}\n")
#-------------------------------------------
# Main file including info about all theta cases per loading segment
#-------------------------------------------
        except:
            n.write(f"{fol:41} data-missing \n")
            missingdata=True
            break
            
    if missingdata==False:            
            
        dfind = pd.concat(indxls_min, axis=0)
        theta_lengths= pd.concat(ind_length, axis=0)
        r1df=pd.concat(rclist, axis=0)
        # fstagedf=pd.concat(failstage_ls, axis=0)
        dfind['rlen']=r1df
        # dfind['fstage']=fstagedf
        if len(dfind.index[dfind.Local=='True']):
            # print('some lcoalization cases')
            dfind_min=dfind[(dfind.Local=='True')].rlen.idxmin()
        else: #no localization
            # print('no localization')
            dfind_min=dfind.CritD.idxmax()
        theta_min=int(dfind_min.split('_')[1])
    #-------------------------------------------
    # energy file, strain file, and damage file    
    #-------------------------------------------  
        fidx = bisect.bisect_right(angle_list, theta_min)
        # if fidx==0:
        #     fidx+=1
        folgr=glob.glob('alfa*')
        # angle in these simulations e.g. 50,52,54
        anglerange=[x for x in range(angle_list[fidx-1],angle_list[fidx-1]+dalfa,int(dalfa/10))]    
        namef=f'{folgr[fidx-1]}/Damage{anglerange.index(theta_min)}.rpt'
    # added 08-09-2022 makes the plot of strain from most critical case
        strainp=f'{folgr[fidx-1]}'
    # add the plot for most critical case----------------------
        
        # fig1=addstrainplot(ep2,ep1,tsw)
        # fig2=addEnergyPlot(strainp,anglerange.index(theta_min))
        # fig1.savefig(f'{fol}-FLC.png',dpi=500,transparent=False)
        # fig2.savefig(f'{fol}-energy.png',dpi=500,transparent=False)
        # plt.close(fig1)
        # plt.close(fig2)
    
    #-------------------------------------------
    # Write critical cases to text file
    #-------------------------------------------
        n_min.write(f"{fol:41}{theta_min:8d}{int(dfind.MaxIndx[dfind_min]):8d}{theta_lengths[dfind_min]:8d}"
                        f"{fiarr[0]:8.2f}{fiarr[1]:8.2f}{fiarr[2]:8.2f}"
                        f"{rarr[0]:8.4f}{rarr[1]:8.4f}{rarr[2]:8.4f}{dfind.rlen[dfind_min]:8.4f}"
                        f"{dfind.CritD[dfind_min]:8.4f}{dfind.Local[dfind_min]:>14s}{dfind.Fstage[dfind_min]:14d}{namef:>25s}\n")  
    else:
        n_min.write(f"{fol:41}{'nan':>8s}{'nan':>8s}{'nan':>8s}{fiarr[0]:8.2f}{fiarr[1]:8.2f}{fiarr[2]:8.2f}{rarr[0]:8.4f}{rarr[1]:8.4f}{rarr[2]:8.4f}"
                f"{'nan':>8s}{'nan':>8s}{'False':>14s}{'nan':>14s}{'nan':>25s}\n")  
    os.chdir("..") 

    
n.close()  
n_min.close()    
