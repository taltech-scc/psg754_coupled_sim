# -*- coding: utf-8 -*-
"""
Created on 13.09.2021 15:15:44

@author: Mihkel

Plot one simulation result based on the sequence number V_**** and the principal
stress direction wrt imperfection band (theta angle). All are bilinear simulations.
The sequence number interpretation: V_****-A_xx-B_yy
    **** - sequence number
    xx - cotfii at the first proportional loading (step A)
    yy - cotfii at the second proportional loading step (step B)

"""
# fff


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division
pi=4*np.arctan(1)


def energy_plot(R,case,*criterion,**kwargs):
# '''
# Energy plot
# '''
    if np.size(criterion)>0:
        if criterion[0]=='energy':
            marker='r'
        else:
            marker='g'
    fig3,ax3=pyp.newfigdef(3)
    
    ax3.grid(which='major') 
    ax3.set(xlabel='Time',ylabel='Energy')
   
    Fname_seg_A=glob.glob(f'{case}*')
    critical=R.loc[(R.Fol==os.path.split(Fname_seg_A[0])[-1])]
    theta=critical.theta.to_numpy()[0]
    Phi_A=critical.fiA.to_numpy()[0]
    Phi_B=critical.fiB.to_numpy()[0]
       
    foldername=Fname_seg_A[0]+'/theta_'+str(theta)
    print(foldername)
    Files1=glob.glob(foldername+'/energy.r*')
    Cri_energy=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,names=['Time','Eout','Ein'],header=0,na_values='-nan(ind)')    
    IndxLoc=critical.Ind
    
    ax3.plot(Cri_energy.Time[IndxLoc],Cri_energy.Ein[IndxLoc],'o',markersize=2); plt.draw()
    # ax3.plot(Cri_energy.Time,Cri_energy.Ein,'-',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
    # ax3.plot(Cri_energy.Time,Cri_energy.Eout,'--',label='Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
    ax3.plot(Cri_energy.Time,Cri_energy.Ein,'-',label='In '+foldername,zorder=100)   
    ax3.plot(Cri_energy.Time,Cri_energy.Eout,'--',label='Out '+foldername,zorder=100)     
    
    if 'alldata' in kwargs:
        Rall=kwargs['alldata']
        extra_theta=kwargs['theta']
        crit_from_all=Rall.loc[(Rall.Fol==os.path.split(Fname_seg_A[0])[-1]) & (Rall.theta==extra_theta) ]
        folder=Fname_seg_A[0]+'/theta_'+str(extra_theta)
        Files2=glob.glob(folder+'/energy.r*')
        Cri_energy2=pd.read_csv(Files2[0],delimiter= '\s+',index_col=False,names=['Time','Eout','Ein'],header=0,na_values='-nan(ind)')    
        IndxLoc2=crit_from_all.Ind
        ax3.plot(Cri_energy2.Time[IndxLoc2],Cri_energy2.Ein[IndxLoc2],'x',markersize=2); plt.draw()
        # ax3.plot(Cri_energy.Time,Cri_energy.Ein,'-',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        # ax3.plot(Cri_energy.Time,Cri_energy.Eout,'--',label='Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        ax3.plot(Cri_energy2.Time,Cri_energy2.Ein,'-b',label='In '+folder,zorder=100)   
        ax3.plot(Cri_energy2.Time,Cri_energy2.Eout,'--b',label='Out '+folder,zorder=100)     



    return fig3,ax3
    
def energy_plot_all(R,case):
    fig3,ax3=pyp.newfigdef(3)
    ax3.grid(which='major') 
    ax3.set(xlabel='Time',ylabel='Energy')
    Fname_seg_A=glob.glob(f'{case}*')
    critical=R.loc[(R.Fol==os.path.split(Fname_seg_A[0])[-1])]
    theta=critical.theta.to_numpy()[0]
    Phi_A=critical.fiA.to_numpy()[0]
    Phi_B=critical.fiB.to_numpy()[0]
    # ax3.set_prop_cycle('color',plt.cm.Blues(np.linspace(1,0,22))) # use before plot command
    col=plt.cm.Blues(np.linspace(1,0,10)) #22
    for i,theta in enumerate(np.arange(0,40,2)):
        foldername=Fname_seg_A[0]+'/theta_'+str(theta)
        print(foldername)
        Files1=glob.glob(foldername+'/energy.r*')
        Cri_energy=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,names=['Time','Eout','Ein'],header=0,na_values='-nan(ind)')    
        
        # ax3.plot(Cri_energy.Time[IndxLoc],Cri_energy.Ein[IndxLoc],'or',markersize=4); plt.draw()
        # ax3.plot(Cri_energy.Time,Cri_energy.Ein,'-',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        # ax3.plot(Cri_energy.Time,Cri_energy.Eout,'--',label='Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        # ax3.plot(Cri_energy.Time,Cri_energy.Ein,'-',color=col[i],label=str(theta))   
        # ax3.plot(Cri_energy.Time,Cri_energy.Eout,'--',color=col[i])     
        ax3.plot(Cri_energy.Time,Cri_energy.Ein,'-',label=str(theta))   
        ax3.plot(Cri_energy.Time,Cri_energy.Eout,'--')  
                # ax.plot(E.t,E.inn,'--',color=map[ith],linewidth=0.5); ax.plot(E.t,E.out,color=map[ith],linewidth=0.5)
                # ax.plot(E.t[ind_plot],E.inn[ind_plot],'o',markersize=4,markerfacecolor='none',markeredgecolor=map[ith],markeredgewidth=0.5)
                # ano=('th={},{:3.1f}'.format(str(angle),indx))
                # ax.annotate(ano,(E.t[ind_plot],E.inn[ind_plot]),fontsize=7,             #Annotation
                #         xytext=(0, 3),textcoords="offset points")

        
        # Files1=glob.glob('energy*')
        # for j,out_rpt in enumerate(Files1):    
        #         ener_in=np.loadtxt(out_rpt,skiprows=3,usecols = (2))
        #         ener_out=np.loadtxt(out_rpt,skiprows=3,usecols = (1))
        #         time=np.loadtxt(out_rpt,skiprows=3,usecols = (0))
        #         if len(out_rpt)>10:
        #             ax3.plot(time,ener_in,'-k',label='1 element')    
                    
        #         else:   
        #             ax3.plot(time[IndxLoc],ener_in[IndxLoc],'or',markersize=4)
        #             ax3.plot(time,ener_in,'-',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
        #             ax3.plot(time,ener_out,'--',label='Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
    ax3.legend(loc='lower right')  
    return fig3,ax3

def plot_path(R,case,*dif_name_format):
    # R - dataset
    
    cwd=os.getcwd()
    critical=R.loc[(R.Fol.str.contains(case))]
    theta=critical.theta.to_numpy()[0]
    Phi_A=critical.fiA.to_numpy()[0]
    Phi_B=critical.fiB.to_numpy()[0]    
    

    if PathAfixed=='no':
        foldername =f'{critical.Fol.values[0]}/theta_{theta}'
    else:
        foldername =f'theta_{theta}/{critical.Fol.values[0]}'



    # Fname_seg_A=glob.glob(case)
    
    # critical=R.loc[(R.Fol==os.path.split(Fname_seg_A[0])[-1])]
    # theta=critical.theta.to_numpy()[0]
    # Phi_A=critical.fiA.to_numpy()[0]
    # Phi_B=critical.fiB.to_numpy()[0]
    
    # Load the critical file as well

    Files1=glob.glob(foldername+'/Principal_strains.r*')
    if np.size(dif_name_format)==0:
        Cri_strains=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,
                names=['Time','e1out','e2out','e3out','e1','e2','e3','epsout','eps','Tout','T'],header=0,na_values='-nan(ind)')
    else:
        Cri_strains=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,
                names=['Time','e1out','e2out','e3out','e1','e2','e3'],header=0,na_values='-nan(ind)')
                       
    
    
    fig,ax=pyp.FLC_plot()
    
    f=open(foldername+'/vdisp_BILINEAR-R1.for')
    lines=f.readlines()
    r1sim=float(lines[21].split('=')[1].replace("\n",""))
    r2sim=float(lines[22].split('=')[1].replace("\n",""))
    
    # print('Radius comp:',r1sim,critical.Ra)
    # print('Radius comp:',r2sim,critical.Rbtot)
    
    '''
    This is to plot the applied strain defined in the VDISP.for file
    '''
    dt=0.0025
    ep1,ep2=gd.applied_strain_hist(r1sim,r2sim,Phi_A,Phi_B,dt)
    ax.plot(ep2,ep1,'-k',linewidth=0.6,markersize=0.5)   
    IndxLoc=critical.Ind
    ax.plot(ep2[IndxLoc],ep1[IndxLoc],'or',linewidth=1,markersize=2,markerfacecolor='None') 
    try:
        ax.plot(critical.e2f,critical.e1f,'og',linewidth=0.3,markersize=2)  
    except:
        print('strains not defined in the output file')
    
    def add_sim_principal(cr_case):
        # Files1=glob.glob(folder+'/Principal_strains.r*')
        # for j,out_rpt in enumerate(Files1):
        #         e1i=np.loadtxt(out_rpt,skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
        #         e2i=np.loadtxt(out_rpt,skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
        #         e3i=np.loadtxt(out_rpt,skiprows=3,usecols = (6))
        ax.plot(cr_case.e3,cr_case.e1,':r',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))  
        ax.plot(cr_case.e2,cr_case.e1,':b')
        # ax.legend(loc='best')           
        ax.grid(b=True, which='major')
        # mainfol,_,lastfolder=pyg.splitpath(os.getcwd())   
        # ax.set(title=mainfol[-20:-14]+' '+lastfolder)   
    add_sim_principal(Cri_strains)
    # os.chdir(cwd)
    return critical,Cri_strains

def plastic_strain_plot(R,case):
    fig,ax=pyp.newfigdef()
    #Load the postprocessed results with critical index Localization-all.txt
    # -----------------------------------------------------------------------
    # This part exactly the same as in plot_path
    # fname=glob.glob(analysismethod)
    # # fname=glob.glob('Localization-min-energy.txt')
    # R=pd.read_csv(fname[0],delimiter= '\s+',index_col=False,
    #         names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
    Fname_seg_A=glob.glob(case)
    # critical=R.loc[(R.Fol==Fname_seg_A[0])]
    critical=R.loc[(R.Fol==os.path.split(Fname_seg_A[0])[-1])]
    theta=critical.theta.to_numpy()[0]
    Phi_A=critical.fiA.to_numpy()[0]
    Phi_B=critical.fiB.to_numpy()[0]
    # os.chdir(Fname_seg_A[0]+'/theta_'+str(theta))
    # -----------------------------------------------------------------------
    
    
    foldername=Fname_seg_A[0]+'/theta_'+str(theta)
    Files1=glob.glob(foldername+'/Principal_strains.r*')
    try:
        Cri_strains=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,
                names=['Time','e1out','e2out','e3out','e1','e2','e3','epsout','eps','Tout','Tin'],header=0)
    except:
        Cri_strains=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,
                names=['Time','e1out','e2out','e3out','e1','e2','e3'],header=0)
        
    
    
    
    # fi1=glob.glob('Principal_strains.r*')
    # e1i=np.loadtxt(fi1[0],skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
    # e2i=np.loadtxt(fi1[0],skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
    # e3i=np.loadtxt(fi1[0],skiprows=3,usecols = (6))
    # epsin=np.loadtxt(fi1[0],skiprows=3,usecols = (8))
    # Triax_sim_in=np.loadtxt(fi1[0],skiprows=3,usecols = (10))
    e1i=Cri_strains.e1 #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
    e2i=Cri_strains.e2 #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
    e3i=Cri_strains.e3
    epsin=Cri_strains.eps
    Triax_sim_in=Cri_strains.Tin
    
    
    
    
    
    #calculation of triaxiality
    eps=np.sqrt(2)/3*np.sqrt((e1i-e2i)**2+(e1i-e3i)**2+(e2i-e3i)**2)
    dt=0.0025
    time=np.arange(0,1+dt,dt)
    r1=critical.Ra; r2=critical.Rbtot
    tswitch=r1/(r1+r2)
    Phi=np.zeros(len(e1i))
    for i,tim in enumerate(time):
        # print(i)
        if tim<=tswitch.to_numpy()[0]:
            Phi[i]=Phi_A
        else:
            Phi[i]=Phi_B
    Triax=1/np.sqrt(3)*(1+Phi)/np.sqrt(1+Phi+Phi**2)
    IndxLoc=critical.Ind
    ax.plot(Triax,eps.to_numpy())
    ax.plot(Triax_sim_in,epsin,'-r')
    ax.plot(Triax[IndxLoc],eps[IndxLoc],'or',linewidth=1,markersize=2,markerfacecolor='None') 
   
    step=np.arange(0,401,1)
    fig2,ax2=pyp.newfigdef(2)
    ax2.plot(step,eps,'--k')
    ax2.plot(step,epsin,'--r')
    ax2.plot(step[IndxLoc],eps[IndxLoc],'ok')
    ax2.plot(step[IndxLoc],epsin[IndxLoc],'xr',markersize=5)
        
if __name__ == "__main__":
    # Define what you want to plot
    flcplot=1
    epsplot=0
    energy=0
    energyall=0
    multi='no'
    largedataset=1 
    bilin_path_A_fix=0
    linearFLC=0
    
#------------------------------------------------------------------------------
#Results from bi-linear Path A fixed
#------------------------------------------------------------------------------    
    if bilin_path_A_fix==1:
        diru_script=os.getcwd().split('Scripts')
        fol=glob.glob(f'{diru_script[0]}/Model2/BiLinear_A_Fixed/BI-A-fix-V_000-a_0.80-R1_0.4')       
        os.chdir(fol[0])    
        resultstxt='Localization-min-Damage-19082022.txt'
        dataset=pd.read_csv(glob.glob(resultstxt)[0],delimiter= '\s+',index_col=False,
                    names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbinit','e1A','e2A','e1f','e2f','e1','e2'],header=0)
    
        compcase='V_001'
        PathAfixed='Yes'
        if flcplot==1:
            # Cr,Cr_strains=plot_path(dataset,compcase)
            dif_name_format=1
            Cr,Cr_strains=plot_path(dataset,compcase,dif_name_format)
#------------------------------------------------------------------------------        
# Results from the large dataset
#------------------------------------------------------------------------------
    if largedataset==1 and multi=='no':
        Model='Model2'
        fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear-Damage')
        os.chdir(fol[0])
        compcase='V_00001'
        # compcase='V_03833*'
        anal2='Localization-min-Damage-02-05-2022.txt'
        R=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',index_col=False,
                names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
        PathAfixed='no'
        if flcplot==1:
            # Cr,Cr_strains=plot_path(dataset,compcase)
            dif_name_format=1
            Cr,Cr_strains=plot_path(R,compcase,dif_name_format)
        if energy==1:
            energy_plot(dataset,compcase,'energy',alldata=R,theta=0)
        Ra=R.loc[(R.fiA==0.75) & (R.fiB==-0.35)]
#------------------------------------------------------------------------------        
# Results from the large dataset(checking fii ranges)
#------------------------------------------------------------------------------
    if largedataset==1 and multi=='yes':
        Model='Model2'      
        fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear-Damage')
        os.chdir(fol[0])
        anal2='Localization-min-Damage-02-05-2022.txt'
        R=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',index_col=False,
                names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
        Ra=R.loc[(R.fiA==0.75) & (R.fiB==-0.35)]
        fig,ax=pyp.FLC_plot(1)
        for i,e in enumerate(Ra.itertuples(index=False)):
            path=glob.glob(f'{e.Fol}/theta_{e.theta}')[0]
            gd.sim_strain_hist_plot_folder(path,fig,ax,'',e.Ind)

        # plot_path(R,compcase,dif_name_format)
        
#------------------------------------------------------------------------------
#Results from proportional linear loading
#------------------------------------------------------------------------------
    if linearFLC==1:
        fol=glob.glob('C:/work/ML_FLC/Model2/LinearBeam/BB-LT5-ALL-24112021-hc')
        
        os.chdir(fol[0])
        #-------------
        #Vana versioon
        # resultstxt='Postprocessing/minindex-energy.txt'
        # dataset=pd.read_csv(glob.glob(resultstxt)[0],delimiter= '\s+',index_col=False,
        #             names=['Fol','theta','Ind','Elength','fiA','fiB','Rtot','Rc'],header=0)
        #-----------------------------
        resultstxt='Localization-min-Damage-13-05-2022-reduced.txt'
        dataset=pd.read_csv(glob.glob(resultstxt)[0],delimiter= '\s+',index_col=False,
                    names=['Fol','theta','Ind','Elength','fiA','fiB','Rtot','Rc'],header=0)
        # alldat=pd.read_csv(glob.glob('Localization-all-energy*')[0],delimiter= '\s+',index_col=False,
        #             names=['Fol','theta','Ind','Elength','fiA','fiB','Rtot','Rc'],header=0)
        compcase='V_018*' #used in the paper as the shear example
        # compcase='V_020*' #used in the paper as the PST example
    
    
        if flcplot==1:
            # Cr,Cr_strains=plot_path(dataset,compcase)
            dif_name_format=1
            Cr,Cr_strains=plot_path(dataset,compcase,dif_name_format)
        
        
        #Plot principal designs and plastic strain
        if epsplot==1:
            plastic_strain_plot(dataset,compcase)
        #Plot principal designs and plastic strain
        if energy==1:
            energy_plot(dataset,compcase,'energy',alldata=dataset,theta=0)
            energy_plot(dataset,compcase,'energy',alldata=dataset,theta=10)
            energy_plot(dataset,compcase,'energy',alldata=dataset,theta=20)
            energy_plot(dataset,compcase,'energy',alldata=dataset,theta=30)
        if energyall==1:
            energy_plot_all(dataset,compcase)
        
# add also damage case

    # resultstxt='Localization-min-damage-13-05-2022.txt'
    # dataset=pd.read_csv(glob.glob(resultstxt)[0],delimiter= '\s+',index_col=False,
    #             names=['Fol','theta','Ind','Elength','fiA','fiB','Rtot','Rc'],header=0)
    # compcase='V_003*' #used in the paper as the shear example
    # fig3,ax3=energy_plot(dataset,compcase,'damage')
    # ax3.set(ylim=[0,None],xlim=[0,None])
    # fig3.savefig('CaseV003_comparison.pdf',transparent=True)



