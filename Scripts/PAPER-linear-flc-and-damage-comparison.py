import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd

cwd = os.getcwd()

Model='Model2'
fol=glob.glob('C:\work\ML_FLC\Model2\LinearBeam\BB-LT5-ALL-24112021-hc')
os.chdir(fol[0])
import Create_linear_FLC_v2
Create_linear_FLC_v2

import PAPER_plot_material
PAPER_plot_material.Paper_material_and_fracture(0,0,1)


os.chdir(cwd)

fig=plt.gcf(); ax=plt.gca()
fig.set_size_inches(pyp.cm2inch(7, 7))
ax.set(xlim=[-1.3, 1.3],ylim=[0,1.6])
ax.legend(loc='lower right')
plt.tight_layout()

filename=os.path.basename(__file__)


plt.savefig('FLC-linear_'+filename+'.pdf') 