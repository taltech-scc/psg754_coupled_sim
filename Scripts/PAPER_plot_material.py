import matplotlib.pyplot as plt
import numpy as np
import python_plotting as pyp
import Hardening_laws as hard
import fracture_criteria_plots as fcp

def Paper_material_and_fracture(mat,locus,locusFLC):
# mat=0
# locus=0
# locusFLC=1
# Material hardening 
    if mat==1:
        sy_m=275*0.99
        K_m=630
        n_m=0.21
        eplat_m=0.012
        ep=[0.0000,0.0025,0.00679,0.0100,0.015, 0.0200,0.0270, 0.0359,0.0387,0.0466,0.0576,0.0692,0.0805,0.0915,0.1026,0.1135,0.1283,0.1443,0.1607,0.1789,0.1980,0.2200,0.2453,0.2760,0.3073,0.3482,0.3867,0.4354,0.4746,0.5185,0.5731,0.6387,0.7010,0.80,0.9,1.,1.1,1.4]
        s_swift_matine=hard.s_swift(ep,sy_m,eplat_m,n_m,K_m)
        
        #current properties
        sy=272
        K=620
        n=0.2
        eplat=0
        
        s_swift=hard.s_swift(ep,sy,eplat,n,K)
        fig,ax=pyp.newfigdef()
        ax.plot(ep,s_swift_matine,'--k')
        ax.plot(ep,s_swift,'-k')
        
        ax.set(xlabel='Equivalent plastic strain (-)',ylabel='Flow resistance k (MPa)')
        
    if locus==1:
       fig2,ax2=pyp.newfigdef()
       # fcp.plot_HC(1.3762,1.4113,0.0088,'HC matine cal',space='strain') 
       fcp.plot_HC(1.3762,1.4113,0.0088,'HC matine cal',space='stress',label='HC fit') 
       fcp.MMC(630,0.294,space='stress',label='MATINE')
       
       
       ax2.legend(loc='best')
       ax2.set(ylabel='Equivalent plastic strain (-)',xlabel='triaxiality (-)')
       
       
    if locusFLC==1:
       fig,ax=pyp.FLC_plot()
       # fcp.plot_HC(1.3762,1.4113,0.0088,'HC matine cal',space='strain') 
       fcp.plot_HC(1.3762,1.4113,0.0088,'HC matine cal',space='strain',label='HC fit') 
       # fcp.MMC(630,0.294,space='strain',label='MATINE')
       
       
       ax.legend(loc='best')
       # ax.set(ylabel='Equivalent plastic strain (-)',xlabel='triaxiality (-)')   
       
       
       