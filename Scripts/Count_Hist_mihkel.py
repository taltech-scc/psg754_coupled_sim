import matplotlib.pyplot as plt
import numpy as np
import os
import glob
import numpy as np
import py_general as pyg
import Global_definitions as gd
pi=4*np.arctan(1)


'''
Version 1

'''
# cwd=os.getcwd()
# Model='Model2'  # SO-BI-LT10-R02-07102021-MLrandomdataset
# fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear-Damage')
# os.chdir(fol[0])

# path = "Localization-min-Damage-02-05-2022.txt"

# CotfiiA = np.loadtxt(path, skiprows=1,usecols = (4))
# CotfiiB = np.loadtxt(path, skiprows=1,usecols = (5))


# fig, ax = plt.subplots(1,1,figsize=pyg.cm2inch(5, 3)) 

# ax.hist(CotfiiA, bins=39,edgecolor='black', linewidth=0.5)
# ax.set(ylabel='Count')
# ax.set_xticks([-1,-0.9,0,1])
# ax.set_yticks(np.arange(0,600,100))
# fig.tight_layout() 



# fig2, ax2 = plt.subplots(1,1,figsize=pyg.cm2inch(5, 3)) 
# ax2.hist(CotfiiB, bins=39,edgecolor='black', linewidth=0.5)
# ax2.set(ylabel='Count')
# ax2.set_xticks([-1,-0.9,0,1])
# ax2.set_yticks(np.arange(0,600,100))
# fig2.tight_layout() 

# os.chdir(cwd)
# fig.savefig('dataset_hist-phase1.pdf',transparent=True)
# fig2.savefig('dataset_hist-phase2.pdf',transparent=True)


'''
Version 2
'''
cwd=os.getcwd()
fol=glob.glob("C:/work/ML_FLC/Model2_multimodel/Bilinear-reruns-combined/all2")
# os.chdir(fol[0])
analysis='Bilinear-results-strain.txt'
Respath=f'{fol[0]}/{analysis}'

#This dataset ignores all cases where Rb<0.1
R=gd.datset_pn(Respath,combined='no',criteria='yes')
Rall=gd.datset_pn(Respath,combined='no',criteria='no')
# CotfiiA = np.loadtxt(Respath, skiprows=1,usecols = (4))
# CotfiiB = np.loadtxt(Respath, skiprows=1,usecols = (5))


fig, ax = plt.subplots(1,1,figsize=pyg.cm2inch(5, 3)) 

ax.hist(Rall.fiA, bins=39,color='blue',edgecolor='black',alpha=0.3, linewidth=0.5,zorder=10)
ax.hist(R.fiA, bins=39,color='#1f77b4',edgecolor='black', linewidth=0.5)



ax.set(ylabel='Count')
ax.set_xticks([-1,-0.9,0,1])
ax.set_yticks(np.arange(0,600,100))
fig.tight_layout() 






fig2, ax2 = plt.subplots(1,1,figsize=pyg.cm2inch(5, 3)) 
ax2.hist(Rall.fiB, bins=39,color='blue',edgecolor='black',alpha=0.3, linewidth=0.5,zorder=10)
ax2.hist(R.fiB, bins=39,edgecolor='black', linewidth=0.5)
ax2.set(ylabel='Count')
ax2.set_xticks([-1,-0.9,0,1])
ax2.set_yticks(np.arange(0,600,100))
fig2.tight_layout() 



#Analyze everything
# Rall=gd.datset_pn(Respath,combined='no',criteria='no')

# os.chdir(cwd)
fig.savefig('dataset_hist-phase1.pdf',transparent=True)
fig2.savefig('dataset_hist-phase2.pdf',transparent=True)
