# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 08:51:23 2022
Critical eps-trix data based on the localization index from principal strains

@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division
pi=4*np.arctan(1)

def find_critical_plastic_strain(R):
    #Load the postprocessed results with critical index Localization-all.txt
    '''
    This script takes the post processed dataset (The file that includes the case and corresponding )
    Folder                                     angle   Index Elength cotfiiA cotfiiB      Ra      Rb           Rbtot      e1A      e2A      e1f      e2f       e1       e2 Missing
    V_000-a_-0.95-T_0.030                          0     400     400    0.70   -0.95  0.1500  1.9800  1.980000000000   0.1229   0.0860   1.5584  -1.2777   1.5584  -1.2777       0
    ...
    Reads from this file the critical case corresponding to each loading state, imports the principal strains, and finds the 
    eps corresponding to critical index value. That is the first step. 
    
    Next it calculates the traixaility based on the cotfii value. It adds these two outputs to the pandas and then writes as excel
    
    
    '''
    # -----------------------------------------------------------------------
    cwd=os.getcwd()
    eps_critical=[]
    Triax_critical=[]
    T_calc=[]
    
    # iterate through the pandas dataframe
    for sims,e in enumerate(R.itertuples(index=False)):
        print(os.getcwd())
        # os.chdir('theta_'+str(e.theta)+'/'+e.Fol+'/')
        # os.chdir('theta_'+str(e.theta))
        fi1=glob.glob('theta_'+str(e.theta)+'/'+e.Fol+'/'+'Principal_strains.r*')
        eps_sim=np.loadtxt(fi1[0],skiprows=3,usecols = (8))
        Triax_sim=np.loadtxt(fi1[0],skiprows=3,usecols = (10))
        eps_critical.append(eps_sim[e.Ind])
        Triax_critical.append(Triax_sim[e.Ind])
        T_calc.append(1/np.sqrt(3)*(1+e.fiB)/np.sqrt(1+e.fiB+e.fiB**2))
        # eps_crit=eps_sim[]
        
    R['epscritical']=eps_critical
    R['Tcritical']=T_calc
    return R
        # os.chdir(cwd)
        
def history_eps_phi_T(R):
    '''
    This one writes the entire plastic strain - stress triaxiality history for each case
    
    '''
    eps_critical=[]
    Triax_critical=[]
    T_calc=[]  
    dt=0.0025 
    time=np.arange(0,1+dt,dt)
    for sims,e in enumerate(R.itertuples(index=False)):
        resfile=e.Fol+'_hist.txt'
        fiL=open(resfile,"a")
        fiL.write("{:>9}{:>10}{:>10}\n".format('eps','cotfii','T(cotfii)'))
        # print(os.getcwd())
        # os.chdir('theta_'+str(e.theta)+'/'+e.Fol+'/')
        # os.chdir('theta_'+str(e.theta))
        fi1=glob.glob('theta_'+str(e.theta)+'/'+e.Fol+'/'+'Principal_strains.r*')
        eps_sim=np.loadtxt(fi1[0],skiprows=3,usecols = (8))
        r1=e.Ra; r2=e.Rbtot
        tswitch=r1/(r1+r2)
        Phi=np.zeros(len(eps_sim))
        for i,tim in enumerate(time):
            # print(i)
            if tim<=tswitch:
                Phi[i]=e.fiA
            else:
                Phi[i]=e.fiB      
        Triax=1/np.sqrt(3)*(1+Phi)/np.sqrt(1+Phi+Phi**2)
        for i,epsi in enumerate(eps_sim):
            fiL.write("{:9.4f}{:10.4f}{:10.4f}\n".format(epsi,Phi[i],Triax[i]))         
        fiL.close() 
        # {:9.4f}{:9.4f}{:9.4f}
        # eps_critical.append(eps_sim[e.Ind])
        # Triax_critical.append(Triax_sim[e.Ind])
        # T_calc.append(1/np.sqrt(3)*(1+e.fiB)/np.sqrt(1+e.fiB+e.fiB**2))   








if __name__ == "__main__":
    # Define what you want to plot
    run_find_critical_plastic_strain=0
    run_history_eps_phi_T=1
    

    Model='Model2'
    fol=glob.glob('C:/work/ML_FLC/'+ Model+'/BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15')
    os.chdir(fol[0])
    anal2='Localization-BI-*.txt'
    dataset=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',index_col=False,
            names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)

    #Plot principal designs and plastic strain
    if run_find_critical_plastic_strain==1:
        Rnew=find_critical_plastic_strain(dataset)
        Rnew.to_excel(r'Localization-epscrit-BI-A-fix-V_000-a_0.80-T_0.665_R1_0.22.xlsx',  index=None)
    # dataset.to_csv(r'pandas.csv',  index=None, sep=' ', mode='a')
    # dataset.to_excel(r'excel.xlsx',  index=None)
    #Plot principal designs and plastic strain
    if run_history_eps_phi_T==1:
        history_eps_phi_T(dataset)