# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 13:24:03 2022
Plots the required specific cases for linear FLC used in paper
@author: Mihkel
"""

import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import PLOT_Dataset_indi_case as caseres

cwd=os.getcwd()
# -----------------------------------------------------------------------------
#Plots the energy output and critical point for energy criterion
# -----------------------------------------------------------------------------
fol=glob.glob('C:/work/ML_FLC/Model2/LinearBeam/BB-LT5-ALL-24112021-hc')
os.chdir(fol[0])
resultstxt='Postprocessing/minindex-energy.txt'
dataset=pd.read_csv(glob.glob(resultstxt)[0],delimiter= '\s+',index_col=False,
            names=['Fol','theta','Ind','Elength','fiA','fiB','Rtot','Rc'],header=0)
case='V_039*'
# case='V_003*'
caseres.energy_plot(dataset, case,'energy')      # Plot single case
# caseres.energy_plot_all(dataset,case)   # Plot all imperfection band angles
# Plot linear flc results
# 

caseres.plot_path(dataset, case,'namesdiff')

# -----------------------------------------------------------------------------
#Plots the energy output and critical point for damage criterion
# -----------------------------------------------------------------------------
res_D_txt='Postprocessing/minindex-damage.txt'
data_D=pd.read_csv(glob.glob(res_D_txt)[0],delimiter= '\s+',index_col=False,
            names=['Fol','theta','Ind','Elength','fiA','fiB','Rtot','Rc'],header=0)
fig,ax=caseres.energy_plot(data_D, case,'damage')

if case=='V_020*':
    ax.set(ylim=[0,2e+6],xlim=[0, 0.3])
elif case=='V_039*':
    ax.set(ylim=[0,2.5e+6],xlim=[0, 1])
else:
    ax.set(ylim=[0,1.1e+6],xlim=[0, 1])

os.chdir(cwd)
# #Figure output    

# plt.tight_layout()  # This is run after to fit the all labels and titles to specified box


fig.set_size_inches(pyp.cm2inch(7, 7))
plt.tight_layout()
ax.legend(loc=(0.04,0))
# plt.savefig('Linear_fLC_energy-v003.pdf',transparent=True)
# # plt.savefig('FD_matine_plots.png',dpi=500,transparent=False)