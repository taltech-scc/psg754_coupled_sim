# -*- coding: utf-8 -*-
"""
TO BE RUN ONLY ON *MLrandomdataset FOLDERS
Findex the localization point value per simulated case (all theta values)


This file creates the Localization-all.txt that contains 
Folder                    angle   Index Elength CotfiiA CotfiiB      Ra Rb(ind)           Rbtot      e1A      e2A       e1       e2 Missing
V_0000-A_0.80-B_0.75          0     212     229    0.80    0.75  0.5504  0.3440  0.649000000000   0.4298   0.3438   0.4980   0.3950        
V_0000-A_0.80-B_0.75          5     212     229    0.80    0.75  0.5504  0.3440  0.649000000000   0.4298   0.3438   0.4980   0.3950        
...   


Folder: folder name
Critical angle: FOr each bi-linear imposed strain state (A and B) there are 19 simulations performed
                theta=0,5...90 where theta is the major principal strain direction wrt to imperfection
                band. One of those angles leads to localization that prevails earlier compared with rest
                of the analysis. This angle is NOT given in this array. To get this minimum, you need to run the
                Critical_Theta_RD.py
                
Index: simulation step at which the localization takes place
Elength: Total steps in the simulation
CotfiiA: strain ratio at stage 1 of the loading
CotfiiB: strain ratio at stage 2 of the loading
Ra: radius in FLC space at stage 1 
Rb(ind): radius until the localization step in FLC space at stage 2 
Rbtot: Rb value used in the fortran file (intended length of segment B loading)
Missing: if some of the simulation folders are missing this is reported in this array
               
@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import ast

pi=4*np.arctan(1)
Model='Model2_multimodel'
PathAfixed='yes'
plotres='yes'
criterion="damage" #strain 7772,19,9936
#--------------------- Start OF USER INPUT -------------------------------------
#Define the dataset to be analyzed
os.chdir('C:\work\ML_FLC')
# fol=glob.glob(Model+'/Bilinear/Dataset/')

if PathAfixed=='yes':
    # fol=glob.glob(Model+'/BI-A-fix-V_000-a_0.80-T_0.665_R1_0.22/')
    fol=glob.glob(Model+'/Complex_MM_3FLC/Complex_MM_3FLC-2/rerun-critical-cases')
   
else:
    fol=glob.glob(Model+'/Bilinear/Dataset/Postprocessed')
os.chdir(fol[0])


if PathAfixed=='yes':
    Fnames=glob.glob('theta_*')
else:  
# plt.ioff()
    Fnames=glob.glob('theta_*') #strain 7772,19,9936

#--------------------- END OF USER INPUT -------------------------------------
#Construction of file name and specification of names in the header of read files
tswitch_add=5 #oli 10

date='9-09-2022'
if criterion=="energy":
    Fail='energy.rpt'
    Fail_1el='energy-1EL.rpt'
    namehead=["Time", "out", "inn"]
    resultfail='Localization-all-energy-'+date+'.txt'
    resultfail_min='Localization-min-energy-'+date+'.txt'
    # resultfail_min_use='Localization-min-energy-work.txt'
elif criterion=="strain":
    Fail='Principal_strains.rpt'
    Fail_1el='Principal_strains-1EL.rpt'   
    namehead=['Time','e1o','e2o','e3o','e1i','e2i','e3i']
    resultfail='TEST-Localization-all-strain.txt'
    resultfail_min='TEST-Localization-min-strain.txt'
elif criterion=="damage":
    Fail='Damage.rpt'
    Fail_1el='Damage-1EL.rpt'   
    namehead=['Time','Din','Dout']
    resultfail='Localization-all-Damage'+date+'.txt'
    resultfail_min='Localization-min-Damage'+date+'.txt'

if len(Fnames)==1:
    resultfail='TEST.txt'
    resultfail_min='TESTmin.txt'

naverage=3          #This is the value over which running mean is calculated
dt=0.0025

viridis = cm.get_cmap('rainbow', 20)
map=viridis(range(20))



n=open(resultfail,"a")
n.write("{:40}{:>8}{:>8}{:>8}{:>9}{:>9}{:>8}\n"
        .format('Folder','angle','Index','Elength','e1','e2','Missing'))
n_min=open(resultfail_min,"a")
n_min.write("{:40}{:>8}{:>8}{:>8}{:>9}{:>9}{:>8}\n"
        .format('Folder','angle','Index','Elength','e1f','e2f','Missing'))

def plot_energy_curve(E,indx,angle,ax):
    ax.plot(E.Time,E.inn,'--',linewidth=0.5)
    ax.plot(E.Time,E.out,'--',linewidth=0.5,label=angle)
    ax.plot(E.Time[indx],E.out[indx],'o',linewidth=0.5)
    ax.legend(loc='best')  
    # if angle==0:
    #     ax.plot(E.t,E.out,'-k',linewidth=0.6)    
    




# if i<18395:
#     continue
angle_list = np.arange(0,45,5)
angle_list_working=[]
indxls=[]; missing=""
indxls_min=[]
e1_list=[]; e2_list=[];# e1B_list=[]; e2B_list=[]
if plotres=='yes':
    fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
    ax=fig.axes[0]; ax1=fig.axes[1]
    
for ith,angle in enumerate(angle_list):
    # fig2, ax2 = plt.subplots(figsize=pyp.cm2inch(8, 8))
    fol='theta_'+str(angle)
    f=open('vdisp_NL.for')
    lines=f.readlines()                 #reads the R lines from the fortran file, e.g.    '      rarr=(/0.05,0.8/)\n'
# Reads the R and phi array from fortran as numeric values
    rarr=ast.literal_eval(lines[60].split('=')[1].replace("\n","")[2:-2])
    fiarr=ast.literal_eval(lines[61].split('=')[1].replace("\n","")[2:-2])

#Calculate the switch time knowing the simulation step time    
    _,_,tsw=gd.applied_strain_hist_long(dt,rarr,fiarr)      #w/o extra parameters the output is e1,e2,tsw -
    
# This is the increment number when last loading segment begins (slightly inaccurate now for some reason), but is not used even
    tswitch_step=int(sum(tsw[:-1])/dt)  #gives the last swtich time
    try:

        path='theta_'+str(angle)+ "/"+ Fail
        # print(path)
        E=pd.read_csv(path,delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)') #damage.rpt or Principle_strain.rpt
        if criterion=="energy":
            ind_plot=gd.critical_indx(E,naverage,tswitch_step,map[ith])  #extra arg=ax1
        elif criterion=="damage":
            ind_plot=np.max(np.where((E.Din<1)))-1
        # if ind_plot<tswitch_step-tswitch_add: #means that analysis has crashed very early (stage A) and there is no data
        #     # indx=0
        #     indx=ind_plot
            
        # else:
            indx=ind_plot
        # e1A,e2A,e1,e2,e1B,e2B=gd.strain_state(r1,r2,cotfii_a,cotfii_b,indx,dt)
        ep1,ep2,_=gd.applied_strain_hist_long(dt,rarr,fiarr)
        e1=ep1[indx]; e2=ep2[indx]
        if plotres=='yes':
            Energy=pd.read_csv('theta_'+str(angle)+ "/energy.rpt",delimiter= '\s+',names=["Time", "out", "inn"],header=0,na_values='-nan(ind)')
            plot_energy_curve(Energy,indx,angle,ax)
        # _,_,e1f,e2f,_,_=gd.strain_state(r1,r2*indx/400,cotfii_a,cotfii_b,indx,dt)
        # indxls.append(indx)
        indxls_min.append(indx)
        e1_list.append(e1); e2_list.append(e2);# e1B_list.append(e1B); e2B_list.append(e2B)
        # missing=''
        theta=angle
        angle_list_working.append(theta)
        # r2max=(e1-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
        # rmax=
        n.write("{:40}{:8d}{:8d}{:8d}{:9.4f}{:9.4f}{:8}\n".
            format(fol,theta,indx,len(E.Time)-1,e1,e2,missing))  
    except:
        missing+=str(angle)+", "
        theta=angle    
        n.write("{:40}{:8d}{:8}{:8}{:9}{:9}{:8}\n".
            format(fol,theta,'-','-','-','-','-'))  
        # break   #this break is intended to speed up the flow. When data is missing, stop processing this set. 
try:
    min_val=np.min(indxls_min)  #gives the value of min index e.g. 230 from 400
    # 19 different simulations. 
    indmin=np.min(np.where(indxls_min==min_val))   #from the 19 different cases finds the location of min_val (0 to 18)
    theta_min=angle_list_working[indmin]
    e1_min=e1_list[indmin]
    e2_min=e2_list[indmin]    
except:
    min_val=400
    theta_min=0
    e1_min=100
    e2_min=100
if len(missing)<2:
    # r2max=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
    n_min.write("{:40}{:8d}{:8d}{:8d}{:9.4f}{:9.4f}{:8}\n".
        format('theta_'+str(theta_min),theta_min,int(min_val),len(E.Time)-1,e1_min,e2_min,len(missing))) 
        # format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2*min_val/400,r2,e1A,e2A,e1_min,e2_min,e1,e2,len(missing))) 
# if len(missing)==0:
#     n_min_all.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}\n".
#         format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2*min_val/400,r2,e1A,e2A,e1_min,e2_min,e1,e2))         
del missing  
if PathAfixed=='no':
    os.chdir("..")     
n.close()  
n_min.close()    
# n_min_all.close()    

