# -*- coding: utf-8 -*-
"""
TO BE RUN ONLY ON *MLrandomdataset FOLDERS
Findex the localization point value per simulated case (all theta values)


This file creates the Localization-all.txt that contains 
Folder                    angle   Index Elength CotfiiA CotfiiB      Ra Rb(ind)           Rbtot      e1A      e2A       e1       e2 Missing
V_0000-A_0.80-B_0.75          0     212     229    0.80    0.75  0.5504  0.3440  0.649000000000   0.4298   0.3438   0.4980   0.3950        
V_0000-A_0.80-B_0.75          5     212     229    0.80    0.75  0.5504  0.3440  0.649000000000   0.4298   0.3438   0.4980   0.3950        
V_0000-A_0.80-B_0.75         10     215     239    0.80    0.75  0.5504  0.3488  0.649000000000   0.4298   0.3438   0.5052   0.4004        
V_0000-A_0.80-B_0.75         15     220     239    0.80    0.75  0.5504  0.3569  0.649000000000   0.4298   0.3438   0.5172   0.4094        
V_0000-A_0.80-B_0.75         20     226     239    0.80    0.75  0.5504  0.3667  0.649000000000   0.4298   0.3438   0.5316   0.4202        
V_0000-A_0.80-B_0.75         25     235     249    0.80    0.75  0.5504  0.3813  0.649000000000   0.4298   0.3438   0.5532   0.4364        
V_0000-A_0.80-B_0.75         30     245     259    0.80    0.75  0.5504  0.3975  0.649000000000   0.4298   0.3438   0.5772   0.4544        
V_0000-A_0.80-B_0.75         35     257     269    0.80    0.75  0.5504  0.4170  0.649000000000   0.4298   0.3438   0.6060   0.4760        
....

Folder: folder name
Critical angle: FOr each bi-linear imposed strain state (A and B) there are 19 simulations performed
                theta=0,5...90 where theta is the major principal strain direction wrt to imperfection
                band. One of those angles leads to localization that prevails earlier compared with rest
                of the analysis. This angle is NOT given in this array. To get this minimum, you need to run the
                Critical_Theta_RD.py
                
Index: simulation step at which the localization takes place
Elength: Total steps in the simulation
CotfiiA: strain ratio at stage 1 of the loading
CotfiiB: strain ratio at stage 2 of the loading
Ra: radius in FLC space at stage 1 
Rb(ind): radius until the localization step in FLC space at stage 2 
Rbtot: Rb value used in the fortran file (intended length of segment B loading)
Missing: if some of the simulation folders are missing this is reported in this array
               
@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
# sys.path.append('C:\work\ML_FLC\Scripts')
import Global_definitions as gd


pi=4*np.arctan(1)
Model='Model2'
PathAfixed='no'  #yes-bilinear scenarios with first path fixed
linearflc='no'   #linear FLC scenario  
criterion="damage" #strain 7772,19,9936

# angle_list = np.arange(0,45,5)  #for bigdataset
# angle_list = np.arange(0,45,5)  #for bi-linear dataset
angle_list = np.arange(0,45,5)  #for preloading FLC
# angle_list = np.arange(0,42,2)    #for linearflc
#--------------------- Start OF USER INPUT -------------------------------------

#Define the dataset to be analyzed

os.chdir('C:\work\ML_FLC')
# fol=glob.glob(Model+'/Bilinear/Dataset/')
if PathAfixed=='yes':
    fol=glob.glob(Model+'/BiLinear_A_Fixed/BI-A-fix-V_000-a_0.80-R1_0.4/') #bi-linear path scenario with first path fixed (folder structure a bit differernt)
    # fol=glob.glob(Model+'/NonLinear_paths/NL_V01-Theta/')
else:
    fol=glob.glob(Model+'/Bilinear-Damage')


# if Model=='Model2':
#     fol=glob.glob(Model+'Bilinear/Dataset/')
# elif Model=='Model1':
#     fol=glob.glob('SO-BI-LT10-R02-07102021-MLrandomdataset')
# elif Model=='Model3':
#     fol=glob.glob('Data_07102021-C')

# os.chdir('C:\work\ML_FLC')
# fol=glob.glob('testset/')


os.chdir(fol[0])

if PathAfixed=='yes':

    os.chdir('theta_90')
    Fnames=glob.glob('V_*')
    os.chdir("..")
else:  
# plt.ioff()
    Fnames=glob.glob('V_*') #strain 7772,19,9936

#--------------------- END OF USER INPUT -------------------------------------
tswitch_add=5 #oli 10

plotres='yes'
plotenergy=='no'
if criterion=="energy":
    Fail='energy.rpt'
    Fail_1el='energy-1EL.rpt'
    namehead=["Time", "out", "inn"]
    resultfail='Localization-all-energy-13-05-2022.txt'
    resultfail_min='Localization-min-energy-13-05-2022.txt'
    # resultfail_min_use='Localization-min-energy-work.txt'
elif criterion=="strain":
    Fail='Principal_strains.rpt'
    Fail_1el='Principal_strains-1EL.rpt'   
    namehead=['Time','e1o','e2o','e3o','e1i','e2i','e3i']
    resultfail='TEST-Localization-all-strain.txt'
    resultfail_min='TEST-Localization-min-strain.txt'
elif criterion=="damage":
    Fail='Damage.rpt'
    Fail_1el='Damage-1EL.rpt'   
    namehead=['Time','Din','Dout']
    resultfail='test.txt'
    resultfail_min='test-min.txt'

if len(Fnames)==1:
    resultfail='TEST.txt'
    resultfail_min='TESTmin.txt'

naverage=3          #This is the value over which running mean is calculated
dt=0.0025

viridis = cm.get_cmap('rainbow', 21)
map=viridis(range(21))



n=open(resultfail,"a")
n.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>8}\n"
        .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Rb','Rbtot','e1A','e2A','e1','e2','Missing'))
n_min=open(resultfail_min,"a")
n_min.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>9}{:>9}{:>8}\n"
        .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'))
#Rb - is the actual length until localization
#Rbtot - is the initial guess length (simulation) of the segmant 2

# n_min_all=open(resultfail_min_use,"a")
# n_min_all.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>9}{:>9}\n"
#         .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2'))

# fig=plt.figure(1); ax=fig.axes
# if ax==[]:
#     plt.close(fig)
#     fig, ax = plt.subplots(figsize=pyp.cm2inch(8, 8))
# else:
#     ax=ax[0]  

# def pathchoice(PathAfixed,angle,fol,Fail,):
#     if PathAfixed=='no':
#         path ='theta_'+str(angle) + "/" + Fail
#     else:
#         path='theta_'+str(angle)+ "/" +fol+ "/"+ Fail   
#     return path
def con(f):
    res=pi/2.-np.arctan(f+1e-10)
    return res

def plot_energy_curve(E,indx,angle,ax):
    ax.plot(E.Time,E.inn,'--',linewidth=0.5,label=angle)
    ax.plot(E.Time,E.out,'--',linewidth=0.5)
    ax.plot(E.Time[indx],E.out[indx],'o',linewidth=0.5)
    ax.legend(loc='best')  
    # if angle==0:
    #     ax.plot(E.t,E.out,'-k',linewidth=0.6)    
    
# sys.exit()
for i,fol in enumerate(Fnames):
    if fol!='V_09951-A_0.75-B_-0.35-RA_0.20-RB_0.47':
        continue
    # if PathAfixed=='yes' and linearflc=='yes':
    #     angle_list = np.arange(0,42,2)
    # else:
    
    angle_list_working=[]
    indxls=[]; missing=""
    indxls_min=[]
    e1_list=[]; e2_list=[];# e1B_list=[]; e2B_list=[]
    r1_list=[]
    if PathAfixed=='no':
        os.chdir(fol)
    if plotres=='yes':
        fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
        ax=fig.axes[0]; ax1=fig.axes[1]
    
    # if postprocessed=='yes':
    #     fols=glob.glob('theta*')
    #     for ith,angle in enumerate(fols):    
    # else:   
        
        
    for ith,angle in enumerate(angle_list):
        # if angle==25:
        #     print: 'angle'
        # fig2, ax2 = plt.subplots(figsize=pyp.cm2inch(8, 8))
        if PathAfixed=='no':
            f=open('theta_'+str(angle) + "/vdisp_BILINEAR-R1.for")
        else:
            f=open('theta_'+str(angle)+ "/" +fol+ "/vdisp_BILINEAR-R1.for")
        lines=f.readlines()
        r1=float(lines[21].split('=')[1].replace("\n",""))
        r2=float(lines[22].split('=')[1].replace("\n",""))
        cotfii_a=float(lines[24].split('=')[1].replace("\n","")); cotfii_b=float(lines[25].split('=')[1].replace("\n",""))
        
        
        tswitch_step=int(r1/(r1+r2)/0.0025)+tswitch_add   #increment number+5 of the loading segment A. Localization should happen after, not before this stage
        if linearflc=='yes':
            tswitch_step=6
        try:
            if PathAfixed=='no':
                path ='theta_'+str(angle) + "/" + Fail
                path1 ='theta_'+str(angle) + "/" + Fail_1el
            else:
                path='theta_'+str(angle)+ "/" +fol+ "/"+ Fail
                path1='theta_'+str(angle)+ "/" +fol+ "/"+ Fail_1el
            if criterion=="energy" or criterion=="strain":
                # path1 = 'theta_'+str(angle) + "/" + Fail_1el
                if angle==0:
                    E1=pd.read_csv(path1,delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
            # print(path)
            E=pd.read_csv(path,delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
            if criterion=="energy":
                ind_plot=gd.critical_indx(E,naverage,tswitch_step,map[ith])  #extra arg=ax1
            elif criterion=="strain":
                ind_plot=gd.critical_indx_ep(E,E1,tswitch_step,map[ith])
            elif criterion=="damage":
                ind_plot=np.max(np.where((E.Din<1)))-1
            if ind_plot<tswitch_step-tswitch_add: #means that analysis has crashed very early (stage A) and there is no data
                indx=0
            else:
                indx=ind_plot
            e1A,e2A,e1,e2,e1B,e2B=gd.strain_state(r1,r2,cotfii_a,cotfii_b,indx,dt)
            #e1,e2 - applied strain at the point of index failure
            if plotenergy=='yes':
                plot_energy_curve(E,indx,angle,ax)
            # _,_,e1f,e2f,_,_=gd.strain_state(r1,r2*indx/400,cotfii_a,cotfii_b,indx,dt)
            # indxls.append(indx)
            indxls_min.append(indx)
            e1_list.append(e1); e2_list.append(e2);# e1B_list.append(e1B); e2B_list.append(e2B)
            # missing=''
            theta=angle
            angle_list_working.append(theta)
            r2max=(e1-r1*np.sin(con(cotfii_a)))/np.sin(con(cotfii_b))
            if linearflc=='yes':
                r1=r1*indx/400
                r1_list.append(r1)
            else:
                r1_list.append(r1)
            n.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:8}\n".
                format(fol,theta,indx,len(E.Time)-1,cotfii_a,cotfii_b,r1,r2max,r2,e1A,e2A,e1,e2,missing))  
        except:
            missing+=str(angle)+", "
            theta=angle    
            n.write("{:40}{:8d}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>8}\n".
                format(fol,theta,'-','-','-','-','-','-','-','-','-','-','-','-'))  
            # break   #this break is intended to speed up the flow. When data is missing, stop processing this set. 
    try:
        min_val=np.min(indxls_min)  #gives the value of min index e.g. 230 from 400
        # 19 different simulations. 
        indmin=np.min(np.where(indxls_min==min_val))   #from the 19 different cases finds the location of min_val (0 to 18)
        theta_min=angle_list_working[indmin]
        e1_min=e1_list[indmin]
        e2_min=e2_list[indmin]    
        r1_min=r1_list[indmin]    #Added 17.05.2022
    except:
        min_val=400
        theta_min=0
        e1_min=100
        e2_min=100
    if len(missing.split(',')[:-1])<2:
        r2max=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
        n_min.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:8}\n".
            format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1_min,r2max,r2,e1A,e2A,e1_min,e2_min,e1,e2,len(missing))) 
# e1A - p.strain at the end of path A
#e1_min,e2_min - applied strain at the point of failure
#e1,e2 - applied strain at the point of index failure (for each angle, this can be removed from n_min list (not needed there))
            # format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2*min_val/400,r2,e1A,e2A,e1_min,e2_min,e1,e2,len(missing))) 
    # if len(missing)==0:
    #     n_min_all.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}\n".
    #         format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2*min_val/400,r2,e1A,e2A,e1_min,e2_min,e1,e2))         
    del missing  
    if PathAfixed=='no':
        os.chdir("..")     
n.close()  
n_min.close()    
# n_min_all.close()    

