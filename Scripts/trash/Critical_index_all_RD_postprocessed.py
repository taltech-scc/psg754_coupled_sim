# -*- coding: utf-8 -*-
"""
TO BE RUN ONLY ON *MLrandomdataset FOLDERS
Findex the localization point value per simulated case (all theta values)


This file creates the Localization-all.txt that contains 
Folder                    angle   Index Elength CotfiiA CotfiiB      Ra Rb(ind)           Rbtot      e1A      e2A       e1       e2 Missing
V_0000-A_0.80-B_0.75          0     212     229    0.80    0.75  0.5504  0.3440  0.649000000000   0.4298   0.3438   0.4980   0.3950        
V_0000-A_0.80-B_0.75          5     212     229    0.80    0.75  0.5504  0.3440  0.649000000000   0.4298   0.3438   0.4980   0.3950        
V_0000-A_0.80-B_0.75         10     215     239    0.80    0.75  0.5504  0.3488  0.649000000000   0.4298   0.3438   0.5052   0.4004        
V_0000-A_0.80-B_0.75         15     220     239    0.80    0.75  0.5504  0.3569  0.649000000000   0.4298   0.3438   0.5172   0.4094        
V_0000-A_0.80-B_0.75         20     226     239    0.80    0.75  0.5504  0.3667  0.649000000000   0.4298   0.3438   0.5316   0.4202        
V_0000-A_0.80-B_0.75         25     235     249    0.80    0.75  0.5504  0.3813  0.649000000000   0.4298   0.3438   0.5532   0.4364        
V_0000-A_0.80-B_0.75         30     245     259    0.80    0.75  0.5504  0.3975  0.649000000000   0.4298   0.3438   0.5772   0.4544        
V_0000-A_0.80-B_0.75         35     257     269    0.80    0.75  0.5504  0.4170  0.649000000000   0.4298   0.3438   0.6060   0.4760        
....

Folder: folder name
Critical angle: FOr each bi-linear imposed strain state (A and B) there are 19 simulations performed
                theta=0,5...90 where theta is the major principal strain direction wrt to imperfection
                band. One of those angles leads to localization that prevails earlier compared with rest
                of the analysis. This angle is NOT given in this array. To get this minimum, you need to run the
                Critical_Theta_RD.py
                
Index: simulation step at which the localization takes place
Elength: Total steps in the simulation
CotfiiA: strain ratio at stage 1 of the loading
CotfiiB: strain ratio at stage 2 of the loading
Ra: radius in FLC space at stage 1 
Rb(ind): radius until the localization step in FLC space at stage 2 
Rbtot: Rb value used in the fortran file (intended length of segment B loading)
Missing: if some of the simulation folders are missing this is reported in this array
               
@author: Mihkel
"""
import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
# sys.path.append('C:\work\ML_FLC\Scripts')
import Global_definitions as gd


pi=4*np.arctan(1)
Model='Model2'
PathAfixed='yes'
criterion="damage" #strain 7772,19,9936
#--------------------- Start OF USER INPUT -------------------------------------

#     Model='Model1'  # SO-BI-LT10-R02-07102021-MLrandomdataset
#     # Model='Model2'  # SO-BI-LT5-28112021-HC
#     # Model='Model3'  # Data_07102021-C
#     # AnalyzedFolder=('C:/work/ML_FLC/'+ Model +'/Bilinear/Dataset')
# # Model1   
#     analysismethod='Localization-min.txt'
#     fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear/Dataset')




# os.chdir('C:\work\ML_FLC\Model'+str(Model)+'\Bilinear')
#Define the dataset to be analyzed

os.chdir('C:\work\ML_FLC')
# fol=glob.glob(Model+'/Bilinear/Dataset/')
if PathAfixed=='yes':
    # fol=glob.glob(Model+'/BI-A-fix-V_000-a_0.70-T_0.663_R1-0.15/')
    fol=glob.glob(Model+'/LinearBeam/BB-LT5-ALL-24112021-hc/')
else:
    fol=glob.glob(Model+'/Bilinear/Dataset/Postprocessed')



# if Model=='Model2':
#     fol=glob.glob(Model+'Bilinear/Dataset/')
# elif Model=='Model1':
#     fol=glob.glob('SO-BI-LT10-R02-07102021-MLrandomdataset')
# elif Model=='Model3':
#     fol=glob.glob('Data_07102021-C')

# os.chdir('C:\work\ML_FLC')
# fol=glob.glob('testset/')


os.chdir(fol[0])

if PathAfixed=='yes':
    os.chdir('theta_40')
    Fnames=glob.glob('V_*')
    os.chdir("..")
else:  
# plt.ioff()
    Fnames=glob.glob('V_00007*') #strain 7772,19,9936

#--------------------- END OF USER INPUT -------------------------------------
tswitch_add=5 #oli 10

plotres='no'
if criterion=="energy":
    Fail='energy.rpt'
    Fail_1el='energy-1EL.rpt'
    namehead=["Time", "out", "inn"]
    resultfail='Localization-all-energy-14-03-2022.txt'
    resultfail_min='Localization-min-energy-14-03-2022.txt'
    # resultfail_min_use='Localization-min-energy-work.txt'
elif criterion=="strain":
    Fail='Principal_strains.rpt'
    Fail_1el='Principal_strains-1EL.rpt'   
    namehead=['Time','e1o','e2o','e3o','e1i','e2i','e3i']
    resultfail='TEST-Localization-all-strain.txt'
    resultfail_min='TEST-Localization-min-strain.txt'
elif criterion=="damage":
    Fail='Damage.rpt'
    Fail_1el='Damage-1EL.rpt'   
    namehead=['Time','Din','Dout']
    resultfail='Localization-all-Damage-posp.txt'
    resultfail_min='Localization-min-Damage-posp.txt'

if len(Fnames)==1:
    resultfail='TEST.txt'
    resultfail_min='TESTmin.txt'

naverage=3          #This is the value over which running mean is calculated
dt=0.0025

viridis = cm.get_cmap('rainbow', 20)
map=viridis(range(20))



n=open(resultfail,"a")
n.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>8}\n"
        .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Rb','Rbtot','e1A','e2A','e1','e2','Missing'))
n_min=open(resultfail_min,"a")
n_min.write("{:40}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>9}{:>9}{:>8}\n"
        .format('Folder','angle','Index','Elength','cotfiiA','cotfiiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'))


def plot_energy_curve(E,indx,angle,ax):
    ax.plot(E.Time,E.inn,'--',linewidth=0.5,label=angle)
    ax.plot(E.Time,E.out,'--',linewidth=0.5)
    ax.plot(E.Time[indx],E.out[indx],'o',linewidth=0.5)
    ax.legend(loc='best')  
    # if angle==0:
    #     ax.plot(E.t,E.out,'-k',linewidth=0.6)    
    
# sys.exit()
for i,fol in enumerate(Fnames):
    # if i<18395:
    #     continue
    angle_list = np.arange(0,95,5)
    angle_list_working=[]
    indxls=[]; missing=""
    indxls_min=[]
    e1_list=[]; e2_list=[];# e1B_list=[]; e2B_list=[]
    if PathAfixed=='no':
        os.chdir(fol)
    if plotres=='yes':
        fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
        ax=fig.axes[0]; ax1=fig.axes[1]
    

    fols=glob.glob('theta*')
    angle=int(fols[0].split("_")[1])
    ith=0
    # for ith,angle in enumerate(fols): 
    
    # fig2, ax2 = plt.subplots(figsize=pyp.cm2inch(8, 8))
    if PathAfixed=='no':
        f=open('theta_'+str(angle) + "/vdisp_BILINEAR-R1.for")
    else:
        f=open('theta_'+str(angle)+ "/" +fol+ "/vdisp_BILINEAR-R1.for")
    lines=f.readlines()
    r1=float(lines[21].split('=')[1].replace("\n",""))
    r2=float(lines[22].split('=')[1].replace("\n",""))
    cotfii_a=float(lines[24].split('=')[1].replace("\n","")); cotfii_b=float(lines[25].split('=')[1].replace("\n",""))
    
    tswitch_step=int(r1/(r1+r2)/0.0025)+tswitch_add   #increment number+5 of the loading segment A. Localization should happen after, not before this stage
    try:
        if PathAfixed=='no':
            path ='theta_'+str(angle) + "/" + Fail
            path1 ='theta_'+str(angle) + "/" + Fail_1el
        else:
            path='theta_'+str(angle)+ "/" +fol+ "/"+ Fail
            path1='theta_'+str(angle)+ "/" +fol+ "/"+ Fail_1el
        if criterion=="energy" or criterion=="strain":
            # path1 = 'theta_'+str(angle) + "/" + Fail_1el
            if angle==0:
                E1=pd.read_csv(path1,delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
        # print(path)
        E=pd.read_csv(path,delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
        if criterion=="energy":
            ind_plot=gd.critical_indx(E,naverage,tswitch_step,map[ith])  #extra arg=ax1
        elif criterion=="strain":
            ind_plot=gd.critical_indx_ep(E,E1,tswitch_step,map[ith])
        elif criterion=="damage":
            ind_plot=np.max(np.where((E.Din<1)))-1
        if ind_plot<tswitch_step-tswitch_add: #means that analysis has crashed very early (stage A) and there is no data
            indx=0
        else:
            indx=ind_plot
        e1A,e2A,e1,e2,e1B,e2B=gd.strain_state(r1,r2,cotfii_a,cotfii_b,indx,dt)
        if plotres=='yes':
            plot_energy_curve(E,indx,angle,ax)
        # _,_,e1f,e2f,_,_=gd.strain_state(r1,r2*indx/400,cotfii_a,cotfii_b,indx,dt)
        # indxls.append(indx)
        indxls_min.append(indx)
        e1_list.append(e1); e2_list.append(e2);# e1B_list.append(e1B); e2B_list.append(e2B)
        # missing=''
        theta=angle
        angle_list_working.append(theta)
        r2max=(e1-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
        n.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:8}\n".
            format(fol,theta,indx,len(E.Time)-1,cotfii_a,cotfii_b,r1,r2max,r2,e1A,e2A,e1,e2,missing))  
    except:
        missing+=str(angle)+", "
        theta=angle    
        n.write("{:40}{:8d}{:>8}{:>8}{:>8}{:>8}{:>8}{:>8}{:>16}{:>9}{:>9}{:>9}{:>9}{:>8}\n".
            format(fol,theta,'-','-','-','-','-','-','-','-','-','-','-','-'))  
        # break   #this break is intended to speed up the flow. When data is missing, stop processing this set. 
    try:
        min_val=np.min(indxls_min)  #gives the value of min index e.g. 230 from 400
        # 19 different simulations. 
        indmin=np.min(np.where(indxls_min==min_val))   #from the 19 different cases finds the location of min_val (0 to 18)
        theta_min=angle_list_working[indmin]
        e1_min=e1_list[indmin]
        e2_min=e2_list[indmin]    
    except:
        min_val=400
        theta_min=0
        e1_min=100
        e2_min=100
    if len(missing)<2:
        r2max=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
        n_min.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:8}\n".
            format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2max,r2,e1A,e2A,e1_min,e2_min,e1,e2,len(missing))) 
            # format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2*min_val/400,r2,e1A,e2A,e1_min,e2_min,e1,e2,len(missing))) 
    # if len(missing)==0:
    #     n_min_all.write("{:40}{:8d}{:8d}{:8d}{:8.2f}{:8.2f}{:8.4f}{:8.4f}{:16.12f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}{:9.4f}\n".
    #         format(fol,theta_min,int(min_val),len(E.Time)-1,cotfii_a,cotfii_b,r1,r2*min_val/400,r2,e1A,e2A,e1_min,e2_min,e1,e2))         
    del missing  
    if PathAfixed=='no':
        os.chdir("..")     
n.close()  
n_min.close()    
# n_min_all.close()    

