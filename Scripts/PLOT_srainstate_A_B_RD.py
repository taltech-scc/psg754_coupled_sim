# -*- coding: utf-8 -*-
"""
Created on 13.09.2021 15:15:44

@author: Mihkel
Script applies to random data folders. 

Plots the strain state at the end of first (figure1) and second (figure2) loading step. 

Plot is made based on the critical index determined with script Critical_index_PathB.py, outputs minindex.txt

"""
# fff


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('C:\work\ML_FLC\Scripts')
import Global_definitions as gd
import pickle
pi=4*np.arctan(1)





# def only_criticalA(fig,ax,fig_e,ax_e):
#     cwd=os.getcwd()
#     max_steps=int(1/0.0025)
#     # Results=np.loadtxt(fname[0] ,skiprows=1)
    
    
    
#     Model='Model2' 
#     fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear-Damage')
#     os.chdir(fol[0])
# # Define which results to plot. These analysis method files have been postprocesssed earlier using Critical_index_all_RD.py
#     analysismethod='Localization-min-Damage*.txt'
#     R=datset_pn(analysismethod,combined='no')
#     tswitch=R.Ra/(R.Ra+R.Rbtot)
    
#     # Strain state at the end of the first loading path
#     e1A=R.Ra*np.sin(pi/2.-np.arctan(R.fiA+1e-10))
#     e2A=R.Ra*np.cos(pi/2.-np.arctan(R.fiA+1e-10))
    
#     # fig,ax=pyp.FLC_plot(1)
#     ax.plot(e2A,e1A,'.k',markeredgecolor='none',markersize=1,alpha=0.5)#,zorder=10
# #-------------------------------------------    
#     compcase='V_09951*'
#     plot_path_single_case_dataset(analysismethod,R,compcase,'g',fig,ax,fig_e,ax_e)   
#     # compcase='V_00337*'
#     # plot_path_single_case_dataset(analysismethod,R,compcase,'g',fig,ax,fig_e,ax_e)   
# #------------------------------------------------------    
#     # FLc figure    
#     # ax.legend(loc=(1.1,0),prop={'size': 5})
#     ax.get_legend().remove()
#     fig.set_size_inches(pyp.cm2inch(6, 6))
#     ax.set(ylim=[0,0.9],xlim=[-0.8,0.8],xlabel='',ylabel='')
#     fig.tight_layout()        
#     # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
#     ax.set_xticks(np.arange(-0.75,0.75+0.25,0.25))
#     ax.set_yticks(np.arange(0,0.9+0.2,0.2))
#     os.chdir(cwd)
#     fig.savefig('dataset_FLC-[damage-criterion]-1stpath.pdf',transparent=True)
    
    
#     return R
    
# def only_criticalB(fig2,ax2,analysismethod,data,col,**input): 
#     # data[0]=analysismethod, 
#     # data[1]=R 
#     # if len(data)>0:
#     #     # analysismethod=data[0]
#     R=data
#     # else:
#     #     max_steps=int(1/0.0025)
#     #     fname_min=glob.glob(analysismethod)
#     #     Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
#     #            names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
#         # Ra=pd.read_csv(fname[0],delimiter= '\s+',index_col=False,
#         #       names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1','e2','Missing'],header=0)
              
#         # Create a subset of results
#         # R=Ra.loc[(Ra.Ind != 0) & (Ra.Ind < 399)]   #this subset removes particular cases
          
#     ax2.plot(R.e2f,R.e1f,'.'+col,markeredgecolor='none',markersize=1,alpha=input['alpha'])
#     # ax2.plot(Rend.e2f,Rend.e1f,'ob',markeredgecolor='none',markersize=1)
    
#     # adding total paths to points
#     if input['paths']=='yes':
#         dt=0.0025
#         for index, r in R.iterrows():
#             # print(r.Ra) ---> row of the dataframe
#         # for i in R:
#             # testR2=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
#             # ep1,ep2=gd.applied_strain_hist(r.Ra,r.Rb,r.fiA,r.fiB,dt)
#             xdat=[0,r.e2A,r.e2f]; ydat=[0,r.e1A,r.e1f] 
#             ax2.plot(xdat,ydat,'-',color=[0.8,0.8,0.8],linewidth=0.3,zorder=1)   
    
#     return R 

# def plot_all(fig,ax,fig2,ax2):   
#     fname=glob.glob('minindex-all.txt')
#     max_steps=int(1/0.0025)
#     # Results=np.loadtxt(fname[0] ,skiprows=1)
    
#     R=pd.read_csv(fname[0],delimiter= '\s+',index_col=False,
#                              names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1','e2','Missing'],header=0)
#     ax.plot(R.e2A,R.e1A,'.k')
#     # Strain state at the end of the second loading path
#     ax2.plot(R.e2,R.e1,'.k')
    
# def plot_path_single_case_dataset(analysismethod,data,case,color,fig,ax,fig_e,ax_e,**pathAfixed):
#     # **pathAfixed - not used currently
#     defaultKwargs = { 'pathAfixed': 'no'}
#     kwargs = { **defaultKwargs, **pathAfixed}    
#     #Plots the 
#     # ------Specify the input parameters------------
#     #Define the dataset to be analyzed
#     # fol=glob.glob('Data_07102021-*')
#     # os.chdir(fol[0])
#     #Load the postprocessed results with critical index Localization-all.txt
#     # fname=glob.glob(analysismethod)
#     # R=pd.read_csv(fname[0],delimiter= '\s+',index_col=False,
#     #         names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
#     R=data
    
#     # Rsub=R.loc[(R.e1f>0.4) & (R.e2f>0.2) & (R.e2f<0.3)]
    
#     #critical by case
#     # if kwargs['pathAfixed']=='yes':
#     #     case=case[8:]
#     print(os.getcwd())
#     Fname_seg_A=glob.glob(case) # porbleemsed 15892,5892
    
#     critical=R.loc[(R.Fol==Fname_seg_A[0].split('\\')[-1])]
#     theta=critical.theta.to_numpy()[0]
#     Phi_A=critical.fiA.to_numpy()[0]
#     Phi_B=critical.fiB.to_numpy()[0]
    
#     if kwargs['pathAfixed']=='no':
#         path =Fname_seg_A[0]+'/theta_'+str(theta)
#     else:
#         path='theta_'+str(theta)+ "/" +Fname_seg_A[0].split('\\')[-1]
    
    
#     # --------------End of input------------
#     #% Start processing current data
#     # if 
#     # Phi_A=float(Fname_seg_A[0][-13:-6].replace("_", "").replace("A","").replace("-B",""))
#     # Phi_B=float(Fname_seg_A[0][-5:].replace("_", ""))
    
#     # fig,ax=pyp.FLC_plot(1)
    
#     #Simulation results (element output)
#     # os.chdir(Fname_seg_A[0]+'/theta_'+str(theta))
#     os.chdir(path)
    
#     f=open('vdisp_BILINEAR-R1.for')
#     lines=f.readlines()
#     r1sim=float(lines[21].split('=')[1].replace("\n",""))
#     r2sim=float(lines[22].split('=')[1].replace("\n",""))
    
#     def add_sim_principal():
#         Files1=glob.glob('Principal_strains.r*')
#         for j,out_rpt in enumerate(Files1):
#                 e1i=np.loadtxt(out_rpt,skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
#                 e2i=np.loadtxt(out_rpt,skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
#                 e3i=np.loadtxt(out_rpt,skiprows=3,usecols = (6))
#                 ax.plot(e3i,e1i,':r',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))  
#                 ax.plot(e2i,e1i,':b')
#         # ax.legend(loc='best')           
#         ax.grid(b=True, which='major')
#         mainfol,_,lastfolder,analfolder=pyg.splitpath(os.getcwd())   
#         ax.set(title=mainfol[-20:-14]+' '+analfolder)
    
#     '''
#     This is to plot the applied strain defined in the VDISP.for file
#     '''
#     mainfol,_,lastfolder,analfolder=pyg.splitpath(os.getcwd())   
#     dt=0.0025
#     # ------------------------------------------------------------
#     # Approach 1 - calculate using the function
#     ep1,ep2=gd.applied_strain_hist(r1sim,r2sim,Phi_A,Phi_B,dt)
#     # Approach 2 - plot based on postprocessed results
#     xdat=np.array([0,critical.e2A.to_numpy()[0],critical.e2f.to_numpy()[0]])
#     ydat=np.array([0,critical.e1A.to_numpy()[0],critical.e1f.to_numpy()[0]])
#     ax.plot(xdat,ydat,'-',linewidth=0.5,label=analfolder)   
#     # ax.plot(ep2,ep1,'-r',linewidth=0.3,markersize=0.5,label=analfolder)
#     # ax.plot(xdat,ydat,'-k',linewidth=0.6,markersize=0.5,label=analfolder)
#     #------------------------------------------------------------------
#     IndxLoc=critical.Ind
#     #comparsion that calculated and in result file are in the same place
#     # ax.plot(ep2[IndxLoc],ep1[IndxLoc],'ob',markeredgewidth=0.4,markersize=5,markerfacecolor='None') 
#     ax.plot(critical.e2f,critical.e1f,'o'+color,linewidth=0.3,markersize=2)  
#     ax.legend(loc='best')  
#     # ax.set(title=analysismethod)
    
    
#     '''
#     Energy plot
#     '''    
#     ax_e.grid(which='major') 
#     ax_e.set(xlabel='Time',ylabel='Energy')
    
#     Files1=glob.glob('energy*')
#     for j,out_rpt in enumerate(Files1):    
#             ener_in=np.loadtxt(out_rpt,skiprows=3,usecols = (2))
#             ener_out=np.loadtxt(out_rpt,skiprows=3,usecols = (1))
#             time=np.loadtxt(out_rpt,skiprows=3,usecols = (0))
#             if len(out_rpt)>10:
#                 ax_e.plot(time,ener_in,'-'+color,label='1 element')    
                
#             else:   
#                 ax_e.plot(time[IndxLoc],ener_in[IndxLoc],'o'+color,markersize=4)
#                 ax_e.plot(time,ener_in,'-',label=analfolder)#'In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
#                 ax_e.plot(time,ener_out,'--')#'Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
#     ax_e.legend(loc='best')      
#     os.chdir("../..")

def datset_pn(analysismethod,**input):
#this function only imports the file into variable R
    fname_min=glob.glob(analysismethod)
    if input['combined']=='yes':
        Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
               names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing','Met'],header=0)    
    else:
        Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
               names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0) 
    
    # Ra=pd.read_csv(fname[0],delimiter= '\s+',index_col=False,
    #       names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1','e2','Missing'],header=0)
    

    defaultKwargs = { 'criteria': 'no'}
    kwargs = { **defaultKwargs, **input}  
    if kwargs['criteria']=='no':    
        R=Ra.loc[(Ra.Ind != 0) & (Ra.Ind < 399) ]
    else:
    # Create a subset of results
    # R=Ra.loc[(Ra.Ind >= 399)]   #this subset removes particular cases
    # R=Ra.loc[(Ra.Ind != 0)]   #this subset removes particular cases
    # R=Ra.loc[(Ra.Ind == 400) & (Ra.fiA<0)]   #this subset removes particular cases
    # R=Ra.loc[(Ra.Ind != 0) & (Ra.Ind >= 399) & (Ra.e1f<0.5) & (Ra.e1f>0.4)]
    # R=Ra.loc[(Ra.Ind != 0) & (Ra.Ind < 399) ] # the main cluster
    # R=Ra.loc[(Ra.e1f<0.02)]
        R=Ra.loc[(Ra.fiA==0.7) & (Ra.Ra<0.2) & (Ra.Ra>0.1)] #Cluster 2
    # R=Ra.loc[(Ra.e1f< 0.65) & (Ra.e1f>0.6) & (Ra.e2f>0.15) & (Ra.e2f<0.2) & (Ra.Ind != 0) & (Ra.Ind != 400)] #Cluster 3
    # R=Ra.loc[(Ra.e1f<0.4) & (Ra.e1f>0.35) & (Ra.e2f>0.245) & (Ra.e2f<0.3) & (Ra.Ind != 0) & (Ra.Ind != 400)] #Cluster 4
    # R=Ra.loc[(Ra.fiA>0.5) & (Ra.fiB<0) & (Ra.Ind != 400) & (Ra.Rb > 0.7)]
    # R=Ra.loc[(Ra.fiA>0.5) & (Ra.fiB<0) & (Ra.Ind != 400)]
     # the main cluster
    return R


def paper_res1_bilinear(fig_e,ax_e):
#----------------------Plot muhammed postprocessed--------------------------------
    cwd=os.getcwd()
    Model='Model2'  # SO-BI-LT10-R02-07102021-MLrandomdataset
#Create the energy figure
    
# Create FLC figure
    fig3= pickle.load(open('FLC-base.fig.pickle', 'rb'))
    ax3=plt.gca()    
    # fig3,ax3=pyp.FLC_plot(2)
    
    
    fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear-Damage')
    os.chdir(fol[0])
    # This adds the single step FLC on the plot
    # fig3,ax3=gd.plot_1_step_FLC('c:\work\ML_FLC\Model2\LinearBeam\BB-LT5-ALL-24112021-hc\Postprocessing\FLC_data_BB-LT5-ALL-24112021-hc.txt',2,plottype='normal')
    
    # fig3= pickle.load(open('FLC-base.fig.pickle', 'rb'))
    # ax3=plt.gca()
    
    # fig3,ax3=pyp.FLC_plot(2)
# Define which results to plot. These analysis method files have been postprocesssed earlier using Critical_index_all_RD.py
    analysismethod='Localization-min-Damage*.txt'
    # analysismethod='Localization-min-Damage*.txt'
    # analysismethod='POSTPROCESSED_ENERGY_DATA*.txt'

#These are for energy data
    if analysismethod=='POSTPROCESSED_ENERGY_DATA*.txt':
        Rend=datset_pn(analysismethod,combined='no')
        only_criticalB(fig3,ax3,analysismethod,Rend,'r',paths='no',alpha=1)
        compcase='V_02253*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)
        compcase='V_07302*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)    
        compcase='V_00337*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)       
        compcase='V_07158*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)      
        compcase='V_08376*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)     
    else:
        Rend=datset_pn(analysismethod,combined='no')
        only_criticalB(fig3,ax3,analysismethod,Rend,'r',paths='no',alpha=0.7)
        compcase='V_02255*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)
        # compcase='V_00021*'
        # plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)    
        compcase='V_00337*'
        plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)       
        # compcase='V_07158*'
        # plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)      
        # compcase='V_08376*'
        # plot_path_single_case_dataset(analysismethod,Rend,compcase,'g',fig3,ax3,fig_e,ax_e)       
    
    os.chdir(cwd)
# Energy figure    
    # ax_e.legend(loc=(1.1,0),prop={'size': 5})  
    fig_e.set_size_inches(pyp.cm2inch(6, 6))    
    ax_e.set(xlim=[0,1],ylim=[0,2e+6])
    fig_e.tight_layout()
    
    # ax_e.legend(loc=(0.04,0),prop={'size': 4})
    if analysismethod=='POSTPROCESSED_ENERGY_DATA*.txt':
        fig_e.savefig('dataset_energy-[energy_criterion].pdf',transparent=True)
    else:
        fig_e.savefig('dataset_energy-[damage_criterion].pdf',transparent=True) 
    
    
    
# FLc figure    
    # ax3.legend(loc=(1.1,0),prop={'size': 5})
    ax3.get_legend().remove()
    fig3.set_size_inches(pyp.cm2inch(6, 6))
    ax3.set(ylim=[0,1],xlim=[-0.8,0.8],xlabel='',ylabel='')
    fig3.tight_layout()        
    # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
    ax3.set_xticks(np.arange(-0.75,0.75+0.25,0.25))

    if analysismethod=='POSTPROCESSED_ENERGY_DATA*.txt':
        fig3.savefig('dataset_FLC-[energy_criterion].pdf',transparent=True)
    else:
        fig3.savefig('dataset_FLC-[damage-criterion].pdf',transparent=True)
        
    
if __name__ == "__main__":
    # function for results in the paper from bilinear dataset
    # fig_e,ax_e=pyp.newfigdef(1)
    # # paper_res1_bilinear(fig_e,ax_e) 
    fig= pickle.load(open('FLC-base.fig.pickle', 'rb'))
    ax=plt.gca()
    # only_criticalA(fig,ax,fig_e,ax_e)
    
# -----------# USE this case to plot from entire dataset
#     cwd=os.getcwd()
#     Model='Model2/Bilinear-Damage/'  # SO-BI-LT10-R02-07102021-MLrandomdataset
# # #Create the energy figure
    fig_e,ax_e=pyp.newfigdef(1)

#     fol=glob.glob('C:/work/ML_FLC/'+ Model)
#     os.chdir(fol[0])
#     # This adds the single step FLC on the plot
#     # fig3,ax3=gd.plot_1_step_FLC('c:\work\ML_FLC\Model2\LinearBeam\BB-LT5-ALL-24112021-hc\Postprocessing\FLC_data_BB-LT5-ALL-24112021-hc.txt',2,plottype='normal')
#     fig3,ax3=pyp.FLC_plot(2)
#     analysismethod='Localization-min-Damage-02-05-2022*.txt'
#     Rend=datset_pn(analysismethod,combined='no',criteria='yes')
#     only_criticalB(fig3,ax3,analysismethod,Rend,'k',paths='no',alpha=0.6)


# -----------# USE THIS TO PLOT ARBITRARY CASES WITH PATH
    paper_res1_bilinear(fig_e,ax_e)