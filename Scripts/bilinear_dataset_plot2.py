# -*- coding: utf-8 -*-
"""
Created on Thu Sep 15 09:05:29 2022

@author: Mihkel
"""

# fff


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('C:\work\ML_FLC\Scripts')
import Global_definitions as gd
import pickle
pi=4*np.arctan(1)




def paper_res1_bilinear(fig,ax,fig_e,ax_e):
#----------------------Plot muhammed postprocessed--------------------------------
    cwd=os.getcwd()
    Model='Model2'  # SO-BI-LT10-R02-07102021-MLrandomdataset
#Create the energy figure
    fol=glob.glob('C:/work/ML_FLC/'+ Model+'/Bilinear-Damage')
    os.chdir(fol[0])

    # 
# Define which results to plot. These analysis method files have been postprocesssed earlier using Critical_index_all_RD.py
    analysismethod='Localization-min-Damage*.txt'

#Fn that loads the data
    Rend=gd.datset_pn(analysismethod,combined='no')
    gd.only_criticalB(fig,ax,analysismethod,Rend,'r',paths='no',alpha=0.7)
    compcase='V_02255*'
    gd.plot_path_single_case_dataset(Rend,compcase,ax,ax_e)
        
    os.chdir(cwd)
# Energy figure    
    # ax_e.legend(loc=(1.1,0),prop={'size': 5})  
    fig_e.set_size_inches(pyp.cm2inch(6, 6))    
    ax_e.set(xlim=[0,1],ylim=[0,2e+6])
    fig_e.tight_layout()
    
    # ax_e.legend(loc=(0.04,0),prop={'size': 4})
    if analysismethod=='POSTPROCESSED_ENERGY_DATA*.txt':
        fig_e.savefig('dataset_energy-[energy_criterion].pdf',transparent=True)
    else:
        fig_e.savefig('dataset_energy-[damage_criterion].pdf',transparent=True) 
    
    
    
# FLc figure    
    # ax3.legend(loc=(1.1,0),prop={'size': 5})
    ax.get_legend().remove()
    fig.set_size_inches(pyp.cm2inch(6, 6))
    ax.set(ylim=[0,1],xlim=[-0.8,0.8],xlabel='',ylabel='')
    fig.tight_layout()        
    # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
    ax.set_xticks(np.arange(-0.75,0.75+0.25,0.25))

    fig.savefig('dataset_FLC-[damage-criterion].pdf',transparent=True)
        
    
if __name__ == "__main__":
    fig= pickle.load(open('FLC-base.fig.pickle', 'rb'))
    ax=plt.gca()
    # fig,ax=pyp.FLC_plot(1)
    # fig_e,ax_e=pyp.newfigdef(2)
    # fig,ax=pyp.newfigdef(1)
    fol=glob.glob("C:/work/ML_FLC/Model2_multimodel/Bilinear-reruns-combined/all2")
    # os.chdir(fol[0])
    analysis='Bilinear-results-strain.txt'
    Respath=f'{fol[0]}/{analysis}'
    R=gd.datset_pn(Respath,combined='no',criteria='yes')
    
    
    fig,ax=gd.only_criticalA(fig,ax,R)
    fig.savefig('Dataset-path-A.pdf',transparent=True)
    plt.close(fig)
    
    fig= pickle.load(open('FLC-base.fig.pickle', 'rb')); ax=plt.gca()
    fig,ax=gd.only_criticalB(fig,ax,R,alpha=0.9,paths='no')
    fig.savefig('Dataset-path-B.pdf',transparent=True)
    # compcase='V_09951*'
    # plot_path_single_case_dataset(analysis,R,compcase,'g',fig,ax,fig_e,ax_e)  
# def only_criticalB(fig2,ax2,fol,analysis,col,**input): 