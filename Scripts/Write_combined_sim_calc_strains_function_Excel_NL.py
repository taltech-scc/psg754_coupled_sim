

'''
This file reads the strain data from bilinear dataset and puts it to desired format:
such as time, corfii, and strain data. 
10.03.2022
First this was made on a data which included also equivalent plastic strain (c:\work\ML_FLC\Model2\Bilinear\Dataset\Postprocessed\).
Note that this data was generated based on c:\work\ML_FLC\Model2\Bilinear\Dataset critical theta values. HOwever, the 
re-simulations sometimes ended earlier, so the critical index values in POSTPROCESSED_ENERGY_DATA.txt are now always correct. 

18.03.2022
The second run of this script was made on the original dataset folder. This did not include the output of plastic strain,
but this was calculated manually. 



'''


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import ast
# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division
pi=4*np.arctan(1)


def postprocess_sim_to_strain_and_cotfii(R,dirname):
    '''
    Base case is the postprocessing of multimodel results
    
    Second case is the multimodel result reruns where only specific cases were simulated
    '''
    base_case=0
    for ind,e in enumerate(R.itertuples(index=False)):
            # if ind<7864:
            #     continue
            if base_case==1:
                fpath=f'theta_{e.theta}'
                linesr=[60,61]
            else:
                fpath=f'{e.Folder}' 
                linesr=[59,60]
            fi1=glob.glob(f'{fpath}/Principal_strains.r*')
            # try:
            #     F=pd.read_csv(fi1[0],delimiter= '\s+',index_col=False,
            #             names=['Time','e1out','e2out','e3out','e1','e2','e3','epsout','eps','Tout','Tin'],header=0)   
            # except:
            #TODO This read gives warning because in some cases i have more data here (eps values)    
            F=pd.read_csv(fi1[0],delimiter= '\s+',index_col=False,
                        names=['Time','e1out','e2out','e3out','e1','e2','e3'],header=0)  
            f=open(f'{fpath}/vdisp_NL.for')
            lines=f.readlines()            
            rarr=ast.literal_eval(lines[linesr[0]].split('=')[1].replace("\n","")[2:-2])   
            fiarr=ast.literal_eval(lines[linesr[1]].split('=')[1].replace("\n","")[2:-2])
            dt=0.0025
            ep1,ep2,tsw,Ri,Phi=gd.applied_strain_hist_long(dt,rarr,fiarr,'extra')

            # Ri,Phi=gd.applied_strain_hist_long(dt,rarr,fiarr,'extra')
            # tswitch=r1/(r1+r2)
            # Phi=np.zeros(len(F.Time))
            # for i,tim in enumerate(F.Time):
            #     # print(i)
            #     if tim<=tswitch:
            #         Phi[i]=e.fiA
            #     else:
            #         Phi[i]=e.fiB      
            Triax_calc=1/np.sqrt(3)*(1+Phi)/np.sqrt(1+Phi+Phi**2)
    
            # ep1,ep2=gd.applied_strain_hist(r1,r2,e.fiA,e.fiB,dt)
            ep3=-(ep2+ep1) #from compressibility condition
            eps_calc=np.sqrt(2)/3*np.sqrt((ep1-ep2)**2+(ep1-ep3)**2+(ep2-ep3)**2)
            results=pd.DataFrame()
    
            results['Time']=F.Time
            results['cotfii']=Phi
            results['e1sim']=F.e1
            results['e2sim']=F.e2
            results['e3sim']=F.e3
            # results['epssim']=F.eps
            # results['Tsim']=F.Tin
            results['e1_applied']=ep1[:len(F.Time)]
            results['e2_applied']=ep2[:len(F.Time)]
            results['e3_incomp']=ep3[:len(F.Time)]
            results['eps_applied']=eps_calc[:len(F.Time)]
            results['T_applied']=Triax_calc[:len(F.Time)]
            # res_file=e.Fol+'/theta_'+str(e.theta)+'/'+e.Fol+'_Res_postpocessed.xlsx'
            # res_file2='postprocessed_res/'+e.Fol+'_Res_postpocessed.xlsx'
            if ind==0:
                os.mkdir(dirname)
            path,drive,folder_fin,folder_prev=pyg.splitpath(os.getcwd())
            res_file2=f'{dirname}/{fpath}.xlsx'
            
            # results.to_excel(res_file,  index=None)
            results.to_excel(res_file2,  index=None)
            
if __name__ == "__main__":
    

    postprocess_fixed1=1

    
    # input 2------------------------------------------  
    if postprocess_fixed1==1:
        Model='Model2_multimodel'
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15')
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/BI-A-fix-V_000-a_0.70-T_0.663_R1-0.15')
        # fol=glob.glob('C:/work/ML_FLC/'+ Model+'/NonLinear_paths/NL-3linear/NL3_-0.95,0.2,0.8_short')
        fol=glob.glob(f'C:/work/ML_FLC/{Model}/BiLinear_A_Fixed_MM/BI-A-fix-V_000-a_0.80-R1_0.4/Data400-fia08')
        os.chdir(fol[0])
    
        # anal2='Localization-min-Damage*.txt'
        # R=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',index_col=False,
        #         names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0)
        anal2='HP-VB-400points-v1.txt'
        R=pd.read_csv(glob.glob(anal2)[0],delimiter= '\s+',header=0)        
        dt=0.0025 
        time=np.arange(0,1+dt,dt)
        # Ra=R.loc[(R.Fail_stage==3) & (R.Localization==True) & (R.Dcrit>0.98)] 
        # Ra=R.loc[(R.Rb>0.1)] 
        postprocess_sim_to_strain_and_cotfii(R,'Excel_SM')
        
  