import sys
import glob
import os
# import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import py_general as pyg
import pandas as pd
from matplotlib import cm
import python_plotting as pyp
from scipy.signal import savgol_filter
pi=4*np.arctan(1)

def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)

def plot_path_single_case_dataset(data,case,ax,ax_e,**pathAfixed):
    '''
Plots the results for specific analyzed case.
input is:
1) data - dataset where to look the case from
2) case - case='V_02255*', use glob to look the specific folder
3) ax - axis where the strain results are plotted
4) axe - axis where the energy results are plotted
5) **pathAfixed - when FLC results are plotted then yes, pathAfixed='yes', default no

    '''
    # **pathAfixed - not used currently
    defaultKwargs = { 'pathAfixed': 'no'}
    kwargs = { **defaultKwargs, **pathAfixed}    
    #Plots the 
    # ------Specify the input parameters------------
    #Define the dataset to be analyzed
    R=data
    
    # Rsub=R.loc[(R.e1f>0.4) & (R.e2f>0.2) & (R.e2f<0.3)]
    # print(os.getcwd())
    Fname_seg_A=glob.glob(case) # porbleemsed 15892,5892
    
    # critical=R.loc[(R.Fol==Fname_seg_A[0].split('\\')[-1])]
    critical=R.loc[(R.Fol==Fname_seg_A[0])]
    theta=critical.theta.to_numpy()[0]
    Phi_A=critical.fiA.to_numpy()[0]
    Phi_B=critical.fiB.to_numpy()[0]
    
    if kwargs['pathAfixed']=='no':
        path =Fname_seg_A[0]+'/theta_'+str(theta)
    else:
        path=f'theta_{theta}/{Fname_seg_A[0]}'
    # path=f'theta_{theta}/{Fname_seg_A[0].split('\\')[-1]}'
    
    # --------------End of input------------
    #% Start processing current data
    # if 
    # Phi_A=float(Fname_seg_A[0][-13:-6].replace("_", "").replace("A","").replace("-B",""))
    # Phi_B=float(Fname_seg_A[0][-5:].replace("_", ""))
    
    # fig,ax=pyp.FLC_plot(1)
    
    #Simulation results (element output)
    # os.chdir(Fname_seg_A[0]+'/theta_'+str(theta))
    os.chdir(path)
    
    f=open('vdisp_BILINEAR-R1.for')
    lines=f.readlines()
    r1sim=float(lines[21].split('=')[1].replace("\n",""))
    r2sim=float(lines[22].split('=')[1].replace("\n",""))
    



    def add_sim_principal():
        Files1=glob.glob('Principal_strains.r*')
        for j,out_rpt in enumerate(Files1):
                e1i=np.loadtxt(out_rpt,skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
                e2i=np.loadtxt(out_rpt,skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
                e3i=np.loadtxt(out_rpt,skiprows=3,usecols = (6))
                ax.plot(e3i,e1i,':r',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))  
                ax.plot(e2i,e1i,':b')
        # ax.legend(loc='best')           
        ax.grid(b=True, which='major')
        mainfol,_,lastfolder,analfolder=pyg.splitpath(os.getcwd())   
        ax.set(title=mainfol[-20:-14]+' '+analfolder)
    
    '''
    This is to plot the applied strain defined in the VDISP.for file
    '''
    mainfol,_,lastfolder,analfolder=pyg.splitpath(os.getcwd())   
    dt=0.0025
    # ------------------------------------------------------------
    # Approach 1 - calculate using the function
    ep1,ep2=applied_strain_hist(r1sim,r2sim,Phi_A,Phi_B,dt)
    # Approach 2 - plot based on postprocessed results
    xdat=np.array([0,critical.e2A.to_numpy()[0],critical.e2f.to_numpy()[0]])
    ydat=np.array([0,critical.e1A.to_numpy()[0],critical.e1f.to_numpy()[0]])
    ax.plot(xdat,ydat,'-',linewidth=0.5,label=analfolder)   
    # ax.plot(ep2,ep1,'-r',linewidth=0.3,markersize=0.5,label=analfolder)
    # ax.plot(xdat,ydat,'-k',linewidth=0.6,markersize=0.5,label=analfolder)
    #------------------------------------------------------------------
    IndxLoc=critical.Ind
    #comparsion that calculated and in result file are in the same place
    # ax.plot(ep2[IndxLoc],ep1[IndxLoc],'ob',markeredgewidth=0.4,markersize=5,markerfacecolor='None') 
    ax.plot(critical.e2f,critical.e1f,'o',linewidth=0.3,markersize=2)  
    ax.legend(loc='best')  
    # ax.set(title=analysismethod)
    
    
    '''
    Energy plot
    '''    
    ax_e.grid(which='major') 
    ax_e.set(xlabel='Time',ylabel='Energy')
    
    Files1=glob.glob('energy*')
    for j,out_rpt in enumerate(Files1):    
            ener_in=np.loadtxt(out_rpt,skiprows=3,usecols = (2))
            ener_out=np.loadtxt(out_rpt,skiprows=3,usecols = (1))
            time=np.loadtxt(out_rpt,skiprows=3,usecols = (0))
            if len(out_rpt)>10:
                ax_e.plot(time,ener_in,'-k',label='1 element')    
                
            else:   
                ax_e.plot(time[IndxLoc],ener_in[IndxLoc],'ok',markersize=4)
                ax_e.plot(time,ener_in,'-',label=analfolder)#'In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
                ax_e.plot(time,ener_out,'--')#'Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
    ax_e.legend(loc='best')      
    os.chdir("../..")





def datset_pn(analysismethod,**input):
    '''
    this function only imports the file into variable R 
    analysismethod - is the name of the file containing the results
    **input - combined='yes' or 'no' depending on how data header looks
    **input - criteria='yes' or 'no' depending on whether dataset is constrained with. If yes, constraint is defined in the code here
    '''

    fname_min=glob.glob(analysismethod)
    if input['combined']=='yes':
        Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
               names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing','Met'],header=0)    
    elif input['combined']=='nohead':
        Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,header=0)
    else:
        Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
               names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','Dcrit','e1A','e2A','e1f','e2f','Stage'],header=0) 
    
# criteria for extra
    defaultKwargs = { 'criteria': 'no'}
    kwargs = { **defaultKwargs, **input}  
    if kwargs['criteria']=='no':    
        # R=Ra.loc[(Ra.Ind != 0) & (Ra.Ind < 399) ]
        R=Ra
    else:
        R=Ra.loc[(Ra.Rb>0.1)]
    # Create a subset of results
    # R=Ra.loc[(Ra.Ind != 0) & (Ra.Ind < 399) ] # the main cluster
    # R=Ra.loc[(Ra.e1f<0.02)]
        # R=Ra.loc[(Ra.fiA==0.7) & (Ra.Ra<0.2) & (Ra.Ra>0.1)] #Cluster 2
    # R=Ra.loc[(Ra.e1f< 0.65) & (Ra.e1f>0.6) & (Ra.e2f>0.15) & (Ra.e2f<0.2) & (Ra.Ind != 0) & (Ra.Ind != 400)] #Cluster 3
    # R=Ra.loc[(Ra.e1f<0.4) & (Ra.e1f>0.35) & (Ra.e2f>0.245) & (Ra.e2f<0.3) & (Ra.Ind != 0) & (Ra.Ind != 400)] #Cluster 4
    return R

def only_criticalB(fig2,ax2,analysismethod,data,col,**input): 
    R=data
    ax2.plot(R.e2f,R.e1f,'.'+col,markeredgecolor='none',markersize=1,alpha=input['alpha'])
    # ax2.plot(Rend.e2f,Rend.e1f,'ob',markeredgecolor='none',markersize=1)
    
    # adding total paths to points
    if input['paths']=='yes':
        dt=0.0025
        for index, r in R.iterrows():
            # print(r.Ra) ---> row of the dataframe
        # for i in R:
            # testR2=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
            # ep1,ep2=gd.applied_strain_hist(r.Ra,r.Rb,r.fiA,r.fiB,dt)
            xdat=[0,r.e2A,r.e2f]; ydat=[0,r.e1A,r.e1f] 
            ax2.plot(xdat,ydat,'-',color=[0.8,0.8,0.8],linewidth=0.3,zorder=1)   
    
    return R 

def critical_indx(E,N,skip,col,*args): 
    # if cotB>0:
    #     crit=0.6
    # else:
    #     crit=1
    #Critical values are chosen based on following considerations. There can be very large
    # difference in slope, but when energy value is similar, then this is not detected as the 
    # localization point. This is to overcome the path change instabilities. 
    crit=0.5  #nominal difference in slope
    crit_energy=0.1    #nominal difference in energy values (used to be 0.1)
    
    # E-stands for the beam analysis results
    # E1-stands for single element analysis results
    
    #calculate differentials
    dt= np.diff(E.Time)
    dEout=np.diff(E.out)
    dEin=np.diff(E.inn)
    # dE1=np.diff(E1.inn);  dt1= np.diff(E1.Time); dt1[-1]=dt1[-2]
    #Calculate slopes in energy-time curve
    sl_inn=dEin/dt
    sl_out=dEout/dt     #
    # slE1=dE1/dt1
    # slE1[-1]=slE1[-2]
    # print(len(sl_inn),len(slE1[:len(dt)]))
    # minlen=min(len(dt),len(dt1))
    #Slope ratio (comparison with single element solution)
    slm=sl_inn/(sl_out+0.0000000001)      # slope ratio, the length (e.g. 269) of this will be 1 less than initial length (e.g. len(E.t)=270)
    # Modified 13.10.2021: instead of inner element use outer element
    #slm=sl_out/(slE1[:len(dt)]+0.0000000001)
    # slm=sl_inn[:minlen]/(slE1[:minlen]+0.0000000001)       # slope ratio, the length (e.g. 269) of this will be 1 less than initial length (e.g. len(E.t)=270)
    x=running_mean(slm, N)    #the length of this will be N shorter than initial length 270-6=264
    #ADditional criteria, in some cases the slope ratio can wrong value. In combined with difference criteria everything should run somoothly
    ediff=pd.to_numeric((E.inn/E.out)**2,errors='coerce').to_numpy()
    ediff2=running_mean(ediff[1:],N)
    # print(len(args))
    if len(args)>0:
        ax1=args[0]
        ax1.plot(E.Time[N:][skip:],x[skip:],'-',color=col)          #start plotting from second stage
        ax1.plot([0,1],[2,2],'--')
        ax1.plot([0,1],[0,0],'--')
        ax1.set(ylim=[-1,3])
    ind=[]
    
    x_skipped=x[skip:]          #slope ratio of single element model and beam model element in the middle
    ediff2_skip=ediff2[skip:]   #
    
    try: 
        # slope difference between energy in beam and 1element analysis is larger than 10%
        ind.append(np.min(np.where((x[skip:]>(1+crit))*(ediff2[skip:skip+len(x[skip:])]>1+crit_energy))))
        # ind.append(np.min(np.where((x[skip:]>(1+crit))*(ediff2[skip:]>1+crit_energy))))
    except:
        ind.append(len(E.Time))
    try: 
        # slope difference between energy in beam and 1element analysis is less than 10%
        ind.append(np.min(np.where((x[skip:]<(1-crit))*(ediff2[skip:skip+len(x[skip:])]>1+crit_energy))))
        # ind.append(np.min(np.where((x[skip:]<(1-crit))*(ediff2[skip:]>1+crit_energy))))
    except:
        ind.append(len(E.Time)) 
        
    indx=np.min(ind)+N+skip         #To make up the missing values due to running mean process (add N to index value) and initial values skipped from the array (skip)
    if indx>len(E.Time):
        indx=len(E.Time)-1  
    # if indx==0:
    #     indx=len(E.t)-1     #to avoid cases where data was not written oout completely
    return indx

def critical_indx_ep(PE,PE1,skip,col,*args): #was (PE,PE1,skip,col,*args):
    crit=1 
    ind=[]
    # calculation of the strain rate for band
    dt= np.diff(PE.Time)
    de1_band=np.diff(PE.e1i)
    v_in=de1_band/dt
# calculation of the strain rate for single element
    de1_single=np.diff(PE1.e1i)
    dt_single= np.diff(PE1.Time) 
    v_single=de1_single/dt_single    
    
    v_in_skip=v_in[skip:]     
    try: 
        # slope difference between energy in beam and 1element analysis is larger than 10%
        ind.append(np.min(np.where((v_in_skip>(1+crit)))))
    except:
        ind.append(len(v_in_skip))            
    # try: 
    #     # slope difference between energy in beam and 1element analysis is less than 10%
    #     ind.append(np.min(np.where((v_in_skip>(1-crit)))))
    # except:
    #     ind.append(len(E.t)) 
    indx=np.min(ind)+skip
    # print(len(args))
    if len(args)>0:
        ax2=args[0]
        y2ax2 = ax2.twinx() 
        y2ax2.plot(PE.Time,PE.e1i,'-r'); y2ax2.plot(PE1.Time,PE1.e1i,'-k')
        ax2.plot(PE.Time[1:],v_in,'-r',label='vin') 
        ax2.plot(PE1.Time[1:],v_single,'-k',label='v1-single')                         
        ax2.plot(PE1.Time[indx],v_single[indx],'or')     
    return indx

def strain_state(ra,rb,fiA,fiB,Ind,dt,*args):
    pi=4*np.arctan(1)
    max_steps=int(1/dt)
    tswitch=ra/(ra+rb)
    # Strain state at the end of the first loading path
    # for i in Ind:
    #     if i<=max_steps:
    #         print(i,max_steps)
    e1A=ra*np.sin(pi/2.-np.arctan(fiA+1e-10))#*(Ind/max_steps)/tswitch
    e2A=ra*np.cos(pi/2.-np.arctan(fiA+1e-10))#*(Ind/max_steps)/tswitch
        # else:
        #     e1A=ra*np.sin(pi/2.-np.arctan(fiA+1e-10))
        #     e2A=ra*np.cos(pi/2.-np.arctan(fiA+1e-10))

    # Strain state at the end of the second loading path
    tswitch2=rb/(ra+rb)
    for i in args:
        tswitch2=args[0]        # this is provided since in SO-BI-LT10-R01-28062021 simulations fortran files tswitch was used instead of tswitch2

    e1B=rb*np.sin(pi/2.-np.arctan(fiB+1e-10))*(Ind/max_steps-tswitch)/tswitch2         # (tim-tswitch)/tswitch2
    e2B=rb*np.cos(pi/2.-np.arctan(fiB+1e-10))*(Ind/max_steps-tswitch)/tswitch2 
    e1=e1A+e1B    
    e2=e2A+e2B                                         
    return (e1A,e2A,e1,e2,e1B,e2B)

def applied_strain_hist(r1,r2,fiA,fiB,dt):
    '''
    This is to plot the applied strain
    '''
    # dt=0.0025
    # dt=0.1
    time=np.arange(0,1+dt,dt)
    phi1=fiA
    phi2=fiB
    # r1=0.6
    # r2=0.2
    # phi1=0
    # phi2=1
    ep1=np.zeros(len(time))
    ep2=np.zeros(len(time))
    tswitch=r1/(r1+r2)
    tswitch2=r2/(r1+r2)
    for i,tim in enumerate(time):   
        if tim<=tswitch:
            phi=phi1
            radius=r1
            e1s1=radius*np.sin(pi/2.-np.arctan(phi+1e-10))*tim/tswitch
            e2s1=radius*np.cos(pi/2.-np.arctan(phi+1e-10))*tim/tswitch
            e1s2=0.
            e2s2=0.
        else:
            e1s1=r1*np.sin(pi/2.-np.arctan(phi1+1e-10))
            e2s1=r1*np.cos(pi/2.-np.arctan(phi1+1e-10))
            phi=phi2
            radius=r2
            e1s2=radius*np.sin(pi/2.-np.arctan(phi+1e-10))*(tim-tswitch)/tswitch2
            e2s2=radius*np.cos(pi/2.-np.arctan(phi+1e-10))*(tim-tswitch)/tswitch2
        ep1[i]=e1s1+e1s2
        ep2[i]=e2s1+e2s2
    return (ep1,ep2)

    # ax.plot(ep2,ep1,'-',linewidth=0.5)   

def plot_1_step_FLC(filepath,*fignr,**plottype):
    # os.chdir(filepath)
    defaultKwargs = { 'plottype': 'flc'}
    plottype = { **defaultKwargs, **plottype}
    if len(fignr)>0:
        if plottype['plottype']=='normal':
            fig,ax=pyp.newfigdef(fignr[0])
        elif plottype['plottype']=='flc':
            fig,ax=pyp.FLC_plot(fignr[0])
    else:
        fig,ax=pyp.FLC_plot()
    # if len(fignr)>0:
    #     fig,ax=pyp.newfigdef(fignr[0])
    # else: 
    #     fig,ax=pyp.newfigdef()
    # results_linear='FLC_data_BB-LT5-ALL-24112021-hc.txt'
    FLC=pd.read_csv(filepath,delimiter= '\s+',names=["e1", "e2"],skiprows=2, index_col=False)   
    ax.plot(FLC.e2,FLC.e1,'.-b',linewidth=1,zorder=10)
    return fig,ax 


def critical_indx_25_01_2022(E,E1,N,skip,col,*args): 
    # if cotB>0:
    #     crit=0.6
    # else:
    #     crit=1
    #Critical values are chosen based on following considerations. There can be very large
    # difference in slope, but when energy value is similar, then this is not detected as the 
    # localization point. This is to overcome the path change instabilities. 
    crit=0.5  #nominal difference in slope
    crit_energy=0.1    #nominal difference in energy values (used to be 0.1)
    
    # E-stands for the beam analysis results
    # E1-stands for single element analysis results
    
    #calculate differentials
    dt= np.diff(E.Time)
    dEout=np.diff(E.out)
    dEin=np.diff(E.inn)
    dE1=np.diff(E1.inn);  dt1= np.diff(E1.Time); dt1[-1]=dt1[-2]
    #Calculate slopes in energy-time curve
    sl_inn=dEin/dt
    sl_out=dEout/dt     #
    slE1=dE1/dt1
    slE1[-1]=slE1[-2]
    # print(len(sl_inn),len(slE1[:len(dt)]))
    # minlen=min(len(dt),len(dt1))
    #Slope ratio (comparison with single element solution)
    slm=sl_inn[:len(dt1)]/(slE1[:len(dt)])       # slope ratio, the length (e.g. 269) of this will be 1 less than initial length (e.g. len(E.t)=270)
    # Modified 13.10.2021: instead of inner element use outer element
    #slm=sl_out/(slE1[:len(dt)]+0.0000000001)
    # slm=sl_inn[:minlen]/(slE1[:minlen]+0.0000000001)       # slope ratio, the length (e.g. 269) of this will be 1 less than initial length (e.g. len(E.t)=270)
    x=running_mean(slm, N)    #the length of this will be N shorter than initial length 270-6=264
    #ADditional criteria, in some cases the slope ratio can wrong value. In combined with difference criteria everything should run somoothly
    ediff=pd.to_numeric((E.inn/E.out)**2,errors='coerce').to_numpy()
    ediff2=running_mean(ediff[1:],N)
    # print(len(args))
    if len(args)>0:
        ax1=args[0]
        ax1.plot(E.Time[N:][skip:],x[skip:],'-',color=col)          #start plotting from second stage
        ax1.plot([0,1],[2,2],'--')
        ax1.plot([0,1],[0,0],'--')
        ax1.set(ylim=[-1,3])
    ind=[]
    
    x_skipped=x[skip:]          #slope ratio of single element model and beam model element in the middle
    ediff2_skip=ediff2[skip:]   #
    
    try: 
        # slope difference between energy in beam and 1element analysis is larger than 10%
        ind.append(np.min(np.where((x[skip:]>(1+crit))*(ediff2[skip:skip+len(x[skip:])]>1+crit_energy))))
        # ind.append(np.min(np.where((x[skip:]>(1+crit))*(ediff2[skip:]>1+crit_energy))))
    except:
        ind.append(len(E.Time))
    try: 
        # slope difference between energy in beam and 1element analysis is less than 10%
        ind.append(np.min(np.where((x[skip:]<(1-crit))*(ediff2[skip:skip+len(x[skip:])]>1+crit_energy))))
        # ind.append(np.min(np.where((x[skip:]<(1-crit))*(ediff2[skip:]>1+crit_energy))))
    except:
        ind.append(len(E.Time)) 
        
    indx=np.min(ind)+N+skip         #To make up the missing values due to running mean process (add N to index value) and initial values skipped from the array (skip)
    if indx>len(E.Time):
        indx=len(E.Time)-1  
    # if indx==0:
    #     indx=len(E.t)-1     #to avoid cases where data was not written oout completely
    return indx
# def add_sim_principal():
#     Files1=glob.glob('Principal_strains.r*')
#     for j,out_rpt in enumerate(Files1):
#             e1i=np.loadtxt(out_rpt,skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
#             e2i=np.loadtxt(out_rpt,skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
#             e3i=np.loadtxt(out_rpt,skiprows=3,usecols = (6))
#             ax.plot(e3i,e1i,':r',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))  
#             ax.plot(e2i,e1i,':b')
#     # ax.legend(loc='best')           
#     ax.grid(b=True, which='major')
#     # mainfol,_,lastfolder=pyg.splitpath(os.getcwd())   
#     # ax.set(title=mainfol[-20:-14]+' '+lastfolder)
        
def applied_strain_hist_long(dt,rarr,fiarr,*extra):
    '''
    This is to plot the applied strain
    '''
    # dt=0.0025
    time=np.arange(0,1+dt,dt)
    ep1=np.zeros(len(time))
    ep2=np.zeros(len(time))
    # tswitch=r1/(r1+r2)
    # tswitch2=r2/(r1+r2)
    radius=rarr
    phi=fiarr
    tsw=[]
    Ri_list=[]
    fi_list=[]
    for r in radius:
        tsw.append(r/sum(radius)) # calculates the duration of each segment with respect to the length R
    
    ep1_end=[]
    ep2_end=[]
    # tsw=[0.1,0.3,0.6]
    j=0
    for i,tim in enumerate(time): 
        fi=phi[j]
        Ri=radius[j] 
        Ri_list.append(Ri)
        fi_list.append(fi)
        coeff=(tim-sum(tsw[:j]))/tsw[j]
        if tim>=sum(tsw[:j+1]):
            if tim!=1:
                j+=1
                ep1_end.append(Ri*np.sin(pi/2.-np.arctan(fi+1e-10)))
                ep2_end.append(Ri*np.cos(pi/2.-np.arctan(fi+1e-10)))
                coeff=0
 
        # print((tim-sum(tsw[:j]))/tsw[j])
        e1s1=Ri*np.sin(pi/2.-np.arctan(fi+1e-10))*coeff
        e2s1=Ri*np.cos(pi/2.-np.arctan(fi+1e-10))*coeff

        ep1[i]=e1s1+sum(ep1_end)
        ep2[i]=e2s1+sum(ep2_end)
        # print('{:>6.3f}{:>6.3f}{:>3d}  {:>6.3f}   {:>6.3f}   {:>6.3f}'.
        #     format(tim, (tim-sum(tsw[:j])),j,sum(ep1_end),coeff,ep1[i]))
# if you want to plot by running in main then uncomment this section
        # ax.plot(ep2[i],ep1[i],'or',linewidth=0.5,zorder=1) 
        # plt.draw()
        # plt.show()
#-------------------------------------------------------------
    if len(extra)==0:
        return (ep1,ep2,tsw) 
    else:
        Ri_list=np.array(Ri_list)
        fi_list=np.array(fi_list)
        return (ep1,ep2,tsw,Ri_list,fi_list)   
        
        
def sim_strain_hist_plot_folder(pdir,*args):
#plots the principal strains in folder pdir
# TODO 
# N
# namehead must be checked with data input, if wrong, then might not plot correctly
    if len(args)==0:
        fig,ax=pyp.FLC_plot(1)
        case=''
    else:
        fig=args[0]
        ax=args[1]
        case=args[2]   
        ind=args[3]
    # os.chdir(pdir)
    Files1=glob.glob(f'{pdir}/Principal_strains{case}.rpt')
    # for j,out_rpt in enumerate(Files1):  
# NOTE the first namehead version is used with MM model, second one with SM model. Most common cause of error. 
    # namehead2=["Time", "e1out", "e2out",'e3out',"e1in", "e2in",'e3in']
    namehead2=["Time", "e1out", "e2out",'e3out',"e1in", "e2in",'e3in',"epsout",'epsin',"Tout",'Tin']    #This should be used with reruns where plastic strain is also asked
    e=pd.read_csv(Files1[0],delimiter= '\s+',names=namehead2,header=0,na_values='-nan(ind)')
    ax.plot(e.e3in,e.e1in,'-b',label='e3-e1')   
    ax.plot(e.e2in,e.e1in,'-r',linewidth=0.5,label='e2-e1') 
    # add index
    if len(args)!=0:
        ax.plot(e.e3in[ind],e.e1in[ind],'or') 
        ax.plot(e.e2in[ind],e.e1in[ind],'or') 
    # plt.show()          
def get_strains(pdir,case):
    Files1=glob.glob(f'{pdir}/Principal_strains{int(case)}.rpt')
    
    
    

def plot_triax(pdir):
    fig,ax=pyp.newfigdef()
    os.chdir(pdir)
    Files1=glob.glob('Principal_strains.rpt')
    namehead2=["Time", "e1out", "e2out",'e3out',"e1in", "e2in",'e3in',"epsout",'epsin',"Tout",'Tin']    #This should be used with reruns where plastic strain is also asked
    e=pd.read_csv(Files1[0],delimiter= '\s+',names=namehead2,header=0,na_values='-nan(ind)')
    ax.plot(e.Tin,e.epsin,'-b')   

def sim_energy_curve_plot(pdir,*args):
    if len(args)==0:
        case=''
    else:
        case=args[0]   
    fige,axe=pyp.newfigdef(2)
    Files1=glob.glob(f'{pdir}/energy{case}.rpt')
    namehead=["Time", "out", "inn"]
    E=pd.read_csv(Files1[0],delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
    axe.plot(E.Time,E.inn,'--r',linewidth=0.5)
    axe.plot(E.Time,E.out,'--',linewidth=0.5)
    # ax.plot(E.Time[indx],E.out[indx],'o',linewidth=0.5)
    axe.legend(loc='best') 
    return fige,axe

def only_criticalA(fig,ax,dataset):
    # cwd=os.getcwd()
    # max_steps=int(1/0.0025)
    # Results=np.loadtxt(fname[0] ,skiprows=1)
    # Model='Model2' 
    # fol=glob.glob(fol)
    # os.chdir(fol[0])
# Define which results to plot. These analysis method files have been postprocesssed earlier using Critical_index_all_RD.py
    R=dataset
    tswitch=R.Ra/(R.Ra+R.Rbtot)
    
    # Strain state at the end of the first loading path
    e1A=R.Ra*np.sin(pi/2.-np.arctan(R.fiA+1e-10))
    e2A=R.Ra*np.cos(pi/2.-np.arctan(R.fiA+1e-10))
    
    # fig,ax=pyp.FLC_plot(1)
    ax.plot(e2A,e1A,'.k',markeredgecolor='none',markersize=1,alpha=0.5)#,zorder=10
#-------------------------------------------    
    # compcase='V_09951*'
    # plot_path_single_case_dataset(analysis,R,compcase,'g',fig,ax,fig_e,ax_e)   
    # compcase='V_00337*'
    # plot_path_single_case_dataset(analysismethod,R,compcase,'g',fig,ax,fig_e,ax_e)   
#------------------------------------------------------    
    # FLc figure    
    # ax.legend(loc=(1.1,0),prop={'size': 5})
    # ax.get_legend().remove()
    fig.set_size_inches(pyp.cm2inch(6, 6))
    ax.set(ylim=[0,0.9],xlim=[-0.8,0.8],xlabel='',ylabel='')
    fig.tight_layout()        
    # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
    ax.set_xticks(np.arange(-0.75,0.75+0.25,0.25))
    ax.set_yticks(np.arange(0,0.9+0.2,0.2))
    # os.chdir(cwd)
    # fig.savefig('dataset_FLC-[damage-criterion]-1stpath.pdf',transparent=True)
    return fig,ax
    
def only_criticalB(fig,ax,dataset,**input): 

# Define which results to plot. These analysis method files have been postprocesssed earlier using Critical_index_all_RD.py
    R=dataset

    ax.plot(R.e2f,R.e1f,'.-r',markeredgecolor='none',markersize=3,alpha=input['alpha'])
    # ax2.plot(Rend.e2f,Rend.e1f,'ob',markeredgecolor='none',markersize=1)
    
    # adding total paths to points
    if input['paths']=='yes':
        dt=0.0025
        for index, r in R.iterrows():
            # print(r.Ra) ---> row of the dataframe
        # for i in R:
            # testR2=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
            # ep1,ep2=gd.applied_strain_hist(r.Ra,r.Rb,r.fiA,r.fiB,dt)
            xdat=[0,r.e2A,r.e2f]; ydat=[0,r.e1A,r.e1f] 
            ax.plot(xdat,ydat,'-',color=[0.8,0.8,0.8],linewidth=0.3,zorder=1)   
    fig.set_size_inches(pyp.cm2inch(6, 6))
    ax.set(ylim=[0,0.9],xlim=[-0.8,0.8],xlabel='',ylabel='')
    fig.tight_layout()        
    # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
    ax.set_xticks(np.arange(-0.75,0.75+0.25,0.25))
    ax.set_yticks(np.arange(0,0.9+0.2,0.2))
    return fig,ax

def only_criticalC(fig,ax,dataset,*args): 
    '''
    args(0)=foldername of single case, if only one case needs to be plotted
    '''
    def plotcase(e):
        rarray=[e.Ra,e.Rb,e.Rc]     #note the total Rc, not the localization value
        phiarr=[e.cotfiiA,e.cotfiiB,e.cotfiiC]
        ep1,ep2,tsw=applied_strain_hist_long(dt,rarray,phiarr)
        ax.plot(ep2,ep1,'-k',linewidth=0.6,markersize=0.5,alpha=0.3)   
        ax.plot(ep2[e.Index],ep1[e.Index],'or',linewidth=1,markersize=2,markerfacecolor='None') 
        plt.show()
    
    if len(args)>0:
        singlecase=args[0]   
    else:
        singlecase=0 
    
    dt=0.0025
# Define which results to plot. These analysis method files have been postprocesssed earlier using Critical_index_all_RD.py
    R=dataset
    for _,e in enumerate(R.itertuples(index=False)):
        if singlecase==0:
        
        # if singlecase==0:
        # StrainFile=glob.glob(f'{e.Folder}/Principal_strains.r*')
        # Cri_strains=pd.read_csv(StrainFile[0],delimiter= '\s+',index_col=False,
        #             names=['Time','e1out','e2out','e3out','e1','e2','e3','epsout','eps','Tout','T'],header=0,na_values='-nan(ind)')
            plotcase(e)
        else:
            if singlecase==e.Folder:
                plotcase(e)
            # array=[e.Ra,e.Rb,e.Rc]     #note the total Rc, not the localization value
            # phiarr=[e.cotfiiA,e.cotfiiB,e.cotfiiC]
            # ep1,ep2,tsw=applied_strain_hist_long(dt,rarray,phiarr)
            # ax.plot(ep2,ep1,'-k',linewidth=0.6,markersize=0.5,alpha=0.3)   
            # ax.plot(ep2[e.Index],ep1[e.Index],'or',linewidth=1,markersize=2,markerfacecolor='None') 
            # plt.show()
        # plt.pause(1)
        

    # ax.plot(R.e2f,R.e1f,'.-r',markeredgecolor='none',markersize=3,alpha=input['alpha'])
    # # ax2.plot(Rend.e2f,Rend.e1f,'ob',markeredgecolor='none',markersize=1)
    
    # # adding total paths to points
    # if input['paths']=='yes':
    #     dt=0.0025
    #     for index, r in R.iterrows():
    #         # print(r.Ra) ---> row of the dataframe
    #     # for i in R:
    #         # testR2=(e1_min-r1*np.sin(pi/2.-np.arctan(cotfii_a+1e-10)))/np.sin(pi/2.-np.arctan(cotfii_b+1e-10))
    #         # ep1,ep2=gd.applied_strain_hist(r.Ra,r.Rb,r.fiA,r.fiB,dt)
    #         xdat=[0,r.e2A,r.e2f]; ydat=[0,r.e1A,r.e1f] 
    #         ax.plot(xdat,ydat,'-',color=[0.8,0.8,0.8],linewidth=0.3,zorder=1)   
    # fig.set_size_inches(pyp.cm2inch(6, 6))
    # ax.set(ylim=[0,0.9],xlim=[-0.8,0.8],xlabel='',ylabel='')
    # fig.tight_layout()        
    # # ax3.legend(loc=(0.04,0),prop={'size': 5}) 
    # ax.set_xticks(np.arange(-0.75,0.75+0.25,0.25))
    # ax.set_yticks(np.arange(0,0.9+0.2,0.2))
    return fig,ax
  


  
def plot_path_single_case_dataset(analysismethod,data,case,color,fig,ax,fig_e,ax_e,**pathAfixed):
    # **pathAfixed - not used currently
    defaultKwargs = { 'pathAfixed': 'no'}
    kwargs = { **defaultKwargs, **pathAfixed}    
    #Plots the 
    # ------Specify the input parameters------------
    #Define the dataset to be analyzed
    # fol=glob.glob('Data_07102021-*')
    # os.chdir(fol[0])
    #Load the postprocessed results with critical index Localization-all.txt
    # fname=glob.glob(analysismethod)
    # R=pd.read_csv(fname[0],delimiter= '\s+',index_col=False,
    #         names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
    R=data
    
    # Rsub=R.loc[(R.e1f>0.4) & (R.e2f>0.2) & (R.e2f<0.3)]
    
    #critical by case
    # if kwargs['pathAfixed']=='yes':
    #     case=case[8:]
    print(os.getcwd())
    Fname_seg_A=glob.glob(case) # porbleemsed 15892,5892
    
    critical=R.loc[(R.Fol==Fname_seg_A[0].split('\\')[-1])]
    theta=critical.theta.to_numpy()[0]
    Phi_A=critical.fiA.to_numpy()[0]
    Phi_B=critical.fiB.to_numpy()[0]
    
    if kwargs['pathAfixed']=='no':
        path =Fname_seg_A[0]+'/theta_'+str(theta)
    else:
        path='theta_'+str(theta)+ "/" +Fname_seg_A[0].split('\\')[-1]
    
    
    # --------------End of input------------
    #% Start processing current data
    # if 
    # Phi_A=float(Fname_seg_A[0][-13:-6].replace("_", "").replace("A","").replace("-B",""))
    # Phi_B=float(Fname_seg_A[0][-5:].replace("_", ""))
    
    # fig,ax=pyp.FLC_plot(1)
    
    #Simulation results (element output)
    # os.chdir(Fname_seg_A[0]+'/theta_'+str(theta))
    os.chdir(path)
    
    f=open('vdisp_BILINEAR-R1.for')
    lines=f.readlines()
    r1sim=float(lines[21].split('=')[1].replace("\n",""))
    r2sim=float(lines[22].split('=')[1].replace("\n",""))
    
    def add_sim_principal():
        Files1=glob.glob('Principal_strains.r*')
        for j,out_rpt in enumerate(Files1):
                e1i=np.loadtxt(out_rpt,skiprows=3,usecols = (4)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
                e2i=np.loadtxt(out_rpt,skiprows=3,usecols = (5)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
                e3i=np.loadtxt(out_rpt,skiprows=3,usecols = (6))
                ax.plot(e3i,e1i,':r',label='In '+' A='+str(Phi_A)+' B='+str(Phi_B))  
                ax.plot(e2i,e1i,':b')
        # ax.legend(loc='best')           
        ax.grid(b=True, which='major')
        mainfol,_,lastfolder,analfolder=pyg.splitpath(os.getcwd())   
        ax.set(title=mainfol[-20:-14]+' '+analfolder)
    
    '''
    This is to plot the applied strain defined in the VDISP.for file
    '''
    mainfol,_,lastfolder,analfolder=pyg.splitpath(os.getcwd())   
    dt=0.0025
    # ------------------------------------------------------------
    # Approach 1 - calculate using the function
    ep1,ep2=applied_strain_hist(r1sim,r2sim,Phi_A,Phi_B,dt)
    # Approach 2 - plot based on postprocessed results
    xdat=np.array([0,critical.e2A.to_numpy()[0],critical.e2f.to_numpy()[0]])
    ydat=np.array([0,critical.e1A.to_numpy()[0],critical.e1f.to_numpy()[0]])
    ax.plot(xdat,ydat,'-',linewidth=0.5,label=analfolder)   
    # ax.plot(ep2,ep1,'-r',linewidth=0.3,markersize=0.5,label=analfolder)
    # ax.plot(xdat,ydat,'-k',linewidth=0.6,markersize=0.5,label=analfolder)
    #------------------------------------------------------------------
    IndxLoc=critical.Ind
    #comparsion that calculated and in result file are in the same place
    # ax.plot(ep2[IndxLoc],ep1[IndxLoc],'ob',markeredgewidth=0.4,markersize=5,markerfacecolor='None') 
    ax.plot(critical.e2f,critical.e1f,'o'+color,linewidth=0.3,markersize=2)  
    ax.legend(loc='best')  
    # ax.set(title=analysismethod)
    
    
    '''
    Energy plot
    '''    
    ax_e.grid(which='major') 
    ax_e.set(xlabel='Time',ylabel='Energy')
    
    Files1=glob.glob('energy*')
    for j,out_rpt in enumerate(Files1):    
            ener_in=np.loadtxt(out_rpt,skiprows=3,usecols = (2))
            ener_out=np.loadtxt(out_rpt,skiprows=3,usecols = (1))
            time=np.loadtxt(out_rpt,skiprows=3,usecols = (0))
            if len(out_rpt)>10:
                ax_e.plot(time,ener_in,'-'+color,label='1 element')    
                
            else:   
                ax_e.plot(time[IndxLoc],ener_in[IndxLoc],'o'+color,markersize=4)
                ax_e.plot(time,ener_in,'-',label=analfolder)#'In '+' A='+str(Phi_A)+' B='+str(Phi_B))   
                ax_e.plot(time,ener_out,'--')#'Out '+' A='+str(Phi_A)+' B='+str(Phi_B))   
    ax_e.legend(loc='best')      
    os.chdir("../..")
    
    
if __name__ == "__main__":  
    # e1A,e2A,e1,e2,e1B,e2B=strain_state(0.1069,0.4508,-0.5,0.65,316,0.0025)
    
    
    dt=0.005
    # fi=[0,0.5,0,1]
    fi=[-1.3,0.5,-0.9,0.7]
    # r=[0.2,0.1,0.4,0.15]
    r=[0.2,0.1,0.3,0.15]
    fig,ax=pyp.FLC_plot()
    ep1,ep2,_=applied_strain_hist_long(dt,r,fi)
    
    
    
    ax.plot(ep2,ep1,'-r',linewidth=0.5,zorder=1) 
    # fol=r'C:\work\ML_fracture\BILINEAR\SO-BI-LT10-R02-07102021-MLrandomdataset\V_0226-A_0.90-B_0.20'
    # os.chdir(fol)
    # angle=0
    # naverage=3
    
    # fig, ax = plt.subplots(2,1,figsize=pyp.cm2inch(8, 14))
    # ax=fig.axes[0]; ax1=fig.axes[1]
    # path_energy1 = 'theta_0/energy-1EL.rpt'
    # E1=pd.read_csv(path_energy1,delimiter= '\s+',names=["t", "out", "inn"],header=0)
    # f=open('theta_'+str(angle) + "/vdisp_BILINEAR-R1.for")
    # lines=f.readlines()
    # r1=float(lines[21].split('=')[1].replace("\n",""))
    # r2=float(lines[22].split('=')[1].replace("\n",""))
    # cotfii_a=float(lines[24].split('=')[1].replace("\n","")); cotfii_b=float(lines[25].split('=')[1].replace("\n",""))
    # tswitch_step=int(r1/(r1+r2)/0.0025)+1 
    
