# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 11:24:58 2022

@author: Mihkel
"""



import os
import sys
import glob
import python_plotting as pyp
# import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy import stats
# sys.path.append('C:\work\ML_FLC\Scripts')
import Global_definitions as gd

Model='Model2'

os.chdir('C:\work\ML_FLC')
fol=glob.glob(Model+'/BI-A-fix-V_000-a_0.80-T_0.665_R1_0.22/')
os.chdir(fol[0])

fname_min=glob.glob('Localization-all-energy-25-01-2022.txt')

Ra=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
       names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1','e2','Missing'],header=0) 