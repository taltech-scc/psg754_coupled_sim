# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
# import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle

pi=np.pi
def cotf(f):
    res=pi/2.-np.arctan(f+1e-10)
    return res


fname_dam=glob.glob('Localization-min-Damage-13-05-2022-reduced*')
FLC=pd.read_csv(fname_dam[0],delimiter= '\s+',index_col=False,names=['Fol','theta','Ind','Elength','fiA','fiB','Ratot','Ra'],header=0)

# FLC 
ind_flc=FLC.Ind
r1=FLC.Ratot
phi1=FLC.fiA
e1=r1*np.sin(cotf(phi1))*ind_flc/400
e2=r1*np.cos(cotf(phi1))*ind_flc/400
fig,ax=pyp.FLC_plot()
ax.plot(e2,e1,'-k',linewidth=0.6,markersize=1,zorder=10)   

# FLC with preloading
fig3= pickle.load(open('FLC_1.pkl', 'rb'))




# fig3= pickle.load(open('FLC-base.fig.pickle', 'rb'))
# ax3=plt.gca()  

# fig3= pickle.load(open('FLC-base.fig.pickle', 'rb'))

# my_dict_final = {}  # Create an empty dictionary
# with open('pickle_file1', 'rb') as f:
#     my_dict_final.update(pickle.load(f))   # Update contents of file1 to the dictionary
# with open('pickle_file2', 'rb') as f:
#     my_dict_final.update(pickle.load(f))   # Update contents of file2 to the dictionary
# print my_dict_final