'''
Created by M.Korgesaar on 03.02.2022



plots the deformation history based on principal strain values.
Relies on two folder:
    1) python plotting i have in one drive
    2) scripts folder specific for ML fracture folder

'''


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle

# Current directory
pdir=os.getcwd()
# figx = pickle.load(open('FigureObject-energy.fig.pickle', 'rb'))


#%% This adds to the plot the strain history if needed

# gd.sim_strain_hist_plot_folder(pdir)

# ax.legend(loc='best')  
# plt.savefig(folder + '.pdf',dpi=500,transparent=False)

#%%
# Prediction curve
fig,ax=pyp.FLC_plot()
dt=0.005
# fi=[0,0.5,0,1]
fi=[-1.,0.5,-0.9,0.7,0.9]
# r=[0.2,0.1,0.4,0.15]
r=[0.2,0.1,0.3,0.15,0.6]

gd.sim_strain_hist_plot_folder(pdir)
fig=plt.gcf(); ax=fig.axes[0]
ep1,ep2,_=gd.applied_strain_hist_long(dt,r,fi)

ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=5) 



gd.sim_energy_curve_plot(pdir)













