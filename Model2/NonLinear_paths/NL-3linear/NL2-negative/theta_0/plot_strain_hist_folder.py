'''
Created by M.Korgesaar on 03.06.2022


'''
import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle
import ast
cwd=os.getcwd()




f=open('vdisp_NL.for')
lines=f.readlines()
# rarr=lines[60].split('=')[1].replace("\n","")='(/0.2,0.1,0.3,0.15,0.6/)'
r=ast.literal_eval(lines[60].split('=')[1].replace("\n","")[2:-2])
fi=ast.literal_eval(lines[61].split('=')[1].replace("\n","")[2:-2])
#------------------------------------------------------------------
#Calculate the applied strain as in fortran subroutine and PLOT
fig,ax=pyp.FLC_plot(1)
ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)  
ax.plot(ep2,ep1,'-k',linewidth=0.5,zorder=1,markevery=10) 
#------------------------------------------------------------------
# Add machine learning prediction

#Load dataset - the critical single case
Files1=glob.glob('energy.r*')
E=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,names=['Time','Eout','Ein'],header=0,na_values='-nan(ind)')    

fig2,ax2=pyp.newfigdef(2)

ax2.plot(E.Time,E.Ein,'--',linewidth=0.5)
ax2.plot(E.Time,E.Eout,'--',linewidth=0.5)
ax2.legend(loc='best')  


sys.exit()
