c Subroutine for beam with length of 50 mm
! 2021-05-21 14:48:11 
c     timing correction - when calculation is like this e1s1=radius*sin(pi/2.-atan(phi+1e-10))*tim
c     then Radius1 is not fully achieved since tim is not maximum. Therefore, to get the max R value at the
c end of the path, tim is normalized with tswitch. So if tswitch =0.4, tim/stiwch=1 at the end of the loading cycle
c2021-05-25 08:39:38
!     To check the shear behavior
      subroutine disp(U,KSTEP,KINC,TIME,NODE,NOEL,JDOF,COORDS)
      include 'aba_param.inc'
      common/crdflg/jflag1,jflag2,jflag3,jflag4
      common/epsum/s1u1,s1u2,s2u1,s2u2,s3u1,s3u2,s4u1,s4u2
      DIMENSION U(3),TIME(2),COORDS(3)
      real*8 tim,F11,F22,F12,Lx,Ly
      Lx=0.015
      Ly=0.0003
      tim = TIME(2)
C      *Node, nset=DNORM
      IF(NODE .EQ. 1000000 .AND.JDOF.EQ.1) THEN
        jin=jflag1
c        su1in=s1u1
c        su2in=s1u1
        call loading(jin,s1u1,s1u2,tim,Lx,Ly,jout,F11,F22,F12)
        U(1)=(F11-1.)*Lx
        jflag1=jout
      else if(NODE .EQ. 1000000 .AND.JDOF.EQ.2) THEN
        jin=jflag2
        call loading(jin,s2u1,s2u2,tim,Lx,Ly,jout,F11,F22,F12)
        U(1)=(F22-1.)*Ly  
        jflag2=jout        
C      *Node, nset=DSHEAR
      else if(NODE .EQ. 1000001 .AND.JDOF.EQ.1) THEN
        jin=jflag3
        call loading(jin,s3u1,s3u2,tim,Lx,Ly,jout,F11,F22,F12)
        U(1)=F12*Ly
        jflag3=jout
      else if(NODE .EQ. 1000001 .AND.JDOF.EQ.2) THEN
        jin=jflag4
        call loading(jin,s4u1,s4u2,tim,Lx,Ly,jout,F11,F22,F12)
        U(1)=F12*Lx   
        jflag4=jout
      endif	  
      return
      end

      subroutine loading(jin,su1in,su2in,tim,Lx,Ly,jout,F11,F22,F12)
      include 'aba_param.inc'
      parameter ( zero = 0.d0, half = 0.5d0, one = 1.d0,
     . Simtime=1)
      real*8 lam1,lam2,theta,pi
      real*8 e1s1,e2s1,steps,su
      real*8 ep1,ep2,phi,ri,radius,fi,phi2,e1out,e1out2
      dimension rarr(3),fiarr(3),tws(3)
c      ep1_end(3),ep2_end(3)
      integer iter1,wrinc
c      common/modelvar/pi,tim,Lx,Ly,theta,rarr,fiarr
      pi=4.D0*DATAN(1.D0)
      theta=25*pi/180
C     fi=[-1,0.5,-0.9,0.7]
C     # fi=[-0.5,0]
C     r=[0.2,0.1,0.3,0.15]  
      rarr=(/0.15,0.1,0.3/)
      fiarr=(/-0.5,0.,0.4/)
c Fill the arrays with zeros
C       ep1_end=0.
C       ep2_end=0.
      steps=100.
      iter1=int(tim/1.E-5)
      wrinc=int(1./1.E-5/steps)
      do i = 1,size(rarr)
        tws(i)=rarr(i)/sum(rarr) 
      enddo
      j=jin
      jout=jin
C       su1=su1in
C       su2=su2in
C       suo1=su1in
C       suo2=su2in
C       if ((iter.eq.1).OR.(iter.eq.2)) then   
      fi=fiarr(j)
      ri=rarr(j) 
      coeff=(tim-sum(tws(1:j-1)))/tws(j)
      flag=0
      if(tim .GE. sum(tws(1:j))) THEN
        if (tim .NE. 1.) then          
C             ep1_end(j)=ri*sin(pi/2.-atan(fi+1e-10))
C             ep2_end(j)=ri*cos(pi/2.-atan(fi+1e-10))
            su1in=su1in+ri*sin(pi/2.-atan(fi+1e-10))
            su2in=su2in+ri*cos(pi/2.-atan(fi+1e-10))
            jout=j+1 
c KEEP THE OLD J AS STATE VARIABLE SOMEHOW TO AOW PROGRESSIVE UPDATE            
            coeff=0.
            flag=1
        end if
      end if   
      su=sum(tws(1:j))      
      e1s1=ri*sin(pi/2.-atan(fi+1e-10))*coeff
      e2s1=ri*cos(pi/2.-atan(fi+1e-10))*coeff      
      ep1=e1s1+su1in
      ep2=e2s1+su2in
C	  write(6,121) jflag,j,iter1,wrinc,ri,fi,coeff,TIME(3)
      if (mod(iter1,wrinc)==0) then
      write(126,120) tim,j,flag,ri,fi,coeff,
     * ep1,ep2,su1in,su2in,su
      if (coeff.eq.0.) then
      write(126,*) '---------------------'
      endif
      endif
c write to DAT file      write(6,120) jflag,j
C       endif
  120  format (F7.4,2I5,3F7.3,5E13.4) 
      lam1=exp(ep1)
      lam2=exp(ep2)
!  123  format (4E12.3)
!       if (tim .gt. zero) then
!             write(6,123) e1out1,e1out2,ep1,ep2                
!       end if   
c ---------------------------------------------
      F11=lam1*cos(theta)**2.+lam2*sin(theta)**2.
      F22=lam1*sin(theta)**2.+lam2*cos(theta)**2.
      F12=(lam1-lam2)*sin(theta)*cos(theta)
	  
      return
      end  


 	  
C       SUBROUTINE UVARM(UVAR,DIRECT,T,TIME,DTIME,CMNAME,ORNAME,
C      1                 NUVARM,NOEL,NPT,NLAYER,NSPT,KSTEP,KINC,
C      2                 NDI,NSHR,COORD,JMAC,JMATYP,MATLAYO, LACCFLG)
C       INCLUDE 'ABA_PARAM.INC'
C       CHARACTER*80 CMNAME,ORNAME
C       DIMENSION UVAR(*),TIME(2),DIRECT(3,3),T(3,3),COORD(*),
C      $     JMAC(*),JMATYP(*) 
C       real*8 peqmax
C c     USER DEFINED DIMENSION STATEMENTS
C       CHARACTER*3 FLGRAY(15)
C       DIMENSION ARRAY(15),JARRAY(15)
C       peqmax=20.
C C     The dimensions of the variables ARRAY and JARRAY
C C     must be set equal to or greater than 15
C C https://help.3ds.com/2019/english/dssimulia_established/simacaesubrefmap/simasub-c-getvrm.htm?contextscope=all
C c Obtaining material point information in an Abaqus/Standard analysis
C       CALL GETVRM('PE',ARRAY,JARRAY,FLGRAY,JRCD,
C      $     JMAC,JMATYP,MATLAYO, LACCFLG)
C       UVAR(1)=ARRAY(7)
C       ! if (UVAR(1).GT.1.2) then
C       !       write(6,*) UVAR(1) 
C       ! endif
C       if (UVAR(1).GT.peqmax) then
C             ! write(6,*) UVAR(1)
C             CALL XIT   
C       endif
C       RETURN
C       END

      subroutine umat(stress,statev,ddsdde,sse,spd,scd,rpl,ddsddt,
     1 drplde,drpldt,stran,dstran,time,dtime,temp2,dtemp,predef,dpred,
     2 cmname,ndi,nshr,ntens,nstatv,props,nprops,coords,drot,pnewdt,
     3 celent,dfgrd0,dfgrd1,noel,npt,layer,kspt,jstep,kinc)

      include 'aba_param.inc' 
      !implicit real(a-h o-z)

      character*8 cmname
      dimension stress(ntens),statev(nstatv),ddsdde(ntens,ntens),
     1 ddsddt(ntens),drplde(ntens),stran(ntens),dstran(ntens),
     2 time(2),predef(1),dpred(1),props(nprops),coords(3),drot(3,3),
     3 dfgrd0(3,3),dfgrd1(3,3),jstep(4)
      
      dimension eelas(ntens),eplas(ntens),flow(ntens),olds(ntens),
     + oldpl(ntens),D(3,3),olde(ntens),PS(3)
      
      parameter(toler=1.d-6,newton=20)

!     Initialization
      ddsdde=0.d0
      E=props(1) ! Young's modulus
      xnu=props(2) ! Poisson's ratio
      Sy=props(3) ! Yield stress
      xn=props(4) ! Strain hardening exponent
      Ch=props(5) ! Strain hardening exponent
      eplat=props(6)
      pi=4.*ATAN(1.)
c1=0.02166327 c2= 353.494750 c3=0.995039

      c_nmc=0.1
      Ah=630.
C       c_nmc=0.294
C       C1=0.02166327
C       C2=353.494750
C       C3=0.995039   
      a1=1.3726
      b1=1.4113
      c1=0.0088      
      ezero=(Sy/Ch)**(1./xn)-eplat
      call rotsig(statev(1),drot,eelas,2,ndi,nshr)
      call rotsig(statev(ntens+1),drot,eplas,2,ndi,nshr)
      eqplas=statev(1+2*ntens)
      olds=stress
      oldpl=eplas

!     Build elastic stiffness matrix
      eg=E/(1.d0+xnu)/2.d0
      elam=(E/(1.d0-2.d0*xnu)-2.d0*eg)/3.d0
      
      do i=1,3
       do j=1,3
        ddsdde(j,i)=elam
       end do
       ddsdde(i,i)=2.d0*eg+elam
      end do
      do i=4,ntens
       ddsdde(i,i)=eg
      end do

!     Calculate predictor stress and elastic strain
      stress=stress+matmul(ddsdde,dstran)
      eelas=eelas+dstran
      
!     Calculate equivalent von Mises stress
      Smises=(stress(1)-stress(2))**2+(stress(2)-stress(3))**2
     1 +(stress(3)-stress(1))**2
      do i=4,ntens
       Smises=Smises+6.d0*stress(i)**2
      end do
      Smises=sqrt(Smises/2.d0)	 
      
!     Get yield stress from the specified hardening curve
C       Sf=Sy*(1.d0+E*eqplas/Sy)**xn
      Sf=Ch*(eqplas+ezero)**xn
      
!     Determine if active yielding
      if (Smises.gt.(1.d0+toler)*Sf) then

!     Calculate the flow direction
       Sh=(stress(1)+stress(2)+stress(3))/3.d0
       flow(1:3)=(stress(1:3)-Sh)/Smises
       flow(4:ntens)=stress(4:ntens)/Smises
       
!     Solve for Smises and deqpl using Newton's method
       deqpl=0.d0
C        Et=E*xn*(1.d0+E*eqplas/Sy)**(xn-1)
       Et=Ch*xn*(eqplas+ezero)**(xn-1)
       do kewton=1,newton
        rhs=Smises-(3.d0*eg)*deqpl-Sf
        deqpl=deqpl+rhs/((3.d0*eg)+Et)
C         Sf=Sy*(1.d0+E*(eqplas+deqpl)/Sy)**xn
        Sf=Ch*(eqplas+deqpl+ezero)**xn
C         Et=E*xn*(1.d0+E*(eqplas+deqpl)/Sy)**(xn-1)
        Et=Ch*xn*(eqplas+deqpl+ezero)**(xn-1)
        if(abs(rhs).lt.toler*Sy) exit
       end do
       if (kewton.eq.newton) write(7,*)'WARNING: plasticity loop failed'

! update stresses and strains
       stress(1:3)=flow(1:3)*Sf+Sh
       eplas(1:3)=eplas(1:3)+3.d0/2.d0*flow(1:3)*deqpl
       eelas(1:3)=eelas(1:3)-3.d0/2.d0*flow(1:3)*deqpl
       stress(4:ntens)=flow(4:ntens)*Sf
       eplas(4:ntens)=eplas(4:ntens)+3.d0*flow(4:ntens)*deqpl
       eelas(4:ntens)=eelas(4:ntens)-3.d0*flow(4:ntens)*deqpl
       eqplas=eqplas+deqpl
! Fracture indicator   
c Average triaxiality int((T*de))/eps --> state(7)
       hs=(stress(1)+stress(2)+stress(3))/3.
       q = sqrt ( 3./2. * ( 
     .     ( stress(1) - hs )**2. + 
     .     ( stress(2) - hs)**2. + 
     .     ( stress(3) - hs )**2. + 
     .      2.* (stress(4)**2.+stress(5)**2.+stress(6)**2.)))
       Tr=hs/q
       statev(4+2*ntens)=Tr
      statev(6+2*ntens)=q
      statev(7+2*ntens)=hs
c failure strain    
      theta=1.-2./pi*ACOS(-27./2.*Tr*(Tr**2.-1./3.))
      if (theta .NE. theta) then
        theta=0.99999
      endif
      f1=2./3.*cos(pi/6.*(1.-theta))
      f2=2./3.*cos(pi/6.*(3.+theta))
      f3=-2./3.*cos(pi/6.*(1.+theta))         
      gp=((0.5*((f1-f2)**a1+(f1-f3)**a1+(f2-f3)**a1))**(1./a1)+
     *    c1*(2.*Tr+f1+f3))**(-1./c_nmc)
      e_hc=b1*(1.+c1)**(1./c_nmc)*gp  
c principal strain calculation
      CALL SPRINC(eplas,PS,2,ndi,nshr)
      statev(9+2*ntens)=PS(1) 
      statev(10+2*ntens)=PS(2)
      statev(11+2*ntens)=PS(3)
C        e_mmc=(Ah*(C3+sqrt(3.)*(1.-C3)*(1./cos((1./3.)*asin((13.5)*(Tr)
C      &  **3.-(4.5)*(Tr)))-1.)/(2.-sqrt(3.)))*((1./3.)*sqrt
C      &  (3.*C1**2.+3.)*cos((1./3.)*asin((13.5)*(Tr)**3.-(4.5)*
C      &  (Tr)))+C1*((Tr)-(1./3.)*sin((1./3.)*
C      &  asin((13.5)*(Tr)**3.-(4.5)*(Tr)))))/C2)**(-1./c_nmc) 
C        write(126,121) a1, c1, b1, Tr,e_hc 
C  121  format (5E16.5) 
       statev(8+2*ntens)=e_hc    
       dD=deqpl/e_hc
       statev(5+2*ntens)=statev(5+2*ntens)+dD  
!    Calculate the plastic strain energy density
       do i=1,ntens
        spd=spd+(stress(i)+olds(i))*(eplas(i)-oldpl(i))/2.d0
C         sse=sse+(stress(i)+olds(i))*(eelas(i)-stran(i))/2.d0
C         sse=sse+(stress(i)+olds(i))*(dstran(i))/2.d0
       end do
!     Formulate the jacobian (material tangent)   
       effg=eg*Sf/Smises
       efflam=(E/(1.d0-2.d0*xnu)-2.d0*effg)/3.d0
       effhrd=3.d0*eg*Et/(3.d0*eg+Et)-3.d0*effg
       do i=1,3
        do j=1,3
         ddsdde(j,i)=efflam
        enddo
        ddsdde(i,i)=2.d0*effg+efflam
       end do
       do i=4,ntens
        ddsdde(i,i)=effg
       end do

       do i=1,ntens
        do j=1,ntens
         ddsdde(j,i)=ddsdde(j,i)+effhrd*flow(j)*flow(i)
        end do
       end do
      endif
C  CHANGE IN SPECIFIC ELASTIC STRAIN ENERGY
       do i=1,ntens
         olde(i)=statev(i)
         sse=sse+(stress(i)+olds(i))*(eelas(i)-olde(i))/2.d0
       end do      
C       sse = sse + DEE     
!    Store strains in state variable array
      statev(1:ntens)=eelas
      statev((ntens+1):2*ntens)=eplas
      statev(1+2*ntens)=eqplas
      statev(2+2*ntens)=sse
      statev(3+2*ntens)=spd
C       if (statev(5+2*ntens).GT.1.5) then
C             ! write(6,*) UVAR(1)
C             CALL XIT   
C       endif
      return
      end

      subroutine uexternaldb(lop,lrestart,time,dtime,kstep,kinc)
      include 'aba_param.inc'      
C     Common block used in uwave.f for storage of intermediate configuration
      real*8 s1u1,s1u2,s2u1,s2u2,s3u1,s3u2,s4u1,s4u2 
      common/crdflg/jflag1,jflag2,jflag3,jflag4  
      common/epsum/s1u1,s1u2,s2u1,s2u2,s3u1,s3u2,s4u1,s4u2      
      if(lop.eq.0) then !StartAnalysis
C       Initialize read and print flags and common block used in wave.f 
        jflag1=1
        jflag2=1
        jflag3=1
        jflag4=1
        OPEN(UNIT=126,ACCESS='APPEND',SHARED, 
     * FILE='C:\work\jchecks.txt')
       write(126,121) 'Tim','j','flag','ri',
     * 'fi','coeff','ep1','ep2','su(ep1_end)','su(ep2_end)','su'
 121  format (A7,2A5,3A7,5A13)
  
C       SUBROUTINE UEXTERNALDB(LOP,LRESTART,TIME,DTIME,KSTEP,KINC)
      else if (lOp .eq. 3) then
      close(126)  
      end if 
      return
      end
C       INCLUDE 'ABA_PARAM.INC'
C C
C       DIMENSION TIME(2)     
C       if (LOP .eq. 0) then
      
C       OPEN(UNIT=126,ACCESS='APPEND',SHARED, FILE='C:\work\MS-checks.txt')
C c      OPEN(UNIT=126,ACCESS='APPEND',SHARED, FILE='C:\work\checks.txt')   
C c      write(126,121) 'Time','TF1','TF2','TF3','TF4','TF5','TF6','ca1','ca2',
C c     & 'ca3','ca4','ca5','ca6'         
C       else if (LOP .eq. 6) then
C c      close(125)   
C       close(126)    

C       end if 
C       RETURN
C       END


