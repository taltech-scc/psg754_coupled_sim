'''
Created by M.Korgesaar on 03.06.2022

Compares the complex path simulation and ML prediction results

variables:
    case - number from the Muhammed file which one to compare
'''
import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle
import ast
cwd=os.getcwd()

#Pick the case you want to plot from the loaded text file
case=5



#--------------END of user input--------

#Import all ML predictions
MLpredict=True

if MLpredict==True:
    # ML=pd.read_csv(glob.glob('MLprediction_indexes.txt')[0],delimiter= '\s+',index_col=False,
    #     names=['Fol','Sim','Xtree','Conv'],header=0) 
    ML=pd.read_csv(glob.glob('MLprediction_indexes3.txt')[0],delimiter= '\s+',index_col=False,
        names=['Fol','P1','Sim','P2','P3','P4','P5'],header=0) 


Folder=ML.Fol[case][:-22]

# sys.exit()
# Folders=glob.glob('NL*')
# Folder='NL3_CF0.70_-0.5_-0.1'
# Folder='NL3_CF0.70_-0.5_-0.3' 
# Folder='NL3_CF0.70_-0.5_-0.7' 
# Folder='NL3_CF0.70_-0.5_0.1' 
# Folder='NL3_CF0.70_-0.5_0.3' 
# Folder='NL5_0.9_-0.9_0.1_-0.5_0' 
# Folder='NL5_-0.95_0.5_0.7_-0.7_0.2' 
# Folder='NL3_-0.5,0,0.4_short' 
# Folder='C16' 
# Folder='NL3-c14--0.5,0,0.4' 
if MLpredict==True:
    MLprediction=ML.loc[(ML.Fol==f'{Folder}_Res_postpocessed.xlsx')]


# print(Folders)
os.chdir(Folder)



#INput
all=False #either plots all or only the critical
# r=[0.1,0.2,0.6]
# fi=[0.7,-0.5,-0.1]
f=open('vdisp_NL.for')
lines=f.readlines()
# rarr=lines[60].split('=')[1].replace("\n","")='(/0.2,0.1,0.3,0.15,0.6/)'
r=ast.literal_eval(lines[60].split('=')[1].replace("\n","")[2:-2])
fi=ast.literal_eval(lines[61].split('=')[1].replace("\n","")[2:-2])
#------------------------------------------------------------------
#Calculate the applied strain as in fortran subroutine and PLOT
fig,ax=pyp.FLC_plot(1)
ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)  
ax.plot(ep2,ep1,'-k',linewidth=0.5,zorder=1,markevery=10) 
#------------------------------------------------------------------
# Add machine learning prediction
# if MLpredict==True:
#     ax.plot(ep2[MLprediction.Xtree],ep1[MLprediction.Xtree],'ob',linewidth=0.5,markerfacecolor="None",markersize=4,zorder=1) 
#     ax.plot(ep2[MLprediction.Conv],ep1[MLprediction.Conv],'sg',linewidth=0.5,markerfacecolor="None",markersize=3,zorder=5) 
# if MLpredict==True:
    # ax.plot(ep2[MLprediction.Sim],ep1[MLprediction.Sim],'ob',linewidth=0.5,markerfacecolor="None",markersize=4,zorder=1,label='Sim') 
    # ax.plot(ep2[MLprediction.P1],ep1[MLprediction.P1],'sg',linewidth=0.5,markerfacecolor="None",markersize=3,zorder=5) 

# for col in MLprediction.iloc[:,2:]:
for col in ML.loc[:,ML.columns.str.startswith('P5')]:   
    # print ( MLprediction[col])
    ax.plot(ep2[MLprediction[col]],ep1[MLprediction[col]],'s',linewidth=0.5,markerfacecolor="None",markersize=4,zorder=1,label=col) 





#Load dataset - the critical single case
fname_min=glob.glob('Localization-min-Damage*.txt')
R=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
    names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0)  


Critical_case=R.theta
if all==False:
    Fnames=glob.glob('theta_'+str(Critical_case[0]))
else:   
    Rall=pd.read_csv(glob.glob('Localization-all*.txt')[0],delimiter= '\s+',index_col=False,
        names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0) 
    Fnames=glob.glob('theta_*')


for i,fol in enumerate(Fnames): 
    #% Plot the bilinear FLC
    # fig = pickle.load(open('Figure-energy-BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15.fig.pickle', 'rb'))
    #%% This cell plots the results of the current folder simulations
    os.chdir(fol)
    pdir=os.getcwd()
    ##plots the principal strains in folder pdir (simulation result)
    gd.sim_strain_hist_plot_folder(pdir)
    
    
    #%% This cell plots the actual applied strain 
    # ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)
    fig=plt.gcf(); ax=fig.axes[0]
    # ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=20) 
    ax.plot(R.e2f,R.e1f,'--.g',linewidth=0.5,zorder=1,markevery=20) 
    if all==False:
        ax.plot(R.e2f,R.e1f,'xr',markersize=3,zorder=10, label='Sim') 
        print('critical index=',R.Ind[0])
    else:
        ax.plot(Rall.e2f,Rall.e1f,'xr',markersize=3,zorder=1)
    axe=gd.sim_energy_curve_plot(pdir)
    os.chdir("..")
    # pyp.newfigdef(4)

ax.legend(loc='best')
ax.set(title=Folder)
axe.plot([R.Ind/400,R.Ind/400],[0,2e+6],'--',label='Index:'+str(R.Ind[0]))
axe.legend(loc='best')

ax.set(xlim=[-0.5, 0.5],ylim=[0,0.8])
plt.grid(False) 
fig.set_size_inches(pyp.cm2inch(8, 8))
# fig.set_size_inches(pyp.cm2inch(7, 7))
# os.chdir("..")
# fig.savefig(Folder+'.pdf',transparent=False)