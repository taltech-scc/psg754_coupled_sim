'''
Created by M.Korgesaar on 13.11.2020

plots the deformation history based on principal strain values.
Relies on two folder:
    1) python plotting i have in one drive
    2) scripts folder specific for ML fracture folder

'''
import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle
import ast
cwd=os.getcwd()

#INput
all=True #either plots all or only the critical
# r=[0.1,0.2,0.6]
# fi=[0.7,-0.5,-0.1]
f=open('vdisp_NL.for')
lines=f.readlines()
# rarr=lines[60].split('=')[1].replace("\n","")='(/0.2,0.1,0.3,0.15,0.6/)'
r=ast.literal_eval(lines[60].split('=')[1].replace("\n","")[2:-2])
fi=ast.literal_eval(lines[61].split('=')[1].replace("\n","")[2:-2])
#------------------------------------------------------------------
#Calculate the applied strain as in fortran subroutine and PLOT
fig,ax=pyp.FLC_plot(1)
ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)  
ax.plot(ep2,ep1,'.-k',linewidth=0.5,zorder=1,markevery=10) 
#------------------------------------------------------------------
#Load dataset - the critical single case
fname_min=glob.glob('Localization-min-Damage*.txt')
R=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
    names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0)  
Rall=pd.read_csv(glob.glob('Localization-all*.txt')[0],delimiter= '\s+',index_col=False,
    names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0) 

Critical_case=R.theta
if all==False:
    Fnames=glob.glob('theta_'+str(Critical_case[0]))
else:   
    Fnames=glob.glob('theta_*')


for i,fol in enumerate(Fnames): 
    #% Plot the bilinear FLC
    # fig = pickle.load(open('Figure-energy-BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15.fig.pickle', 'rb'))
    #%% This cell plots the results of the current folder simulations
    os.chdir(fol)
    pdir=os.getcwd()
    ##plots the principal strains in folder pdir (simulation result)
    gd.sim_strain_hist_plot_folder(pdir)
    
    
    #%% This cell plots the actual applied strain 
    # ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)
    fig=plt.gcf(); ax=fig.axes[0]
    # ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=20) 
    ax.plot(R.e2f,R.e1f,'--.g',linewidth=0.5,zorder=1,markevery=20) 
    if all==False:
        ax.plot(R.e2f,R.e1f,'xr',markersize=3,zorder=1) 
    else:
        ax.plot(Rall.e2f,Rall.e1f,'xr',markersize=3,zorder=1)
    axe=gd.sim_energy_curve_plot(pdir)
    os.chdir("..")
    # pyp.newfigdef(4)

       
axe.plot([R.Ind/400,R.Ind/400],[0,2e+6],'--',label='Index:'+str(R.Ind[0]))
axe.legend(loc='best')

