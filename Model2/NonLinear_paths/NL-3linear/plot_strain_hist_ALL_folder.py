# -*- coding: utf-8 -*-
"""
Created on Wed Jun  1 12:41:14 2022

@author: Mihkel

Input: Indexes.xlsx simulation, ML results

"""
import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pathlib import PurePath
mypath=os.getcwd().split('ML_FLC')
sys.path.append(f'{mypath[0]}/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle
import ast


# print('__file__:    ', __file__)
os.chdir(os.path.dirname(__file__))
cwd=os.getcwd()
#Import all ML predictions
# ML=pd.read_csv(glob.glob('Test.xlsx')[0],delimiter= '\s+',index_col=False,
#     names=['Fol','Sim','Xtree','Conv'],header=0,encoding = "ISO-8859-1") 
ML=pd.read_excel(glob.glob('Indexes.xlsx')[0],index_col=False,
    names=['Fol','Sim','Bi','Tri'],header=0) 
all=False #either plots all or only the critical

# Consider only folders for which ML predictions are available
Folders=ML.Fol
#select a subset of folders ------------------------
# Folset=ML.loc[(ML.Fol.str.contains('NL3_-0.5,0,0.4_short'))].Fol
# Folset=ML.iloc[-3:].Fol
Folset=ML.iloc[:].Fol
#------------------------------------------------------
# Folders=next(os.walk('.'))[1]   #lists only folders
fig,ax=pyp.FLC_plot(1)
col=plt.cm.viridis(np.linspace(0,1,len(Folset)))





# iterate through folders
for i,Folder in enumerate(Folset): 
    print(Folder)
    MLprediction=ML.loc[(ML.Fol==Folder)]
    os.chdir(Folder)
    f=open('vdisp_NL.for')
    lines=f.readlines()
    r=ast.literal_eval(lines[60].split('=')[1].replace("\n","")[2:-2])
    fi=ast.literal_eval(lines[61].split('=')[1].replace("\n","")[2:-2])
    #------------------------------------------------------------------
    #Calculate the applied strain as in fortran subroutine and PLOT
    
    ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)  
    ax.plot(ep2,ep1,'-',color=col[i],linewidth=0.5,zorder=1,markevery=10,label=Folder) 
    #------------------------------------------------------------------
    # Add machine learning prediction
    ax.plot(ep2[int(MLprediction.Bi)],ep1[int(MLprediction.Bi)],'ob',linewidth=0.5,markerfacecolor="None",markersize=4,zorder=1) 
    ax.plot(ep2[int(MLprediction.Tri)],ep1[int(MLprediction.Tri)],'sg',linewidth=0.5,markerfacecolor="None",markersize=3,zorder=5) 
    
    
    #Load dataset - the critical single case from each analysis folder
    #Contains information of damage for a most critical case
    fname_min=glob.glob('Localization-min-Damage*.txt')
    R=pd.read_csv(fname_min[0],delimiter= '\s+',index_col=False,
        names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0)  
    Rall=pd.read_csv(glob.glob('Localization-all*.txt')[0],delimiter= '\s+',index_col=False,
        names=['Fol','theta','Ind','Elength','e1f','e2f','Missing'],header=0) 
    
    Critical_case=R.theta
    if all==False:
        Fnames=glob.glob(f'theta_{Critical_case[0]}')
    else:   
        Fnames=glob.glob('theta_*')
    for i,fol in enumerate(Fnames): 
        #% Plot the bilinear FLC
        # fig = pickle.load(open('Figure-energy-BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15.fig.pickle', 'rb'))
        #%% This cell plots the results of the current folder simulations
        os.chdir(fol)
        pdir=os.getcwd()
        ##plots the principal strains in folder pdir (simulation result)
        # gd.sim_strain_hist_plot_folder(pdir)
        
        
        #%% This cell plots the actual applied strain 
        fig=plt.gcf(); ax=fig.axes[0]
        if all==False:
            ax.plot(R.e2f,R.e1f,'xr',markersize=3,zorder=10) 
            print('critical index=',R.Ind[0])
        else:
            ax.plot(Rall.e2f,Rall.e1f,'xr',markersize=3,zorder=1)
        os.chdir("..")
    os.chdir(cwd)

ax.legend(loc=(1.1,0),prop={'size': 5})
ax.set(xlim=[-0.5, 0.5],ylim=[0,0.8])
plt.grid(False) 
plt.show()
fig.set_size_inches(pyp.cm2inch(12, 8))
fig.tight_layout()  
# fig.set_size_inches(pyp.cm2inch(7, 7))
fig.savefig(Folder+'.pdf',transparent=False)