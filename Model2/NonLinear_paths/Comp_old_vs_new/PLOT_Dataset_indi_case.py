# -*- coding: utf-8 -*-
"""
Created on 13.09.2021 15:15:44

@author: Mihkel

Plot one simulation result based on the sequence number V_**** and the principal
stress direction wrt imperfection band (theta angle). All are bilinear simulations.
The sequence number interpretation: V_****-A_xx-B_yy
    **** - sequence number
    xx - cotfii at the first proportional loading (step A)
    yy - cotfii at the second proportional loading step (step B)

"""
# fff


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
import ast
# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division
pi=4*np.arctan(1)

foldername='New-v3'

Files1=glob.glob(foldername+'/Principal_strains.r*')

Cri_strains=pd.read_csv(Files1[0],delimiter= '\s+',index_col=False,
            names=['Time','e1out','e2out','e3out','e1','e2','e3'],header=0,na_values='-nan(ind)')
                   
fig,ax=pyp.FLC_plot()
f=open(foldername+'/vdisp_NL.for')
lines=f.readlines()
# rarr=lines[60].split('=')[1].replace("\n","")='(/0.2,0.1,0.3,0.15,0.6/)'
r=ast.literal_eval(lines[60].split('=')[1].replace("\n","")[2:-2])
fi=ast.literal_eval(lines[61].split('=')[1].replace("\n","")[2:-2])


ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)
ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=5) 


    #%% This cell plots the results of the current folder simulations
os.chdir(foldername)
pdir=os.getcwd()
gd.sim_strain_hist_plot_folder(pdir)