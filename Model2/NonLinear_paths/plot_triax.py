# -*- coding: utf-8 -*-
"""
Created on Wed Feb  2 15:51:19 2022

@author: Mihkel
"""

'''
Created by M.Korgesaar on 13.11.2020

plots the deformation history in MK analysis. 



'''


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# print (__name__)
# import time
np.seterr(divide='ignore', invalid='ignore')            #ignores the zero division


# Fnames=glob.glob('V_*')
Fnames=glob.glob('new*')
# Fnames=glob.glob('V_009-a_-0.72-T_0.181')
# Fnames=[glob.glob('a_*')]




location='out'


fig=plt.gcf(); ax=fig.axes
if ax==[]:
    plt.close(fig)
    fig, ax = plt.subplots(figsize=(12, 6))
    print('create from scratch')
else:
    ax=ax[0]
    print('plot on exisiting')   




for i,it in enumerate(Fnames):
    # print(it)
    os.chdir(it)
    Files1=glob.glob('Triax-eps-hist.r*')
    for j,out_rpt in enumerate(Files1):
        eps_in=np.loadtxt(out_rpt,skiprows=3,usecols = (1)) #equivalent to> F=np.loadtxt(FDcurves[i],skiprows=4,usecols = (1));
        eps_out=np.loadtxt(out_rpt,skiprows=3,usecols = (2))
        T_in=np.loadtxt(out_rpt,skiprows=3,usecols = (3))
        T_out=np.loadtxt(out_rpt,skiprows=3,usecols = (4))
        if location=='in':  
            T=T_in; eps=eps_in
        else:
            T=T_out; eps=eps_out
        T_diff=T[:-1]-T[1:]
        e_diff=eps[:-1]-eps[1:]
        T_e_ratio=T_diff/e_diff  
        try:
            ind=np.min(np.where((abs(T_e_ratio)>1)*(eps[:-1]>0.02)==True))
        except:
            print(it + ': Localization not detected')
            ind=len(T_e_ratio)  #in case there is no localization plots the entire deformation history
        ax.plot(T[:ind+1],eps[:ind+1],'-',label=Fnames[i])   
        ax.plot(T[ind],eps[ind],'o')   
        # ax.plot(T,eps,'.',label=out_rpt)   
    os.chdir("..")   
        
plt.show() 
  