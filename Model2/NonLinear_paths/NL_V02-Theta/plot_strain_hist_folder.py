'''
Created by M.Korgesaar on 13.11.2020

plots the deformation history based on principal strain values.
Relies on two folder:
    1) python plotting i have in one drive
    2) scripts folder specific for ML fracture folder

'''
import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle
cwd=os.getcwd()

# Define the folder where you want to plot the results

r=[0.05,0.1,0.1,0.15,0.1,0.4]
fi=[0.,1,-0.7,-0.9,0.3,-0.5]
fig,ax=pyp.FLC_plot()
ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)
ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=5) 
Fnames=glob.glob('theta_*')
for i,fol in enumerate(Fnames):

    
    #% Plot the bilinear FLC
    # fig = pickle.load(open('Figure-energy-BI-A-fix-V_000-a_-0.65-T_0.230_R1-0.15.fig.pickle', 'rb'))
    
    
    
    #%% This cell plots the results of the current folder simulations
    os.chdir(fol)
    pdir=os.getcwd()
    gd.sim_strain_hist_plot_folder(pdir)
    
    
    #%% This cell plots the actual applied strain
    
    # ep1,ep2=gd.applied_strain_hist(0.15,0.24200000000000002,0.7,-0.14999999999999925,0.0025)
    ep1,ep2,_=gd.applied_strain_hist_long(0.0025,r,fi)
    fig=plt.gcf(); ax=fig.axes[0]
    ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=20) 
    

    
    
    
    # #%%
    # # Prediction curve
    # dt=0.005
    # # fi=[0,0.5,0,1]
    # fi=[-1.,0.5,-0.9,0.7]
    # # r=[0.2,0.1,0.4,0.15]
    # r=[0.2,0.1,0.3,0.15]
    # fig=plt.gcf(); ax=fig.axes[0]
    # ep1,ep2=gd.applied_strain_hist_long(dt,r,fi)
    
    # ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=5) 
    
    
    axe=gd.sim_energy_curve_plot(pdir)
    indx=261
    axe.plot([indx/400,indx/400],[0,2e+6],'--')
    os.chdir("..")
    # pyp.newfigdef(4)