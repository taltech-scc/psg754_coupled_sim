# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *


# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='MASTER.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   

# # xy1 = session.XYDataFromHistory(name='PEEQ E: 155 IP: 1 ELSET OUTSIDE-1', 
    # # odb=odb, 
    # # outputVariableName='Equivalent plastic strain: PEEQ at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # # steps=('Step-1', ), __linkedVpName__='Viewport: 1')



eps_out = session.XYDataFromHistory(name='eps-out', odb=odb, 
    outputVariableName='PEEQ: SDV_PEEQ at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )

eps_in = session.XYDataFromHistory(name='eps-in', odb=odb, 
    outputVariableName='PEEQ: SDV_PEEQ at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )




# session.writeXYReport(fileName='Damage.rpt', appendMode=OFF, xyData=(Din,Dout))
# session.writeXYReport(fileName='energy.rpt', appendMode=OFF, xyData=(SEout,SEin))
# session.writeXYReport(fileName='Principal_strains.rpt', appendMode=OFF, xyData=(ep1,ep2,ep3,ep1in,ep2in,ep3in))
session.writeXYReport(fileName='EPS.rpt', appendMode=OFF, xyData=(eps_out,eps_in))






# # xy1 = session.XYDataFromHistory(name='PEEQ E: 155 IP: 1 ELSET OUTSIDE-1', 
    # # odb=odb, 
    # # outputVariableName='Equivalent plastic strain: PEEQ at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# # c1 = session.Curve(xyData=xy1)
# xy2 = session.XYDataFromHistory(name='PEP1 E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Principal plastic strains: PEP1 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c2 = session.Curve(xyData=xy2)
# xy3 = session.XYDataFromHistory(name='PEP2 E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Principal plastic strains: PEP2 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c3 = session.Curve(xyData=xy3)
# xy4 = session.XYDataFromHistory(name='PEP3 E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Principal plastic strains: PEP3 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c4 = session.Curve(xyData=xy4)
# xy5 = session.XYDataFromHistory(name='TRIAX E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Stress triaxiality: TRIAX at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c5 = session.Curve(xyData=xy5)
# xyp = session.xyPlots['XYPlot-1']
# chartName = xyp.charts.keys()[0]
# chart = xyp.charts[chartName]
# chart.setValues(curvesToPlot=(c0, c1, c2, c3, c4, c5, ), )
# session.charts[chartName].autoColor(lines=True, symbols=True)
# session.viewports['Viewport: 1'].setValues(displayedObject=xyp)