'''
Created by M.Korgesaar on 13.11.2020

plots the deformation history based on principal strain values.
Relies on two folder:
    1) python plotting i have in one drive
    2) scripts folder specific for ML fracture folder

'''


import os
import sys
import glob
import python_plotting as pyp
import py_general as pyg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
sys.path.append('c:/work/ML_FLC/Scripts/')
import Global_definitions as gd
import pickle



pdir=os.getcwd()
gd.sim_strain_hist_plot_folder(pdir)
ep1,ep2=gd.applied_strain_hist(0.15,0.24200000000000002,0.7,-0.14999999999999925,0.0025)
fig=plt.gcf(); ax=fig.axes[0]
ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=5) 



# # ax.legend(loc='best')  
# # plt.savefig(folder + '.pdf',dpi=500,transparent=False)

# #%%
# # Prediction curve
# dt=0.005
# # fi=[0,0.5,0,1]
# fi=[-1.,0.5,-0.9,0.7]
# # r=[0.2,0.1,0.4,0.15]
# r=[0.2,0.1,0.3,0.15]
# fig=plt.gcf(); ax=fig.axes[0]
# ep1,ep2=gd.applied_strain_hist_long(dt,r,fi)

# ax.plot(ep2,ep1,'--.g',linewidth=0.5,zorder=1,markevery=5) 


gd.sim_energy_curve_plot(pdir)


# fig,ax=pyp.FLC_plot()
# # os.chdir(pdir)
# Files1=glob.glob('Principal_strains*')
# for j,out_rpt in enumerate(Files1):   
#     namehead=["Time", "e1out", "e2out",'e3out',"e1in", "e2in",'e3in']
#     e=pd.read_csv(Files1[0],delimiter= '\s+',names=namehead,header=0,na_values='-nan(ind)')
#     ax.plot(e.e3in,e.e1in,'-b')   
#     ax.plot(e.e2in,e.e1in,'-r',linewidth=0.5,markersize=1)        