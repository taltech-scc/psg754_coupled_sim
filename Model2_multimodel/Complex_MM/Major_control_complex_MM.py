import os,glob
import sys
from shutil import copy2
# import python_func_list
import subprocess
import fileinput
import io
from contextlib import closing
import numpy as np
import time
import pandas as pd
import sys
import Py_funcs as pyg
from timeit import default_timer as timer
import time
pi=4*np.arctan(1)

cwd=os.getcwd()
folder=os.path.basename(os.path.normpath(cwd))
start_angle=0
end_angle=90
dalfa=10

#Stress states
phiA=np.arange(-1,1.05,0.05) #41 cases
phiB=np.arange(-1,1.05,0.05) #41 cases
phiC=np.random.choice(np.arange(-1,1.05,0.05),5,replace=False) # phiC arbitrary 5 cases
#Total cases=41*41*5=8405

fortran_name='vdisp_NL_MM.for'
Critical_pathA=np.loadtxt("Critical_pathA_damage.txt" ,skiprows=1)
Bilinearres="Localization-min-Damage-02-05-2022-bilinear.txt"


BIDS=pd.read_csv(glob.glob(Bilinearres)[0],delimiter= '\s+',index_col=False,
     names=['Fol','theta','Ind','Elength','fiA','fiB','Ra','Rb','Rbtot','e1A','e2A','e1f','e2f','e1','e2','Missing'],header=0)
  



RS=open('Run-statistics.txt',"a")
RS.write("{:40}\n".format('Case'))

def con(f):
    res=pi/2.-np.arctan(f+1e-10)
    return res

def deleteFile():
   for f in glob.glob("*/*.inp", recursive=True):
      os.remove(f)
   for f in glob.glob("*/*.txt", recursive=True):
      os.remove(f)
   for f in glob.glob("*/*.bat", recursive=True):
      os.remove(f)
   for f in glob.glob("*/*.py"):
      os.remove(f)
   # for f in glob.glob("*/*.for"):
   #    os.remove(f)

def change_radius(fiA,fiB,fiC,for_name):

    if fiA<-0.9 or fiB<-0.9:
        Ra=np.random.randint(10,50)/100
        Rb=0.7-Ra
    else:
        R=BIDS.loc[(BIDS.fiA==fiA) & (BIDS.fiB==fiB) & (BIDS.Ind < 399)]   #this subset removes particular cases   
        if R.empty:
            Ra=np.random.randint(10,50)/100
            Rb=0.7-Ra    
        else:        
            Ra=np.random.randint(int(min(R.Ra)*1000), int(max(R.Ra)*1000))/1000
            Rb=np.random.randint(int(min(R.Rb)*1000), int(0.8*max(R.Rb)*1000))/1000
    e1=Ra*np.sin(con(fiA))+Rb*np.sin(con(fiB))
    e2=Ra*np.cos(con(fiA))+Rb*np.cos(con(fiB))
    #Final length calculated based on the fact that FLC remains in circle with radius 1.5
    Rc=1.5-np.sqrt(e1**2+e2**2)
    i=0
    with closing(fileinput.FileInput(for_name,inplace = 1)) as file:
        for line in file:
            i+=1
            if i==83:
                print(f'      rarr=(/{Ra},{Rb},{Rc}/)')
            elif i==84:
                print(f'      fiarr=(/{fiA},{fiB},{fiC}/)')
            else:
                print(line,end='')
    return (Ra,Rb,Rc)

def change_fortran(for_name,*angle):
    i=0
    with closing(fileinput.FileInput(for_name,inplace = 1)) as file:
        for line in file:
            i+=1
            if i==21 and len(angle)!=0:
                anglerange=[x for x in range(angle[0],angle[0]+10)]
                print(f'      th_ar=(/{anglerange}/)*pi/180'.replace("[","").replace("]","")),
                # print(f'      th_ar=(/{anglerange}/)*pi/180'),
                # print('File=\'88'+folder+ '.inp\'')
            else:
                print(line,end='')

IDXG=0
for ind_fia,phia in enumerate(phiA):
    for ind_fib,phib in enumerate(phiB):
        for ind_fic,phic in enumerate(phiC):
            IDXG+=1
            fol1=f'V_{IDXG:04d}-f1_{phia:04.2f}_f2_{phib:04.2f}_f3_{phic:04.2f}'
            os.makedirs(fol1)
            copy2(fortran_name, fol1)
            copy2('MASTER.inp', fol1); copy2('Extract_TRIAX_EP1.py', fol1); copy2('Equation.inp', fol1);
            # copy2('Master-single.inp', fol1); copy2('Extract_TRIAX_EP1-1EL.py', fol1);

# Change the radius
            
            os.chdir(fol1)
            rlist=change_radius(phia,phib,phic,fortran_name)   
            for idx1,angle in enumerate(np.arange(start_angle,end_angle,dalfa)):
                fol2=f'alfa_{angle}_{angle+dalfa}'
                os.makedirs(fol2)
                # Copy files to run directory
                copy2(fortran_name, fol2)#; copy2(fortran_name2, fol2); 
                copy2('MASTER.inp', fol2); copy2('Extract_TRIAX_EP1.py', fol2); copy2('Equation.inp', fol2); 
                os.chdir(fol2)
                change_fortran(fortran_name,angle)  
        #------------version 6.14
                # pyg.change_inp_write_solve(1,version=14,pc='mk',analysis='static',fname='MASTER.inp',fortran=fortran_name,theta=angle)
                # n=open('solveModel.bat',"a")       
                # if angle==0:
                #     n.write('call abq6141 cae noGUI=Extract_TRIAX_EP1-1EL\n')
                # n.write('call abq6141 cae noGUI=Extract_TRIAX_EP1\n')
                # n.write('for %%i in (*.*) do if not "%%~xi"==".rpt" if not "%%~xi"==".for" if not "%%~xi"==".bat" del /q "%%i"')
        #------------------------------end of version change
                pyg.change_inp_write_solve(4,version=21,pc='wshp',analysis='static',fname='MASTER.inp',singleEl='no',fortran=fortran_name,theta=angle)
                n=open('solveModel.bat',"a")
                n.write('call abq2021hf6 cae noGUI=Extract_TRIAX_EP1\n')
                n.write('for %%i in (*.*) do if not "%%~xi"==".rpt" if not "%%~xi"==".for" if not "%%~xi"==".inp" if not "%%~xi"==".bat" del /q "%%i"\n')
                n.write('if exist Principal_strains9.rpt (\n')       
                n.write(f'   echo runsfinalized > finalized_calls_{angle}.txt)')
        #------------------------------end of version change
                n.close()         
        #Run the simulation
                lsproce=subprocess.Popen(['solveModel.bat'],shell=True) 
                nr_of_sims=5
                if idx1==0:         #to avoid modulo division 
                    os.chdir("..")
                    continue
                elif ((idx1+1) % nr_of_sims)==0:
                    print(f'idx1={idx1}, angle {angle}')
                    start = timer()
                    os.chdir("..")
                    Fnames=glob.glob('*/finalized_*')
                    lsproce.communicate()
                    while len(Fnames)<nr_of_sims:
                        time.sleep(1)
                        end = timer()
                        print('--------------Sims running--------------')
                        print('Finished:'+str(len(Fnames)))
                        print('Total time: '+str(end-start))
                        Fnames=glob.glob('*/finalized_*')               
                        if (end-start)>1000:
                            RS.write("{:40}\n".format(fol1)) 
                            RS.flush() 
                            break
                        # time.sleep(4)
                    if len(Fnames)==nr_of_sims: #if break because of overtime dont delete files
                        deleteFile()
                    else:
                        continue
                else:
                    os.chdir("..")
            os.chdir("..")  

RS.close()  