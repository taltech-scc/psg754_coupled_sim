# General definations
#-------------------------------------------------------
from abaqus import *
import testUtils
import visualization
testUtils.setBackwardCompatibility()
import displayGroupOdbToolset as dgo 
from abaqusConstants import *
from abaqus import *
from abaqusConstants import *


# Create viewport
#-------------------------------------------------------
myViewport = session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=100,height=100)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()

step=100

File='MASTER.odb'
o1 = session.openOdb(name=File)
odb = session.odbs[File]
import visualization
session.viewports['Viewport: 1'].setValues(displayedObject=o1)   


ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
    outputVariableName='ep3: SDV_ep3 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )
ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
    outputVariableName='ep2: SDV_ep2 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )
ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
    outputVariableName='ep1: SDV_ep1 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )   

ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
    outputVariableName='ep3: SDV_ep3 at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )
ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
    outputVariableName='ep2: SDV_ep2 at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )
ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
    outputVariableName='ep1: SDV_ep1 at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )   


eps_out = session.XYDataFromHistory(name='eps-out', odb=odb, 
    outputVariableName='PEEQ: SDV_PEEQ at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )

eps_in = session.XYDataFromHistory(name='eps-in', odb=odb, 
    outputVariableName='PEEQ: SDV_PEEQ at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )


Tout = session.XYDataFromHistory(name='Tout', odb=odb, 
    outputVariableName='Tr: SDV_Tr at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )

Tin = session.XYDataFromHistory(name='Tin', odb=odb, 
    outputVariableName='Tr: SDV_Tr at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )





# abaqus output variables
# ep3 = session.XYDataFromHistory(name='ep3-out', odb=odb, 
#     outputVariableName='Principal plastic strains: PEP1 at Element 155 Int Point 1 in ELSET OUTSIDE', 
#     steps=('Step-1', ), )
# ep2 = session.XYDataFromHistory(name='ep2-out', odb=odb, 
#     outputVariableName='Principal plastic strains: PEP2 at Element 155 Int Point 1 in ELSET OUTSIDE', 
#     steps=('Step-1', ), )
# ep1 = session.XYDataFromHistory(name='ep1-out', odb=odb, 
#     outputVariableName='Principal plastic strains: PEP3 at Element 155 Int Point 1 in ELSET OUTSIDE', 
#     steps=('Step-1', ), )   

# ep3in = session.XYDataFromHistory(name='ep3-in', odb=odb, 
#     outputVariableName='Principal plastic strains: PEP1 at Element 146 Int Point 1 in ELSET INSIDE', 
#     steps=('Step-1', ), )
# ep2in = session.XYDataFromHistory(name='ep2-in', odb=odb, 
#     outputVariableName='Principal plastic strains: PEP2 at Element 146 Int Point 1 in ELSET INSIDE', 
#     steps=('Step-1', ), )
# ep1in = session.XYDataFromHistory(name='ep1-in', odb=odb, 
#     outputVariableName='Principal plastic strains: PEP3 at Element 146 Int Point 1 in ELSET INSIDE', 
#     steps=('Step-1', ), )    

SEout= session.XYDataFromHistory(name='SENER-out', odb=odb, 
    outputVariableName='Strain energy density: SENER at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ), )

SEin= session.XYDataFromHistory(name='SENER-in', odb=odb, 
    outputVariableName='Strain energy density: SENER at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ), )
# x0 = session.xyDataObjects['E_int']
# x1 = session.xyDataObjects['E_kin']
# x2 = session.xyDataObjects['F']
# x3 = session.xyDataObjects['U']
Din = session.XYDataFromHistory(name='Dinn',odb=odb, 
    outputVariableName='D_mmc: SDV_D_mmc at Element 146 Int Point 1 in ELSET INSIDE', 
    steps=('Step-1', ),)
Dout = session.XYDataFromHistory(name='Dout',odb=odb, 
    outputVariableName='D_mmc: SDV_D_mmc at Element 155 Int Point 1 in ELSET OUTSIDE', 
    steps=('Step-1', ),)

session.writeXYReport(fileName='Damage.rpt', appendMode=OFF, xyData=(Din,Dout))
session.writeXYReport(fileName='energy.rpt', appendMode=OFF, xyData=(SEout,SEin))
session.writeXYReport(fileName='Principal_strains.rpt', appendMode=OFF, xyData=(ep1,ep2,ep3,ep1in,ep2in,ep3in,eps_out,eps_in,Tout,Tin))
# session.writeXYReport(fileName='Triax-eps-hist.rpt', appendMode=OFF, xyData=(eps_out, T_out,eps_in, T_in))






# # xy1 = session.XYDataFromHistory(name='PEEQ E: 155 IP: 1 ELSET OUTSIDE-1', 
    # # odb=odb, 
    # # outputVariableName='Equivalent plastic strain: PEEQ at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# # c1 = session.Curve(xyData=xy1)
# xy2 = session.XYDataFromHistory(name='PEP1 E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Principal plastic strains: PEP1 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c2 = session.Curve(xyData=xy2)
# xy3 = session.XYDataFromHistory(name='PEP2 E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Principal plastic strains: PEP2 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c3 = session.Curve(xyData=xy3)
# xy4 = session.XYDataFromHistory(name='PEP3 E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Principal plastic strains: PEP3 at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c4 = session.Curve(xyData=xy4)
# xy5 = session.XYDataFromHistory(name='TRIAX E: 155 IP: 1 ELSET OUTSIDE-1', 
    # odb=odb, 
    # outputVariableName='Stress triaxiality: TRIAX at Element 155 Int Point 1 in ELSET OUTSIDE', 
    # steps=('Step-1', ), __linkedVpName__='Viewport: 1')
# c5 = session.Curve(xyData=xy5)
# xyp = session.xyPlots['XYPlot-1']
# chartName = xyp.charts.keys()[0]
# chart = xyp.charts[chartName]
# chart.setValues(curvesToPlot=(c0, c1, c2, c3, c4, c5, ), )
# session.charts[chartName].autoColor(lines=True, symbols=True)
# session.viewports['Viewport: 1'].setValues(displayedObject=xyp)